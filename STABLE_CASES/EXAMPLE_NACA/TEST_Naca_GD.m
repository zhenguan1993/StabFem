%% Computation of the potential flow around a 2D airfoil with FreeFem/StabFem
%
% This example is a demonstration of how to compute a potential flow or a viscous
% flow around a wing profile (NACA2412).
% 
% The source code of this program is 
% <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/NACAWINGS/Example_NACA2412.m Example_NACA2412.m>
%

%% First initialize StabFem:

close all;
SF_RecoverDataBase('WORK_NACA2412');
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4,'ffdatadir','WORK_NACA2412');

bf = SF_Load('VISCOUSFLOWS','last');
%bf = SF_Adapt(bf,'hmin',0.0001,'anisomax',2,'nbjacoby',10)

%bf = SF_Adapt(bf,'hmin',0.0001,'recompute',false);
%bf = SF_Launch('Newton_2D.edp','Options',{'Re', 1e4},'Init',bf,'DataFile','BaseFlow.txt','Store','MISC');  

%bf = SF_BaseFlow(bf,'solver','Newton_2D.edp','Options',{'Re', 1.5e4},'type','new'); 
%bf = SF_BaseFlow(bf,'solver','Newton_2D.edp','Options',{'Re', 2.5e4},'type','new'); 
%bf = SF_Adapt(bf,'hmin',0.0001,'anisomax',2,'nbjacoby',10)
%bf = SF_BaseFlow(bf,'solver','Newton_2D.edp','Options',{'Re', 2.5e4},'type','new'); 

%bf = SF_BaseFlow(bf15,'solver','Newton_2D.edp','Options',{'Re', 4e4},'type','new');  


%% without GD, without SUPG              
test = SF_BaseFlow(bf,'solver','Newton_2D_GD.edp','Options',{'Re', 2e4, 'isSUPG',0,'isGD',0},'type','new');        
% (diverge lentement)

%% with GD, without SUPG              
test = SF_BaseFlow(bf,'solver','Newton_2D_GD.edp','Options',{'Re', 2e4, 'isSUPG',0,'isGD',1},'type','new');  
% (diverge aussi)

%% with GD, with SUPG              
test = SF_BaseFlow(bf,'solver','Newton_2D_GD.edp','Options',{'Re', 2e4, 'isSUPG',1,'isGD',1,'RelaxCoef',0.85},'type','new');  
% ca converge !

%% on continue
bf = test;
Mask = SF_Mask(bf,[-.3 1 -0.1 0.1 0.005]);            
bf = SF_Adapt(bf,Mask,'hmin',0.0001,'recompute',false,'anisomax',4);

%%  
test = SF_BaseFlow(bf,'solver','Newton_2D_GD.edp','Options',{'Re', 3e4, 'isSUPG',1,'isGD',1,'RelaxCoef',0.85},'type','new');  


