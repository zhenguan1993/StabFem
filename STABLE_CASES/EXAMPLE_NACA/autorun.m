function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the wake of an airfoil  
%  - Base flow computation
%  - Linear stability with CM (box)
%
% USAGE : 
% autorun -> automatic check (non-regression test). 
% Result "value" is the number of unsuccessful tests

if(nargin==0) 
    verbosity=0; 
end

if SF_core_getopt('ffversion') < 4.7
    SF_core_log('w', 'This autorun skipped because this case requires FreeFem 4.7 or more recent');
    value = -1;
    return
end

% DISABLED ON 14/10/2020 because problem on server...
value = -1;
return


mesh = SF_Mesh('NacaFourDigits_Mesh.edp','Options',{'M',2,'P',4,'XX',12});

         

% Computing flow for alpha = 0 degrees with right circulation (Kutta condition verified)

Gamma =.7081 ; alpha = 10;
FlowK = SF_Launch('PotentialFlow.edp','Options',{'alpha',alpha,'Gamma', Gamma},'mesh',mesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS')

FlowKFY_Ref =  0.6971
FlowK.Fy
               
SFerror(1) = abs(FlowK.Fy/FlowKFY_Ref-1)


% Viscous flow 

FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 100},'mesh',mesh,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS')

FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 300},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS');
Mask = SF_Mask(FlowV,[-.3 1 -0.1 0.1 0.005]);            
FlowV = SF_Adapt(FlowV,Mask,'hmin',0.0001,'recompute',false);


FlowVFY_Ref = 0.2731
FlowV.Fy
               
SFerror(2) = abs(FlowV.Fy/FlowVFY_Ref-1)

value = sum((SFerror>1e-2))



end
