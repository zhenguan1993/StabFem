%% Flow around a wing profile : example for a NACA0012 profile
%

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4,'ffdatadir','WORK_NACA0012');

%% Generation of a mesh

mesh = SF_Mesh('NacaFourDigits_Mesh.edp','Options',{'M',0,'P',0,'XX',12});

figure; 
pos = get(gcf,'Position'); pos(3)=pos(4)*2;set(gcf,'Position',pos); % resize aspect ratio
subplot(1,2,1); SF_Plot(mesh,'title','Mesh');
subplot(1,2,2); SF_Plot(mesh,'title','Mesh (Zoom)','xlim',[.5 1],'ylim',[-.25 .25]);

%% Computing flow for alpha = 20 degrees with zero circulation (Kutta condition not verified)

alpha = 20; %angle in degrees
Gamma = 0; % circulation

Flow0 = SF_Launch('PotentialFlow.edp','Options',{'alpha',alpha,'Gamma', Gamma},'mesh',mesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS')


figure ; 
pos = get(gcf,'Position'); pos(3)=pos(4)*2;set(gcf,'Position',pos);
subplot(1,2,1); SF_Plot(Flow0,'p','xlim',[-2 2],'ylim',[-2 2],'colorrange',[-.5 .5],'colormap','redblue','title','Flow for alpha = 20� ; Gamma = 0');
hold on ; SF_Plot(Flow0,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2]);
subplot(1,2,2); SF_Plot(Flow0,'p','xlim',[.5 1],'ylim',[-.5 1],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(Flow0,{'ux','uy'},'xlim',[.5 1],'ylim',[-.25 .25]);

%% Computing flow for alpha = 20 degrees with right circulation (Kutta condition verified)

alpha = 20; %angle in degrees
Gamma = 1.15; % circulation

FlowK = SF_Launch('PotentialFlow.edp','Options',{'alpha',alpha,'Gamma', Gamma},'mesh',mesh,...
                  'DataFile','PotentialFlow.txt','Store','POTENTIALFLOWS')


figure ; 
pos = get(gcf,'Position'); pos(3)=pos(4)*2;set(gcf,'Position',pos);
subplot(1,2,1); SF_Plot(FlowK,'p','xlim',[-2 2],'ylim',[-2 2],'colorrange',[-.5 .5],'colormap','redblue','title','Flow for alpha = 20� ; Gamma = 1.15');
hold on ; SF_Plot(FlowK,{'ux','uy'},'xlim',[-2 2],'ylim',[-2 2]);
subplot(1,2,2); SF_Plot(FlowK,'p','xlim',[.5 1],'ylim',[-.5 1],'colorrange',[-.5 .5],'cbtitle','p','colormap','redblue','title','(zoom on trailing edge)');
hold on ; SF_Plot(FlowK,{'ux','uy'},'xlim',[.5 1],'ylim',[-.25 .25]);

%% VISCOUS FLOWS
alpha = 20;
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 100},'mesh',mesh,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS')

FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 300},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS')

FlowV = SF_Adapt(FlowV,'hmin',1e-4,'recompute',false);

FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 300},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS')
              
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 600},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS')              
              
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 800},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS')               

FlowV = SF_Adapt(FlowV,'hmin',1e-4,'recompute',false); 

FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 900},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS')  
              
FlowV = SF_Adapt(FlowV,'hmin',1e-4,'recompute',false); 
             
FlowV = SF_Launch('Newton_2D.edp','Options',{'alpha',alpha,'Re', 1000},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS')               
              
figure(4) ; SF_Plot(FlowV,'vort','xlim',[-1 3],'ylim',[-2 2],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[-1 3],'ylim',[-2 2]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[-1 3],'ylim',[-2 2],'CStyle','dashedneg');

              
% NB this solution is detached !              
              



%% SUMMARY 

SF_Status


