function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the wake of an airfoil  
%  - Base flow with zero-flow (conduction state)
%  - Linear stability of this base-flow
%  - Consctruction of a saturated 2D state with rolls (bifurcated base flow)
%  - 3D instability of this state.
%
% USAGE : 
% autorun -> automatic check (non-regression test). 
% autorun(4) -> verbose mode ; autorun(6) -> debug mode.
% Result "value" is the number of unsuccessful tests


if(nargin==0) 
    verbosity=0; 
end
if strcmp(SF_core_getopt('platform'),'mac')
    SF_core_setopt('eigensolver','ARPACK')
end


SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK/');
format long;

ffmesh = SF_Mesh('Mesh_ConvectionCell.edp','Params',[4 20 5],'problemtype','2dboussinesq');

%%
% compute a "base flow" for Ra = 2000
bf = SF_BaseFlow(ffmesh,'Ra',2000,'Pr',0.03,'Qmag',0);

%%
% look for eigenmodes
[ev,em] = SF_Stability(bf,'k',0,'shift',5,'nev',10,'sort','lr');

ev_REF = 0.179107000000000 + 0.000000000000000i
SFerror(1) = abs(ev(1)/ev_REF-1)

% compute a nonlinear base flow
amp = 5; % amplitude (max. velocity) of the expected pattern. NB this coefficient has to be tuned by trial-error 
guess = SF_Add({bf,em(1)},'coefs',[1 amp]); 

%%
% compute a steady state using this "guess" as a starting point
bfNL = SF_BaseFlow(guess,'Ra',2000,'Pr',0.03,'Qmag',0);

Umax_Ref = 2.696460000000000

SFerror(2) = abs(bfNL.Umax/Umax_Ref-1)


%% 
% 3D eigenvalues of this 2D state



[ev,em] = SF_Stability(bfNL,'k',pi,'shift',10+10i,'nev',20,'sort','lr');
ev3D_REF = 0.111670000000000 + 2.754300000000000i

SFerror(3) = abs(ev(1)/ev3D_REF-1)

value = sum((SFerror>1e-2))



end
