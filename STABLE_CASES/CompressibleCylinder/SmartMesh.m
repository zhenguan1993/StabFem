function bf = SmartMesh(meshDIM,varargin)
%
%  Author: Javier Sierra
%  Created: 2019-02-13
%
% ffmeshList,bfList]= SmartMesh(meshDIM,varargin)
%
%

%% Description
%
%  Create a mesh for the rotating cylinder case
%

%% Input Parameters
%
%  meshDIM:   Dimension of the mesh. Array [XMin,Xmax,Ymax]
%  meshRef:   Mesh refinement, used for SF_Adapt('Hmin')
%  Ma:       Mach number to adapt.
%  ReTarget: Reynolds target. It is used to adapt to the baseflow
%  
%

%% Output Parameters
%SmartMesh
%  ffmeshList: List of meshes
%  bfList:      List of baseflows
%

if(nargin==0)
    % parameters for meshDIM creation 
    % Outer Domain 
    meshDIM.xinfm=-40.; meshDIM.xinfv=80.; meshDIM.yinf=40.0;
    bflast = SF_Load('BASEFLOWS','last')
    if ~isempty(bflast)&&bflast.Re==50&&bflast.Mach==0.1
        bf = bflast;
        SF_core_log('n','In smartMesh : mesh/bf already done');
        return
    end
end


disp(' STARTING ADAPTMESH PROCEDURE : ');    
disp(' ');
disp(' ');  

% varargin (optional input parameters)
p = inputParser;
addParameter(p, 'Ma', 0.1, @isnumeric); % variable
addParameter(p, 'ReTarget', linspace(1,50,4)); % variable
addParameter(p, 'MeshRefine', 5, @isnumeric); % variable
addParameter(p, 'meshName', 'WORK/mesh.msh', @ischar); % variable

parse(p, varargin{:});

meshRef = p.Results.MeshRefine;
Ma = p.Results.Ma;
ReTarget = p.Results.ReTarget;
meshName = p.Results.meshName;

% Creation of initial mesh
ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',[meshDIM.xinfm,meshDIM.xinfv,meshDIM.yinf],'problemtype','2dcompsponge');

% Loop over Omegax 
bfList = [];
ffmeshList =  [];
    % Start computing  
    bf=ffmesh;
    for Re=ReTarget
            bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'solver','Newton_2D_Comp_Sponge.edp');
            bf = SF_Adapt(bf,'Hmax',meshRef);
    end
