%% Stability analysis of compressible flow around a cylinder
%
%  This script generates figures 8-9 of the ASME-AMR paper by Fabre et al.
%  (Reproducing results of Fani et al.)
%
% NB this example uses "sponge" method for nonreflecting boundary
% conditions. Alternatively you can use complex mapping. See example in
% another directory.
%
%%




%% CHAPTER 0 : set the global variables needed by the drivers

close all;
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',4,'workdir','./WORK/');

% a few options for figures
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
AspectRatio=.75;

%% Chapter 1 : computing base flow and adapting mesh
%
% Creation of initial mesh

xinfm=-40.; xinfv=80.; yinf=40.0; % dimensions
ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',[xinfm,xinfv,yinf],'problemtype','2dcompsponge');

%%
% first compute solution for Re = 1 starting from zero
Ma = 0.2;
bf=SF_BaseFlow(ffmesh,'Re',1,'Mach',Ma,'solver','Newton_2D_Comp_Sponge.edp');
     
%% 
% Then increase Re progressively to 100

bf=SF_BaseFlow(bf,'Re',10,'Mach',Ma);
bf=SF_BaseFlow(bf,'Re',50,'Mach',Ma);
bf=SF_BaseFlow(bf,'Re',100,'Mach',Ma);
  

%%
% Here we adapt the mesh ; for this we use an "Adaptation mask" which
% enforces the grid size to be at most 0.1 in a rectangular window
% [-1,3]x[0,1]. Outside this box very large cells (up to size 5) are
% allowed.


Mask = SF_Mask(bf,[-1 3 0 2 .1]);
bf = SF_Adapt(bf,Mask,'Hmax',5);
bf=SF_BaseFlow(bf,'Re',50,'Mach',Ma,'solver','Newton_2D_Comp_Sponge.edp'); % recompute after adaptation

%%
% Plots
figure(1);
subplot(2,1,1); SF_Plot(bf,'mesh','xlim',[-1 3],'ylim',[0 2],'title','Mesh (central region)');
subplot(2,1,2); SF_Plot(bf,'mesh','title','Mesh (full)');
%%
% Plot the velocity field

figure(2);    
SF_Plot(bf,'ux','xlim',[-1 3],'ylim',[0 2]);
title('Base flow at Re=150 (axial velocity)');



%% Chapter 1C : Compute eigenmode
[ev,emD] = SF_Stability(bf,'shift',0.152 + 0.642i,'nev',10,'type','D','sym','A','solver','Stab2D_Comp_Sponge.edp'); 

%%
%Plot eigenmode (figure 6 of Fani et al)

figure;
SF_Plot(emD(1),'vort','xlim',[-2 5],'ylim',[0 3],'colorrange','cropcentered','colormap','redblue');
box on; pos = get(gcf,'Position'); pos(4)=pos(3)*AspectRatio;set(gcf,'Position',pos); % resize aspect ratio
set(gca,'FontSize', 18);
saveas(gca,'Cylinder_EigenmodeRe60Ma02_vort','png');
pause(0.1);

%%
figure;
SF_Plot(emD(1),'p','colorrange',[-1e-6,1e-6],'colormap','redblue');
hold on;
SF_Plot(emD(1),'p.im','colorrange',[-1e-6,1e-6],'colormap','redblue','symmetry','XM');
box on; 
set(gca,'FontSize', 18);
saveas(gca,'Cylinder_EigenmodeRe60Ma02_p','png');
pause(0.1);



%% Summary
SF_Status


% [[PUBLISH]] (this tag is to enable automatic publication as html ; don't touch this line)
