% Initializations
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_core_start;
SF_core_setopt('verbosity',4);
SF_core_setopt('ffdatadir','./WORK/');SF_core_arborescence('cleanall');

% Initial mesh 
mesh = SF_Mesh('Mesh_Cylinder.edp','Params',[-40 80 40]);
bf = SF_BaseFlow(mesh,'Re',1,'solver','Newton_2D.edp','Symmetry','S');

% Base flow computation using mesh adaptation 
bf = SF_BaseFlow(bf,'Re',10);
bf = SF_BaseFlow(bf,'Re',60);
bf = SF_Adapt(bf,'Hmax',5,'InterpError',0.01);
[ev,emAdj] = SF_Stability(bf,'shift',0.04+0.74i,'type','A','solver','Stab_2D.edp','nev',1);
bf = SF_Adapt(bf,emAdj(1),'Hmax',5,'InterpError',0.01);

% Plot the mesh and the base flow
figure;    SF_Plot(bf,'mesh','xlim',[-1.5 4.5],'ylim',[0 3]);
figure;    SF_Plot(bf,'vort','xlim',[-1.5 4.5],'ylim',[0 3],'cbtitle','\omega_z','colormap','redblue','colorrange',[-2 2]);
hold on; SF_Plot(bf,'psi','contour','only','clevels',[-.02 0 .2 1 2 5],'xlim',[-1.5 4.5],'ylim',[0 3],'boundary','on','bdlabels',2,'bdcolors','k');

% Eigenmode calculation (single mode)
[ev,em] = SF_Stability(bf,'nev',1,'shift',0.04+0.74i,'type','D','solver','Stab_2D.edp');

% Plot of the eigenmode
figure();   SF_Plot(em,'ux','xlim',[-2 8],'ylim',[0 5],'colormap','redblue',...
			     'colorrange','cropcentered','boundary','on','bdlabels',2,'bdcolors','k');     

% Eigenmode calculation (multiple modes) using 'spectrum explorator'
plotoptions = {'ux','xlim',[-2 8],'ylim',[0 5],'colormap','redblue','colorrange','cropcentered',...
                'boundary','on','bdlabels',2,'bdcolors','k'};
ev = SF_Stability(bf,'nev',20,'shift',0.74i,'PlotSpectrum',true,'PlotModeOptions',plotoptions);			     