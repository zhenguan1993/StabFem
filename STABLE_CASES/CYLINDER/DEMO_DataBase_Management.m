%% Demonstration of Database operation and management with StabFem
%
% This tutorial allows to observe step-by-step how, in a series of baseflow
% computations and mesh adaptations, the files are stored in the database,
% how the contents can be checked using SF_Status and reaccessed using
% SF_Load


%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',3);

%% Initial mesh
%

ffmesh =  SF_Mesh('Mesh_Cylinder.edp','Params',[-40 80 40],'problemtype','2D','cleanworkdir',true);
% Note here the option 'cleanworkdir',true whose effect is to clean up the data base from any previous data. Be careful when using this option !
% Another way to clean up the database is as follows :
% SF_core_arborescence('cleanall')

SF_Status;

%% Computing two BFs with initial mesh
bf = SF_BaseFlow(ffmesh,'Re',1)

SF_Status;

%% Doing a few steps in Re

bf = SF_BaseFlow(bf,'Re',2)

bf = SF_BaseFlow(bf,'Re',3)

%% One step backwards to a BF already computed

bf = SF_BaseFlow(bf,'Re',2) 
% note that here the BF is not recomputed since it is found in the database

SF_Status;

%% Adapting mesh (with recomputing BF)

bf = SF_Adapt(bf,'recompute',true) 
% note that this is what is done by default if option 'recompute' is not specified 

SF_Status;
% Note that a BF has been added in both directories 'MESHES' and
% 'BASEFLMOWS'. The latter the projected one and the former the recomputed
% one.


%% Recomputing for Re = 2 and computing one more for Re = 10

bf = SF_BaseFlow(bf)

bf = SF_BaseFlow(bf,'Re',10)

SF_Status;

%% Adapting mesh (without recomputing BF)
% In some cases it may be safer to disable the automatic recomputation of a
% base flow after adaptation. In this case the BF is not added to folder 'BASEFLOWS'.
% 

bf = SF_Adapt(bf,'recompute',false)
SF_Status;

%% Now recomputing
bf = SF_BaseFlow(bf,'Re',10);
SF_Status;

%% 


%% New bf using this mesh for Re = 2

bf = SF_BaseFlow(bf,'Re',2)
% note that contrary to what happened above in example "One step backwards"
% the BF is recomputed. There is already a BF for Re=2 in the database but
% it is not consistent with the mesh currently used. 

SF_Status;

%% Recovering baseflow from the database
% We can pick a given Baseflow from the badabase using SF_Load. For
% instance to get the third computed one :

bf = SF_Load('BASEFLOWS',3)

%%
% It is also possible to pick the last computed one in this way :

bf = SF_Load('BASEFLOWS','last')

%% Recovering simply the mesh from the database
% To get the second one, for instance

mesh = SF_Load('MESH',2)

%% Warning (legacy)
% It was previously possible to pick up both a mesh and the first computed base flow
% associated to it using this :
%
% bf = SF_Load('MESHES',i) 
% bf = SF_Load('MESHES','last') 
%
% This syntax is not recommended any more, since it will now provide the base flow projected on the new mesh, 
% not the recomputed one. Importation of data from .ff2m file may also be incorrect.

%% Database management of eigenmodes
% Finally we show how eigenmodes are stored and can be reaccessed 

[ev,em] = SF_Stability(bf,'nev',10,'shift',1i);
ev = SF_Stability(bf,'nev',10,'shift',2i);

sfs = SF_Status

%% Recovering an eigenmode

em5 = SF_Load('EIGENMODES',5)

%
% NOTE : eigenmodes are stored in this way if using the syntax [ev,em] = SF_Stability(...)
%        but not with the syntax ev =  SF_Stability(...)


%% Recovering all eigenvalues

% Note that all eigenvalues, along with the associated metadata, are
% available in the field 'EIVENVALUES' of the object sfs returned by
% SF_Status

sfs.EIGENVALUES



% [[PUBLISH]]
