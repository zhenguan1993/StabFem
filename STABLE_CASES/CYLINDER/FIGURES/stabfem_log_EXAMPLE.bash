#  This file is summary of all bash commands used when running you StabFem case 
echo  -40  80  40   | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Mesh_Cylinder.edp  
mv ./WORK/mesh.msh ./WORK/MESHES/FFMESH_000001.msh
mv ./WORK/mesh.ff2m ./WORK/MESHES/FFMESH_000001.ff2m
mv ./WORK/mesh_connectivity.ff2m ./WORK/MESHES/FFMESH_000001_connectivity.ff2m

cp ./WORK/MESHES/FFMESH_000001.msh ./WORK/mesh.msh
echo   | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Newton_2D.edp  -Re 1  -Symmetry S  
cp ./WORK/BaseFlow.txt ./WORK/BASEFLOWS/FFDATA_000001.txt
cp ./WORK/BaseFlow.ff2m ./WORK/BASEFLOWS/FFDATA_000001.ff2m
mv ./WORK/BaseFlow.txt ./WORK/MESHES/FFDATA_000001.txt
mv ./WORK/BaseFlow.ff2m ./WORK/MESHES/FFDATA_000001.ff2m

cp ./WORK/MESHES/FFDATA_000001.txt ./WORK/BaseFlow_guess.txt
cp ./WORK/MESHES/FFMESH_000001.msh ./WORK/mesh.msh
echo   | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Newton_2D.edp  -Re 10  -Symmetry S  
mv ./WORK/BaseFlow.txt ./WORK/BASEFLOWS/FFDATA_000002.txt
mv ./WORK/BaseFlow.ff2m ./WORK/BASEFLOWS/FFDATA_000002.ff2m
cp ./WORK/BASEFLOWS/FFDATA_000002.txt ./WORK/BaseFlow_guess.txt
cp ./WORK/BASEFLOWS/FFDATA_000002.ff2m ./WORK/BaseFlow_guess.ff2m

(...)
echo   | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Newton_2D.edp  -Re 60  -Symmetry S  

#  Here a file  Param_Adaptmesh.idp has been created by driver SF_Adapt 
cp ./WORK/MESHES/FFMESH_000001.msh ./WORK/mesh.msh
cp ./WORK/BASEFLOWS/FFDATA_000003.txt ./WORK/FlowFieldToAdapt1.txt
echo  1  ReP2P2P1 1  | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 AdaptMesh.edp  
(...)
echo   | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Newton_2D.edp  -Re 60  -Symmetry S  -type POSTADAPT  
(...)
echo   | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Stab_2D.edp  -nev 1  -Re 60  -type A  -shift_r 0.04 -shift_i 0.74 
(...)
echo  2  ReP2P2P1 1  CxP2P2P1 0  | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 AdaptMesh.edp  
(...)
echo   | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Newton_2D.edp  -Re 60  -Symmetry S  -type POSTADAPT  
(...)
echo   | "/usr/local/bin/FreeFem++-mpi" -nw -v 0 Stab_2D.edp  -nev 20  -Re 60  -shift_r 0 -shift_i 0.74 
