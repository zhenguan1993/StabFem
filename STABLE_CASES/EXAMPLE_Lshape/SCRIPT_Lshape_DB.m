%% Tutorial example demonstrating Data-Base management of the StabFem Software
addpath([fileparts(fileparts(pwd)) '/SOURCES_MATLAB']);
SF_Start('verbosity',2); 
SF_core_setopt('ffdatadir','./WORK'); % This is to initialize the database management. 
SF_core_arborescence('cleanall'); % (optional :) this is to clean up the database.

%% create mesh
ffmesh = SF_Mesh('Lshape_Mesh.edp','Options',{'meshdensity',40});

%% We now preform a loop over omega to solve Thermal problems and store results in folder 'Thermal'
for omega = [0.1:0.1:1]
  heatU=SF_Launch('Lshape_Unsteady.edp','Options',{'omega',omega},'Mesh',ffmesh,'DataFile','Heat_Unsteady.ff2m','Store','Thermal')            
end

%% A summary of everything stored into the database can be obtained as follows:
sfs = SF_Status
IndexThermal = sfs.Thermal % The Component 'Thermal' of the return object is the Index of this folder

%% We load and plot the thermal problem corresponding to omega = 0.5 (number 5 in database) as follows:
heatU = SF_Load('Thermal',5);
figure; SF_Plot(heatU,'T');

%% And a plot of impedance as function of omega can be obtained as follows;
array_omega = [sfs.Thermal.omega]; 
array_HeatFlux = [sfs.Thermal.HeatFlux];
figure; plot(array_omega,real(1./array_HeatFlux),'r-*',array_omega,imag(1./array_HeatFlux),'b--*');
xlabel('\omega'); ylabel('Z');legend('Re(Z)','Im(Z)');