%% Script "SCRIPT_Lshape_summary.m" 
% this script is a reduced version of the full example "Script_Lshape.m" available on the website of the project.

%% Initialization
addpath([fileparts(fileparts(pwd)) '/SOURCES_MATLAB']);
SF_Start('ffdatadir','./WORK/','verbosity',2,'cleandir',true);

%% Mesh generation and plot
Ndensity =40;
ffmesh = SF_Mesh('Lshape_Mesh.edp','Options',{'meshdensity',Ndensity})
figure();SF_Plot(ffmesh,'mesh','title','Mesh for L-shape body');

%% First problem : steady conduction
heatS = SF_Launch('Lshape_Steady.edp','Mesh',ffmesh)
figure(1);SF_Plot(heatS,'T','xlim',[0 1],'ylim',[0 1],'title', {'Conduction on a L-shaped domain: ', 'temperature field (colors) and heat flux (quiver) '});
hold on; plot([0 1],[.25,.25],'r--');
Ycut = 0.25; Xcut = 0:.01:1; Tcut = SF_ExtractData(heatS,'T',Xcut,Ycut);
figure(2);plot(Xcut,real(Tcut),'r-',Xcut,imag(Tcut),'b--');


%% Second problem : unsteady conduction
omega = 100;
heatU = SF_Launch('Lshape_Unsteady.edp','Options',{'omega',omega},'Mesh',ffmesh,'DataFile','Heat_Unsteady.ff2m','Store','Thermal')
figure(); SF_Plot(heatU,'T.re','colormap','redblue','title',{'Ti: ' , 'real(colors) and imaginary(levels) parts'});
hold on;  SF_Plot(heatU,'T.im','contour','only'); 
% NB : Here the full program "Script_Lshape.m" contains a sequence of instructions to adapt the mesh. 
%      This part is skipped here, mesh adaptation will be explained in a subsequent chapter. 

%% Third problem : Thermal impedance Z(omega) over a range of omega
res = SF_Launch('Lshape_Impedance.edp','Mesh',ffmesh,'DataFile','Heat_Unsteady_loop.ff2m')
figure;loglog(res.omega,real(1./res.Flux),'r','LineWidth',2);
hold on;loglog(res.omega,imag(1./res.Flux),'b','LineWidth',2);

%% Fourth problem : Stokes flow
Stokes = SF_Launch('Lshape_Stokes.edp','Mesh',ffmesh,'Options',{'nu',1,'Umax',1},'DataFile','Stokes.ff2m','Store','Stokes')
figure;SF_Plot(Stokes,'vorticity');
hold on;SF_Plot(Stokes,{'ux','uy'},'ylim',[0 1],'xlim',[0 1],'title','Stokes flow (vorticity and velocity fields)');

%% Demonstration of database manager
SF_Status

%% [[PUBLISH]]