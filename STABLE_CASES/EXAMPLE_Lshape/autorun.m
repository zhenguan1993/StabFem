function value = autorun(verbosity)

% Autorun function for StabFem. 
% This function will produce sample results for the case EXAMPLE_Lshape
%
% USAGE : 
% autorun -> automatic check
% autorun(1) -> produces the figures used for the manual



 isfigures=0;
 
if(nargin==0) 
   verbosity=0;
end

SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK_AUTORUN/');

value=0;



% Generation of the mesh
Ndensity =40;
ffmesh=SF_Mesh('Lshape_Mesh.edp','Options',{'meshdensity',Ndensity});

if(isfigures)
    mkdir FIGURES
    figure();plotFF(ffmesh,'title','Mesh for L-shape body'); 
    set(gca,'FontSize', 18); saveas(gca,'FIGURES/Lshape_Mesh','png');
    pause(1);
end;

% importation and plotting of a real P1 field : temperature
heatS=SF_Launch('Lshape_Steady.edp','Mesh',ffmesh);
if(isfigures) 
    figure();plotFF(heatS,'T','title', 'Solution of the steady heat equation on a L-shaped domain'); 
    set(gca,'FontSize', 18); saveas(gca,'FIGURES/Lshape_T0','png');
    pause(1);
end;

% importation and plotting of data not associated to a mesh : temperature along a line
Ycut = 0.25;Xcut = [0:.01:1];
heatCut = SF_ExtractData(heatS,'T',Xcut,Ycut);
%heatCut = importFFdata('Heat_1Dcut.ff2m');
if(isfigures) figure();plot(Xcut,heatCut);
    title('Temperature along a line : T(x,y=0.25)') 
    set(gca,'FontSize', 18); saveas(gca,'FIGURES/Lshape_T0_Cut','png');
    pause(1);
end;

critere1 = heatCut(50);
critere1REF = 0.034019400000000;

disp('##### autorun test 1 : Steady temperature at a given point');
SFerror(1) = abs(critere1-critere1REF)
%if(error(1)>1e-3)
%    value = value+1
%end

% importation and plotting of a complex P1 field : temperature for unsteady problem
heatU=SF_Launch('Lshape_Unsteady.edp','Options',{'omega',100},'Mesh',ffmesh,'DataFile','Heat_Unsteady.ff2m');
if(isfigures) 
    figure();plotFF(heatU,'Tc.re','title',['Ti: real(colors) part']) 
    set(gca,'FontSize', 18); saveas(gca,'FIGURES/Lshape_Tc','png');
end


critere2 = max(imag(heatU.T))
critere2REF = 0.5047

disp('##### autorun test 2 : Unsteady temperature at a given point');
SFerror(2) = abs(critere2-critere2REF)

%if(error(2)>1e-3)
%    value = value+1
%end
value = sum(SFerror>1e-3)
end
%[[AUTORUN:SHORT]]
