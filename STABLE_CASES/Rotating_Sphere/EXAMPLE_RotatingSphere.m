%% Initialization
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB/']);
SF_Start('verbosity',3)

close all;

%% generate mesh and base flow
mesh = SF_Mesh('Mesh_Sphere.edp','Params',[-10 20 10],'problemtype','axixrswirl');
bf = SF_BaseFlow(mesh,'Re',1,'Omegax',0.1);
bf = SF_Adapt(bf,'Hmax',1);
bf = SF_BaseFlow(bf,'Re',10);
bf = SF_BaseFlow(bf,'Re',50);
bf = SF_BaseFlow(bf,'Re',100);
bf = SF_Adapt(bf,'Hmax',1);
bf = SF_BaseFlow(bf,'Re',250);
bf = SF_Adapt(bf,'Hmax',1);

%% plot bf
figure; SF_Plot(bf,'uphi','xlim',[-1 3],'ylim',[0,2],'colormap','cool');
hold on;SF_Plot(bf,'psi','contour','only','xlim',[-1 3],'ylim',[0,2]);

%% stability for m=1
[ev,em] = SF_Stability(bf,'m',1,'shift',1+.5i,'nev',20,'Plotspectrum',true)


figure; subplot(2,1,1); SF_Plot(em(1),'ux','xlim',[-1 5],'ylim',[0 1.5],'colormap','redblue','title',['mode 1 : \lambda = ',num2str(ev(1))]);
subplot(2,1,2); SF_Plot(em(2),'ux','xlim',[-1 5],'ylim',[0 1.5],'colorrange','cropcenter','colormap','redblue','title',['mode 2 : \lambda = ',num2str(ev(2))]);


%% mesh adaptation to adjoint of mode 1:
%[~,em1] = SF_Stability(bf,'m',1,'shift',ev(1),'nev',1,'type','A');
%bf = SF_Adapt(bf,em1,'Hmax',1);

%[ev,em] = SF_Stability(bf,'m',1,'shift',1+.5i,'nev',20);
%ev(1:2)

