%%  LINEAR Stability ANALYSIS of the wake of a ROTATING cylinder with STABFEM  
%
%
% This script demonstrates how to use StabFem to do a parametric study of the instability
% in the wake of a 2D ROTATING cylinder in incompressible flow .
% 
% The script reproduces the neutral curve associated to mode I 
% (figure 5 of Sierra et al. 2020)
%
%
% # Generation of an adapted mesh
% # Parametric study as function of Re and alpha
% # Generation of figures in postprocess mode
% 
% 
%
% The raw source code is <https://gitlab.com/stabfem/StabFem/blob/master/STABLE_CASES/Rotating_Cylinder/CYLINDER_WithSmallRotation.m here>. 
%

%%
%
% First we set a few global variables needed by StabFem
%

close all;
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity', 2,'ffdatadir', './WORK/');

if ~exist('postprocessonly')
    postprocessonly = false; % this is a trick to save time if the data has previously been generated
end

if ~postprocessonly 
%% Chapter 1 : mesh generation 
% (from DNS_Cylinder case)

SF_core_arborescence('cleanall');
ffmesh = SF_Mesh('Mesh_Cylinder_FullDomain.edp');
bf=SF_BaseFlow(ffmesh,'Re',1,'solver','Newton_2D.edp');
bf = SF_Adapt(bf,'Hmax',5);
bf = SF_BaseFlow(bf,'Re',10);
bf = SF_BaseFlow(bf,'Re',60);
bf = SF_Adapt(bf,'Hmax',5);
bf = SF_BaseFlow(bf,'Re',60);
[ev,em] = SF_Stability(bf,'shift',0.04+0.76i,'sym','N','type','A','solver','Stab_2D.edp');
%Mask = SF_Mask(bf,'Rectangle',[-2 5 -2 2 .1]);
bf = SF_Adapt(bf,em(1),'Hmax',5);



%% Chapter 2 : parametric study

OmegaRange = [0:.1 :1];
ReRange = [60:-2.5:40];
for Omega = OmegaRange
    bf = SF_BaseFlow(bf,'Re',ReRange(1),'Omegax',Omega);
    ev = SF_Stability(bf,'shift',0.04+0.76i,'nev',10);
    SF_Stability_LoopRe(bf,ReRange,ev(1));
end

postprocessonly = true
end

%% Chapter 3 : post-processing using SF_Status

%% 
% To generate the index of the database

sfs = SF_Status('QUIET');

sfs.THRESHOLDS.Re
sfs.THRESHOLDS.Omegax
%%
% To plot the alpha/Re curve

figure;
plot(sfs.THRESHOLDS.Re,sfs.THRESHOLDS.Omegax)
xlabel('Re'); ylabel('alpha');title('neutral curve in alpha/Re plane')

%%
% To get all eigenvalues computed for alpha = .5 and plot them as function of RE

lambda_toplot = sfs.EIGENVALUES.lambda( sfs.EIGENVALUES.Omegax==.5);
Re_toplot = sfs.EIGENVALUES.Re( sfs.EIGENVALUES.Omegax==.5);
figure;
plot(Re_toplot,real(lambda_toplot),'x')
xlabel('Re'); ylabel('Re(lambda)');title('eigenvalues computed for alpha = .5')

% [[PUBLISH]]
% [[FIGURES]]