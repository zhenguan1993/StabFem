/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////
///// 
/////                              MESH GENERATOR for a cylinder
/////
/////             This file creates a mesh and an initial base flow for the "StabFem" program.
/////
/////             input parameters : Xmin Xmax Ymax
/////  			  
/////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////

include "StabFem.idp";
// densità di vertici nelle sottozone 
int ncil=75, ns=3, n1=18, n2=16;
// 
//int n=10;
int n=4;
real xinfm=-40., xinfv=80., yinf=40.;
real x1m=-5.,x1v=17.,y1=2.5;
real x2m=-8.,x2v=30.,y2=5;
// geometria cilindro

real bccyl=2,bcinf=4,bcsup=4,bcout=3,bcinlet0=1;

border cilindro(t=0,2*pi){ x=cos(t)*0.5;y=0.5*sin(t);label=bccyl;}
// dominio esterno
border inlet(t=0,1){ x=xinfm;y=yinf*(1-2*t);label=bcinlet0;}
border latinf(t=0,1){ x=xinfm+(xinfv-xinfm)*t;y=-yinf;label=bcinf;}
border outlet(t=0,1){ x=xinfv;y=-yinf*(1-2*t);label=bcout;}
border latsup(t=0,1){ x=xinfv-(xinfv-xinfm)*t;y=yinf;label=bcsup;}

// domini di infittimento 1: vicino al cilindro
border a1(t=0,1){ x=x1m;y=y1*(1-2*t);label=0;}
border a2(t=0,1){ x=x1m+(x1v-x1m)*t;y=-y1;label=0;}
border a3(t=0,1){ x=x1v;y=-y1*(1-2*t);label=0;}
border a4(t=0,1){ x=x1v-(x1v-x1m)*t;y=+y1;label=0;}

// infittimento intermedio
border b1(t=0,1){ x=x2m;y=y2*(1-2*t);label=0;}
border b2(t=0,1){ x=x2m+(x2v-x2m)*t;y=-y2;label=0;}
border b3(t=0,1){ x=x2v;y=-y2*(1-2*t);label=0;}
border b4(t=0,1){ x=x2v-(x2v-x2m)*t;y=+y2;label=0;}

mesh th=buildmesh(inlet(2*yinf/n*ns)+latinf((xinfv-xinfm)/n*ns)+outlet(2*yinf/n*ns)+latsup((xinfv-xinfm)/n*ns)+cilindro(-6*ncil*pi/n)+a1(n1/n*2*y1)+a2(n1/n*(x1v-x1m))+a3(n1/n*2*y1)+a4(n1/n*(x1v-x1m))+b1(2*y2/n*n2)+b2((x2v-x2m)/n*n2)+b3(2*y2/n*n2)+b4((x2v-x2m)/n*n2));

// SAVE THE MESH in mesh.msh file 
savemesh(th,ffdatadir+"mesh.msh");


// FIRST AUXILIARY FILE for Stabfem : SF_Init.ff2m
{
            ofstream file(ffdatadir+"SF_Init.ff2m"); 
			file << "Defininition of problem type and geometrical parameters for StabFem. Problem type : " << endl;
			file << "2DComp" << endl;
			file << "Format :  (this list may contain geometrical parameters such as domain dimensions, etc..)" << endl;
			file << "real R real Xmin real Xmax real Rmax" << endl;
			file <<  0.5  << endl << xinfm << endl << xinfv << endl << yinf  << endl;
}

	cout << "Saving" << endl;
// SECOND AUXILIARY FILE  for Stabfem : mesh.ff2m
	SFWriteMesh(ffdatadir+"mesh.ff2m",th,"initial");
	cout << "Mesh Saved" << endl;



// THIRD AUXILIARY FILE for Stabfem : SF_Geom.edp
{
			ofstream file2("SF_Geom.edp"); 
			file2 << "// Description geometry (file automatically created ; to be included in the FreeFem programs)" << endl;
            file2 << " real SFGeomLengthscale = 1 ; // length scale for nondimensionalization" << endl ;
            file2 << " real SFGeomVelocityscale = 1 ; // velocity scale for nondimensionalization " << endl ;
}


// SECOND AUXILIARY FILE  for Stabfem : mesh.ff2m
IFMACRO(SFWriteMesh)
	SFWriteMesh(ffdatadir+"mesh.ff2m",th,"initial")
ENDIFMACRO

// AUXILIARY FILE NEEDED TO PLOT P2 DATA
IFMACRO(SFWriteConnectivity)
	SFWriteConnectivity(ffdatadir+"mesh_connectivity.ff2m",th);
ENDIFMACRO
