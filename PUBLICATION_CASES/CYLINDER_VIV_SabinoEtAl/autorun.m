function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the wake of a SPRING-MOUNTED cylinder with STABFEM  

if(nargin==0) 
    verbosity=0; 
end

if(verbosity==0)
if ~SF_core_detectlib('SLEPc-complex')
  SF_core_log('w', 'Autorun skipped because SLEPc is not available');
  value = -1;
  return;
end
end


SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK_AUTORUN/');
SF_core_arborescence('cleanall');


%% ##### CHAPTER 1 : EIGENVALUES FOR modes EM and FM

bf = SmartMesh_Cylinder('S',[-40 80 40]); 

mstar = 20; Ustar = 3;

% mode "FM"

M = mstar*pi/4; K = pi^3*mstar/Ustar^2;

shiftFM = 0.04689 + 0.74874i; 
%14/10/2020 : (0.0691564,0.732748) ???
[evFM,emFM] = SF_Stability(bf,'solver','Stab2D_VIV.edp','shift',shiftFM,'nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);


% mode "EM"
shiftEM = -0.0188 + 2.0100i; 
% 14/10/2020 : (-0.25001,1.79441) ???
[evEM,emEM] = SF_Stability(bf,'solver','Stab2D_VIV.edp','shift',shiftEM,'nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0);


SFerror(1) = abs(evEM/shiftEM-1)+abs(evFM/shiftFM-1);


%% Test 2 : impedance for Re = 35, omega = 0.75
bf = SF_BaseFlow(bf,'Re',35);
fo = SF_LinearForced(bf,'omega',0.75);

Zref = -1.17483-1.3947i; % -0.5854 - 0.6950i; previously factor 2

SFerror(2) = abs(fo.Z/Zref-1)

%% BILAN
value = sum((SFerror>1e-2))

end
