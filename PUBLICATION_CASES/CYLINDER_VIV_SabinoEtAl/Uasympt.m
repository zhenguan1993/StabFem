
function fo = Uasympt(fo,mstar,gamma)
%
% This function computes the impedance-based approximation of eigenvalue 
% following the approach of Sabino et al. (JFM 2020)
% the function works on an "impedance" objet and adds to it fields "ustar"
% and "omegai"
%

omega = fo.omega;
N = length(omega);
Z = fo.Z/2; %%%/2;
Ustar2 = 4*pi^2./(omega.*(omega-2/pi/mstar*imag(Z)*2));
Ustar2(Ustar2<0) = NaN;
Ustar = sqrt(Ustar2);

% derivative of Z (assuming a constant grid)
if (omega(2)-omega(1))-(omega(N)-omega(N-1))>1e-6
    SF_core_log('w', ' in Usaympt : itseems your omega-mesh is not unuiform !') 
end    
domega = omega(2)-omega(1);

dZdom(2:N-1) = (Z(3:N) - Z(1:N-2))/(2*domega);
dZdom(1) = (-3*Z(1)+4*Z(2)-Z(3))/(2*domega);
dZdom(N) = (3*Z(N)-4*Z(N-1)+Z(N-2))/(2*domega);
dZdom = dZdom.';
fo.dZdom = dZdom;

omegai = - (2*pi^2*gamma*mstar./Ustar+2*real(Z))./(pi*mstar-2*imag(Z)./omega+1i*omega.*dZdom);



fo.Ustar = Ustar;
fo.omegai = omegai;

indices = (real(Z(1:end-1)).*real(Z(2:end)))<0;
fo.Ustarcrit = Ustar(indices);
fo.omegacrit = omega(indices);
fo.Zicrit = imag(Z(indices));


end