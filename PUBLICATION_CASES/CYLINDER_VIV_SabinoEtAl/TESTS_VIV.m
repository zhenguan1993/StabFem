
% This script is a test of VIV solvers using confinment. We compare RR,RA,AA and ALE methods

addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',3);
SF_core_arborescence('cleanall');

bf = SmartMesh_Cylinder('S',[-5,30,2]);

%bf = SF_Load('lastadapted'); 
%bf = SF_BaseFlow(bf,'Re',60);

mstar = 20; Ustar = 3;shiftFM = 0.04689 + 0.74874i; 
M = mstar*pi/4; K = pi^3*mstar/Ustar^2;

Yline = linspace(0,max(bf.mesh.points(2,:)),200); 
Uyb = SF_ExtractData(bf,'uy',0,Yline);
figure(44);hold off;plot(Yline,real(Uyb),'-r')


%% TESTS EIGENVALUE SOLVERS
if (1==0)
% old solver (works only with nev=1)
[ev,em] = SF_Stability(bf,'shift',shiftFM,'nev',1,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0,'solver','Stab2D_VIV_old.edp')

% New solver ; Relative frame / Relative vel mode (default)
[ev,em] = SF_Stability(bf,'shift',shiftFM,'nev',10,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0,'plotspectrum',true)

% New solver ; Relative frame / Abs vel mode
[ev,em] = SF_Stability(bf,'shift',shiftFM,'nev',10,'type','D','STIFFNESS',K,'MASS',M,'DAMPING',0,'plotspectrum',true,'Options','-Vel A')

% Adjoint 
[ev,em] = SF_Stability(bf,'shift',shiftFM,'nev',10,'type','A','STIFFNESS',K,'MASS',M,'DAMPING',0,'plotspectrum',true)
end

%% Validation on the case of a forced problem (omega = 1e-3 ; quasi-static)

omega = 0.001;

% Reference with default solver (cf. Sabino et al)
ffref = SF_LinearForced(bf,'omega',omega); % This solver is incorrect for strong confinement 
Yline = linspace(0,max(bf.mesh.points(2,:)),200); 
Lineref = SF_ExtractData(ffref,'all',0,Yline);

% Relative velocities in relative frame
ffRR = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_work.edp','Options','-Vel R -Frame R -Normalize Y') 
LineRR = SF_ExtractData(ffRR,'all',0,Yline);

% Absolute velocities in relative frame 
ffAR = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_work.edp','Options','-Vel A -Frame R -Normalize Y') ;
LineAR = SF_ExtractData(ffAR,'all',0,Yline);

% Absolute velocities in absolute frame 
% (this one does NOT include added stress !)
ffAA = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_work.edp','Options','-Vel A -Frame A -Normalize Y')
LineAA = SF_ExtractData(ffAA,'all',0,Yline);

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 
LineALE = SF_ExtractData(ffALE,'all',0,Yline);

% ALE solver with elasticity operator instead of Laplacian
ffALEe = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp','Options','-ALEOP elasticity') 
LineALEe = SF_ExtractData(ffALEe,'all',0,Yline);

% NB for the "added stress" term is 

% Comparaison between solvers : Figures

% Forces
[ ffref.Z, ffRR.Fy, ffAR.Fy, ffAA.Fy, ffALE.Fy,ffALE.Fy+ffALE.Fyxi,ffALEe.Fy+ffALEe.Fyxi]

% Velocities along a vertical line
figure(45);plot(Yline,real(LineRR.uy),'-r',Yline,imag(LineRR.uy),':r',Yline,real(LineRR.uya),'--r',Yline,imag(LineRR.uya),'-..r')
figure(46);plot(Yline,real(LineRR.ux),'-r',Yline,imag(LineRR.ux),':r',Yline,real(LineRR.uxa),'--r',Yline,imag(LineRR.uxa),'-..r')
figure(45);hold on;plot(Yline,real(LineAR.uy),'-b',Yline,imag(LineAR.uy),':b',Yline,real(LineAR.uya),'--b',Yline,imag(LineAR.uya),'-..b')
figure(46);hold on;plot(Yline,real(LineAR.ux),'-b',Yline,imag(LineAR.ux),':b',Yline,real(LineAR.uxa),'--b',Yline,imag(LineAR.uxa),'-..b')
figure(45);hold on;plot(Yline,real(LineAA.uyr),'-g',Yline,imag(LineAA.uyr),':g',Yline,real(LineAA.uy),'--g',Yline,imag(LineAA.uy),'-..g')
figure(46);hold on;plot(Yline,real(LineAA.uxr),'-g',Yline,imag(LineAA.uxr),':g',Yline,real(LineAA.ux),'--g',Yline,imag(LineAA.ux),'-..g')
figure(45);hold on;plot(Yline,real(LineALE.uy),'-k',Yline,imag(LineALE.uy),':k',Yline,real(LineALE.uya),'--k',Yline,imag(LineALE.uya),'-..k')
figure(46);hold on;plot(Yline,real(LineALE.ux),'-k',Yline,imag(LineALE.ux),':k',Yline,real(LineALE.uxa),'--k',Yline,imag(LineALE.uxa),'-..k')
figure(45);hold on;plot(Yline,real(LineALEe.uy),'-c',Yline,imag(LineALEe.uy),':c',Yline,real(LineALEe.uya),'--c',Yline,imag(LineALEe.uya),'-..c')
figure(46);hold on;plot(Yline,real(LineALEe.ux),'-c',Yline,imag(LineALEe.ux),':c',Yline,real(LineALEe.uxa),'--c',Yline,imag(LineALEe.uxa),'-..c')


figure(45);legend('Uyrel,r','Uyrel,i','Uyabs,r','Uyabs,i')
figure(46);legend('Uxrel,r','Uxrel,i','Uxabs,r','Uxabs,i')

% plot of velocity fields
figure(15); subplot(4,1,1);SF_Plot(ffRR,'uxa','xlim',[-2 4],'ylim',[0 1.2],'title','RR (abs)');
subplot(4,1,2);SF_Plot(ffRR,'ux','xlim',[-2 4],'ylim',[0 1.2],'title','RR (rel)');
subplot(4,1,3);SF_Plot(ffALE,'uxa','xlim',[-2 4],'ylim',[0 1.2],'title','ALE (abs)');
subplot(4,1,4);SF_Plot(ffALE,'ux','xlim',[-2 4],'ylim',[0 1.2],'title','ALE (ALEfield)');

figure(16); subplot(4,1,1);SF_Plot(ffRR,'uya','xlim',[-2 4],'ylim',[0 1.2],'title','RR (abs)');
subplot(4,1,2);SF_Plot(ffRR,'uy','xlim',[-2 4],'ylim',[0 1.2],'title','RR (rel)');
subplot(4,1,3);SF_Plot(ffALE,'uya','xlim',[-2 4],'ylim',[0 1.2],'title','ALE (abs)');
subplot(4,1,4);SF_Plot(ffALE,'uy','xlim',[-2 4],'ylim',[0 1.2],'title','ALE (ALEfield)');


%% Validation on the case of a forced problem (omega=1)

omega = 1;

% Reference with default solver (cf. Sabino et al)
ffref = SF_LinearForced(bf,'omega',omega); % This solver is incorrect for strong confinement 
Yline = linspace(0,max(bf.mesh.points(2,:)),200); 
Lineref = SF_ExtractData(ffref,'all',0,Yline);

% Relative velocities in relative frame
ffRR = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_work.edp','Options','-Vel R -Frame R -Normalize Y') 
LineRR = SF_ExtractData(ffRR,'all',0,Yline);

% Absolute velocities in relative frame 
ffAR = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_work.edp','Options','-Vel A -Frame R -Normalize Y') ;
LineAR = SF_ExtractData(ffAR,'all',0,Yline);

% Absolute velocities in absolute frame 
% (this one does NOT include added stress !)
ffAA = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_work.edp','Options','-Vel A -Frame A -Normalize Y')
LineAA = SF_ExtractData(ffAA,'all',0,Yline);

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 
LineALE = SF_ExtractData(ffALE,'all',0,Yline);

% ALE solver with elasticity operator instead of Laplacian
ffALEe = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp','Options','-ALEOP elasticity') 
LineALEe = SF_ExtractData(ffALEe,'all',0,Yline);

% NB for the "added stress" term is 

% Comparaison between solvers : Figures

% Forces
[ ffref.Z, ffRR.Fy, ffAR.Fy, ffAA.Fy, ffALE.Fy,ffALE.Fy+ffALE.Fyxi,ffALEe.Fy+ffALEe.Fyxi]

% Velocities along a vertical line
figure(45);plot(Yline,real(LineRR.uy),'-r',Yline,imag(LineRR.uy),':r',Yline,real(LineRR.uya),'--r',Yline,imag(LineRR.uya),'-..r')
figure(46);plot(Yline,real(LineRR.ux),'-r',Yline,imag(LineRR.ux),':r',Yline,real(LineRR.uxa),'--r',Yline,imag(LineRR.uxa),'-..r')
figure(45);hold on;plot(Yline,real(LineAR.uy),'-b',Yline,imag(LineAR.uy),':b',Yline,real(LineAR.uya),'--b',Yline,imag(LineAR.uya),'-..b')
figure(46);hold on;plot(Yline,real(LineAR.ux),'-b',Yline,imag(LineAR.ux),':b',Yline,real(LineAR.uxa),'--b',Yline,imag(LineAR.uxa),'-..b')
figure(45);hold on;plot(Yline,real(LineAA.uyr),'-g',Yline,imag(LineAA.uyr),':g',Yline,real(LineAA.uy),'--g',Yline,imag(LineAA.uy),'-..g')
figure(46);hold on;plot(Yline,real(LineAA.uxr),'-g',Yline,imag(LineAA.uxr),':g',Yline,real(LineAA.ux),'--g',Yline,imag(LineAA.ux),'-..g')
figure(45);hold on;plot(Yline,real(LineALE.uy),'-k',Yline,imag(LineALE.uy),':k',Yline,real(LineALE.uya),'--k',Yline,imag(LineALE.uya),'-..k')
figure(46);hold on;plot(Yline,real(LineALE.ux),'-k',Yline,imag(LineALE.ux),':k',Yline,real(LineALE.uxa),'--k',Yline,imag(LineALE.uxa),'-..k')
figure(45);hold on;plot(Yline,real(LineALEe.uy),'-c',Yline,imag(LineALEe.uy),':c',Yline,real(LineALEe.uya),'--c',Yline,imag(LineALEe.uya),'-..c')
figure(46);hold on;plot(Yline,real(LineALEe.ux),'-c',Yline,imag(LineALEe.ux),':c',Yline,real(LineALEe.uxa),'--c',Yline,imag(LineALEe.uxa),'-..c')


figure(45);legend('Uyrel,r','Uyrel,i','Uyabs,r','Uyabs,i')
figure(46);legend('Uxrel,r','Uxrel,i','Uxabs,r','Uxabs,i')

% plot of velocity fields
figure(15); subplot(4,1,1);SF_Plot(ffRR,'uxa','xlim',[-2 4],'ylim',[0 1.2],'title','RR (abs)');
subplot(4,1,2);SF_Plot(ffRR,'ux','xlim',[-2 4],'ylim',[0 1.2],'title','RR (rel)');
subplot(4,1,3);SF_Plot(ffALE,'uxa','xlim',[-2 4],'ylim',[0 1.2],'title','ALE (abs)');
subplot(4,1,4);SF_Plot(ffALE,'ux','xlim',[-2 4],'ylim',[0 1.2],'title','ALE (ALEfield)');

figure(16); subplot(4,1,1);SF_Plot(ffRR,'uya','xlim',[-2 4],'ylim',[0 1.2],'title','RR (abs)');
subplot(4,1,2);SF_Plot(ffRR,'uy','xlim',[-2 4],'ylim',[0 1.2],'title','RR (rel)');
subplot(4,1,3);SF_Plot(ffALE,'uya','xlim',[-2 4],'ylim',[0 1.2],'title','ALE (abs)');
subplot(4,1,4);SF_Plot(ffALE,'uy','xlim',[-2 4],'ylim',[0 1.2],'title','ALE (ALEfield)');


%% Validation on the case of a forced problem (omega=1) ; FINE MESH

ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',[-5 20 2],'Options','-density 100','problemtype','2d')
bf = SF_BaseFlow(ffmesh,'Re',25);
bf = SF_BaseFlow(ffmesh,'Re',60);

omega = 1;


% Reference with default solver (cf. Sabino et al)
ffref = SF_LinearForced(bf,'omega',omega); % This solver is incorrect for strong confinement 
Yline = linspace(0,max(bf.mesh.points(2,:)),200); 
Lineref = SF_ExtractData(ffref,'all',0,Yline);

% Relative velocities in relative frame
ffRR = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_work.edp','Options','-Vel R -Frame R -Normalize Y') 
LineRR = SF_ExtractData(ffRR,'all',0,Yline);

% Absolute velocities in relative frame 
ffAR = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_work.edp','Options','-Vel A -Frame R -Normalize Y') ;
LineAR = SF_ExtractData(ffAR,'all',0,Yline);

% Absolute velocities in absolute frame 
% (this one does NOT include added stress !)
ffAA = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_work.edp','Options','-Vel A -Frame A -Normalize Y')
LineAA = SF_ExtractData(ffAA,'all',0,Yline);

% ALE solver 
ffALE = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp') 
LineALE = SF_ExtractData(ffALE,'all',0,Yline);

% ALE solver with elasticity operator instead of Laplacian
ffALEe = SF_LinearForced(bf,'omega',omega,'solver','LinearForced2D_ALE.edp','Options','-ALEOP elasticity') 
LineALEe = SF_ExtractData(ffALEe,'all',0,Yline);


% Forces
[ ffref.Z, ffRR.Fy, ffAR.Fy, ffAA.Fy, ffALE.Fy,ffALE.Fy+ffALE.Fyxi,ffALEe.Fy+ffALEe.Fyxi]


%% SUMMARY



SF_Status


% [[PUBLISH]]