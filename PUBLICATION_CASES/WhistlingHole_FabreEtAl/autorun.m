function value = autorun(verbosity)
% Autorun function for StabFem. 
% This function will produce sample results for the case BLUNTBODY_IN_PIPE

if(nargin==0)
    verbosity=0;
end

SF_core_setopt('eigensolver','SLEPC');

if(verbosity==0)
if ~SF_core_detectlib('SLEPc-complex')
  SF_core_log('w', 'Autorun skipped because SLEPc is not available');
  value = -1;
  return;
end
end

close all;
SF_core_setopt('verbosity', verbosity);
SF_core_setopt('ffdatadir', './WORK_AUTORUN/');
SF_core_arborescence('cleanall');

% Test 1 :

%bf = SF_Load('lastadapted');
bf = SmartMesh_Hole(1);

% test : check if BF is correcty recovered


% Test 2 : impedance computation for Re = 1600

bf = SF_BaseFlow(bf,'Re',1600);
Params = [5 1e30 1.25 .5 20 1e30];
omega = 0.8;
foA = SF_LinearForced(bf,omega,'mappingdef','jet','mappingparams',Params);

Zref = -0.149604000000000 - 1.046030000000000i; %-0.1630 - 1.0387i


SFerror(1) = abs(foA.Z/Zref-1)
foA.Z


% test 3 with stability calculation

Params = [5 1e30 1.25 .5 20 1e30];
[ev,em] = SF_Stability(bf,'shift',-2.1i,'m',0,'nev',10,'type','D','mappingdef','jet','mappingparams',Params);

EVref = 0.0818136-2.10096i; %0.0874479 - 2.13519i;% 0.0819 - 2.1013i;

EVref = 0.0877198 - 2.09847i;
%0.079945000000000 - 2.098820000000000i ??
ev(1)

SFerror(2) = abs(ev(1)/EVref-1)
 

value = sum(SFerror>1e-2)

end
