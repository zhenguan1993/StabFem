%% Computation of the flow around a rotating cylinder using pseudo-arclength continuation
%
% This program reproduces figure 3(c) from Sierra et al (JFM, 2020)


%% Initialise
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity', 4,'ffdatadir', './WORK/');
SF_core_arborescence('cleanall');
SF_core_setopt('ErrorIfDiverge',false); % option recommended if netwon divergence is correctly caught.

%% Generation of initial solution and mesh

ffmesh = SF_Mesh('Mesh_Cylinder_FullDomain.edp','Params',[-40 80 40],'problemtype','2d'); 
bf=SF_BaseFlow(ffmesh,'Re',1,'Omegax',4.5);
for Re = [10, 60, 100 170]
    bf=SF_BaseFlow(bf,'Re',Re,'Omegax',4.5);
end
bf = SF_Adapt(bf,'anisomax',10,'Hmax',10);



%% Arclength continuation

alpha = bf.Omegax;
alphaMax = 5.5; stepMax = 0.1;
step = stepMax; 
FxList = []; FyList = []; AlphaList = [];

while ( bf.Omegax < alphaMax)
    bf=SF_BFContinue(bf,'step',step);
    % NB in this example we use the solver 'ArcLengthContinuation2D.edp'
    % which is the default one for this class of problems.
    while(bf.iter < 0)
        SF_core_log('w','Not converged. Arc-length step reduced by a factor of 2');
        step = 0.5*step
        bf=SF_BFContinue(bf,'step',step);
        if (abs(step)<stepMax/100)
            warning('step too small')
            continue;
        end
    end
    step = sign(step)*min(1.4*abs(step),stepMax);
    FxList = [FxList, bf.Fx];
    FyList = [FyList, bf.Fy];
    AlphaList = [AlphaList, bf.Omegax];
end


%% Plot
% Fy
h = figure;
subplot(1,2,1);
plot(AlphaList,FxList,'ok-','linewidth',2)
set(gca,'TickLabelInterpreter','latex','fontsize',24);
xlabel('$\alpha$','interpreter','Latex');
ylabel('$F_x$','interpreter','Latex');

set(gcf, 'Color', 'w');

subplot(1,2,2);
plot(AlphaList,FyList,'ok-','linewidth',2)
set(gca,'TickLabelInterpreter','latex','fontsize',24);
xlabel('$\alpha$','interpreter','Latex');
ylabel('$F_y$','interpreter','Latex');
set(gcf, 'Color', 'w');


% Resize automatically for saving
fig = gcf;
fig.PaperPositionMode = 'auto'
fig_pos = fig.PaperPosition;
fig.PaperSize = [fig_pos(3) fig_pos(4)];
set(fig,'Units','Inches');
print(fig,'FyAlpha','-dpdf','-bestfit');



%% Visualization
index = 20;
bf = SF_Load('BASEFLOWS',index);
bf.Omegax
figure;
Xmin = -2; Xmax = 2; Ymin = -2; Ymax = 4;
SF_Plot(bf,'vort','xlim',[Xmin,Xmax],'ylim',[Ymin,Ymax]);
hold on;
SF_Plot(bf,'psi','contour','only','CLevels',[-10.0:0.05:10],...
    'xlim',[Xmin,Xmax],'ylim',[Ymin,Ymax]);
%% Stability calculation 
[ev,em] = SF_Stability(bf,'shift',0.1,'nev',5,'plotspectrum',true);

%% Summary
SF_Status

% [[PUBLISH]]
