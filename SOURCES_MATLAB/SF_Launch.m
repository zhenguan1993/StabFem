function Data = SF_Launch(file, varargin)
% generic Matlab/FreeFem driver
%
% usage : mesh = SF_Launch('File.edp', {Param1, Value1, Param2, Value2, etc...})
%
% First argument must be a valid FreeFem++ script
%
% Couples of optional parameters/values comprise :
%   'Options' -> Optional arguments passed to
%           Freefem and interpreted with getARGV.
%    Three methods : 
%      1/   A list of descriptor/value pairs
%           Example : 
%              SF_Launch('File.edp','Options',{'option1' 10 'option2' 20})
%              will launch 'FreeFem++ File.edp -option1 10 -option2 20'
%      2/   A structure
%           Example :
%               params.option1 = 10; params.option2 = 20;
%               SF_Launch('File.edp','Options',params)
%      3/   A string containing the optional arguments.
%           Example :
%               SF_Launch('File.edp','Options','-option1 10 -option2 20')
%      
%   'Params' -> an array of (numerical) input parameters for the FreeFem++ script
%           for instance SF_Launch('File.edp','Params',[10 100]) will be
%           equivalent to running 'FreeFem++ File.edp' and entering successively
%           10 and 100 through the keyboard)
%   'Mesh' -> a mesh associated to the data
%           (either a mesh struct or the name of a file)
%   'BaseFlow' -> a baseflow associated to the data
%           (either a flowfield struct or the name of a file)
%           (NB if providing a baseflow it is not ncessary to provide a
%           mesh as themesh is a field of the baseflow object)
%   'DataFile' -> the name of the resulting file to be imported (default is
%   Data.txt)
%   'Store' -> a string corresponding to the name of the subfolder where
%                 the data files should be stored
%
%   'Type' -> a string specifying the type of computation for the FreeFe++ script (OBSOLETE ?)
%
%
%
% by D. Fabre, june 2017, redesigned dec. 2018 then may-june 2020 then sept 2020.
%

% parameters management
p = inputParser;
addParameter(p, 'Params', []);
addParameter(p,'Options','');
addParameter(p, 'Mesh', 0);
addParameter(p, 'Init', 0);
addParameter(p, 'Forcing', 0);
addParameter(p, 'BaseFlow', 0);
addParameter(p, 'DataFile', 'Data.ff2m');
addParameter(p, 'Store', '');
addParameter(p, 'Type', 'none');
parse(p, varargin{:});

% optional arguments for FreeFem
ffargument = p.Results.Options;
ffargument = SF_options2str(ffargument); % transforms into a string

% mesh 
if (isstruct(p.Results.Mesh))
    SF_core_log('d', 'Mesh passed as structure');
    ffmesh = p.Results.Mesh;
    meshname = SFcore_MoveDataFiles(ffmesh.filename,'mesh.msh','cp');
end

% Baseflow
if (isstruct(p.Results.BaseFlow))
    SF_core_log('d', 'Baseflow passed as structure');
    bf = p.Results.BaseFlow;
    SFcore_MoveDataFiles(bf.filename,'BaseFlow.txt','cp');
    if ~exist('ffmesh') 
        ffmesh = bf.mesh;
        SFcore_MoveDataFiles(bf.mesh.filename,'mesh.msh','cp');
    end
end

% Initial
if (isstruct(p.Results.Init))
    SF_core_log('d', 'Starting dataset passed as structure');
    bf = p.Results.Init;
    SFcore_MoveDataFiles(bf.filename,'dnsfield_start.txt','cp'); % expected name for DNS solvers
    SFcore_MoveDataFiles(bf.filename,'BaseFlow_guess.txt','cp'); % expected name for Newton solvers
    if ~exist('ffmesh') 
        ffmesh = bf.mesh;
        SFcore_MoveDataFiles(bf.mesh.filename,'mesh.msh','cp');
    end
else
    SF_core_syscommand('rm',[SF_core_getopt('ffdatadir'),'/','BaseFlow_guess.txt']); % TRICK to avoid a bug
end

% Forcing
if (isstruct(p.Results.Forcing))
    SF_core_log('d', 'Forcing passed as structure');
    forcing = p.Results.Forcing;
    SFcore_MoveDataFiles(forcing.filename,'Forcing.txt','cp');
end



%ffdir = SF_core_getopt('ffdir'); % to remove soon
%ffdirPRIVATE = SF_core_getopt('ffdirPRIVATE');

SF_core_log('nn', ['### Starting SF_Launch ', file]);

stringparam = [];
if (~strcmpi(p.Results.Type,'none'))
        stringparam = [p.Results.Type '  '];
end

%if ~(exist(file,'file'))
%    for ffdir = 
%    if(exist([ffdir file],'file'))
%        file = [ffdir file];
%    elseif(exist([ffdirPRIVATE file],'file'))
%        file = [ffdirPRIVATE file];
%    else
%        error([' Error in SF_Launch : FreeFem++ program ' ,file, ' not found']);
%    end
%end

stringparam = ' ';
if ~isempty(p.Results.Params)
    for pp = p.Results.Params
        if isnumeric(pp)
            stringparam = [stringparam, num2str(pp), '  '];
        else
            stringparam = [stringparam, pp, '  '];
        end
    end
end

% Launch FreeFem
value = SF_core_freefem(file,'parameters',stringparam,'arguments',ffargument);

% Checks the results file
if exist([SF_core_getopt('ffdatadir'),p.Results.DataFile],'file')
        theDataFile = p.Results.DataFile;
elseif exist([SF_core_getopt('ffdatadir'),'BaseFlow.txt'],'file')
        theDataFile = 'BaseFlow.txt';
        SF_core_log('w',['expected file ',p.Results.DataFile,' not found ; instead I found a file BaseFlow.ff2m']);
else
        SF_core_log('e',' You must provide a datafile name using option DataFile');
end

%% If requested : copy file in subfolder of database

if ~isempty(p.Results.Store)
    if ~exist([SF_core_getopt('ffdatadir'),'/',p.Results.Store],'dir')
       SF_core_log('w',[' Folder ' SF_core_getopt('ffdatadir'),'/',...
           p.Results.Store, ' mentioned for database storing does not exist ; creating it']);
       mkdir([SF_core_getopt('ffdatadir'),'/',p.Results.Store])
    end
    SF_core_log('N',['Storing result in folder ',p.Results.Store]);
        
    SFcore_AddMESHFilenameToFF2M(theDataFile,ffmesh.filename);    
    filename = SFcore_MoveDataFiles(theDataFile,p.Results.Store);
else
    filename = theDataFile;
end

% Import results

hadError = 0;
if ~hadError
    if isnumeric(p.Results.Mesh)&&isnumeric(p.Results.BaseFlow)
        Data = SFcore_ImportData(filename);
    else
        Data = SFcore_ImportData(ffmesh, filename);
    end
else
    Data = [];
end


end

function arg = argumentcell2str(cell)

if mod(length(cell),2)==1
    SF_core_log('e', 'must provide a list of pairs descriptor/value')
end

numel = length(cell)/2
arg = ' ';
for i=1:numel
    if ~ischar(cell{2*i-1})
        SF_core_log('e','In argument : descriptors must be char type')
    end    
    arg = [ arg, '-',cell{2*i-1},' '];
    if ischar(cell{2*i})||isstring(cell{2*i-1})
         arg = [ arg, cell{2*i},' '];
    elseif isnumeric(cell{2*i})
        arg = [ arg, num2str(cell{2*i}),' '];
    else
        SF_core_log('e','In argument : value must be char or numeric')
    end 
end

end