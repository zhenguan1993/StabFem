function bfM = SF_Mirror(bf)
%
% transforms a half mesh into a full mesh
%
%
% This is part of the StabFem project, copyright D. Fabre, jkuly 2018.
%
% NB "backups in case of failure" are probably not useful anymore thanks
% to the new method to position entry files. To be checked and probably
% removed...
%
%global ff ffdir ffdatadir sfdir verbosity
%ffdatadir = SF_core_getopt('ffdatadir');

halfmesh = bf.mesh;

 SF_core_log('d', 'FUNCTION SF_Mirror : mirroring mesh for single mesh');
 SFcore_MoveDataFiles(halfmesh.filename, 'mesh.msh','cp');
 SFcore_MoveDataFiles(bf.filename,'BaseFlow.txt','cp');
% launch ff++ code
SF_core_freefem('MirrorMesh.edp');

%SF_core_arborescence('clean');
     

meshfilename = SFcore_MoveDataFiles('mesh_mirror.msh','MESHES');
ffmesh = SFcore_ImportMesh(meshfilename,'problemtype',halfmesh.problemtype);
ffmesh.symmetry = 'N';

% info
SF_core_log('n', '      ### MIRROR MESH : ');
SF_core_log('n', ['      #   Number of points np = ', num2str(ffmesh.np)] ); 


%myrm([ffdatadir, 'BaseFlow_guess.txt']);
%myrm([ffdatadir, 'BASEFLOWS/*']);
 
%%     


% database management 
    SFcore_AddMESHFilenameToFF2M('BaseFlow_mirror.ff2m',meshfilename);
    finalname = SFcore_MoveDataFiles('BaseFlow_mirror.ff2m','MESHES','cp');
    
% import    
    bfM = SFcore_ImportData(finalname);
    

% tweaks   
    if isfield(bf,'solver') 
        bfM.solver = bf.solver;
    end
    bfM.Symmetry = 'N';

    
    
%    SF_core_log('n',' SF_Mirror : recomputing base flow');
%    baseflowNew = SF_BaseFlow(bfMirrored, 'type', 'POSTADAPT'); 
%     if (baseflowNew.iter > 0)
%    finalname = SFcore_MoveDataFiles(baseflowNew.filename,'MESHES');
%    baseflowNew.filename = finalname;
%    bfM = baseflowNew; 
     
     % after adapt we clean the "BASEFLOWS" directory and other directories 
     %as the previous baseflows are no longer compatible => Now done in SF_BaseFlow ??? 

     %SFcore_CleanDir('POSTADAPT'); 
%    SF_core_arborescence('clean');
     
     
%     newfilename = SFcore_MoveDataFiles('BaseFlow.txt',['BASEFLOWS/BaseFlow',des'.txt']);
%     else
%         error('ERROR in SF_Adapt : baseflow recomputation failed');
%     end
 % eventually clean working directory from temporary files
 SF_core_arborescence('cleantmpfiles')
end
