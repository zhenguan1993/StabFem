
% !TEX root = main.tex

\chapter{Basic drivers and explanation of the interface}



The purpose of this chapter is to demonstrate the usage of \stabfem as a Matlab/Octave interface to \freefem, 
allowing to monitor computations, import and plot results from Matlab/Octave.

The chapter will present the fundamental \stabfem drivers, namely \SF{Mesh},  \SF{Launch},  \SF{Plot} and \SF{ExtractData}, and will explain the usage of the exchange files  (.ff2m) allowing importation of the results.

For the purpose of the demonstration, we focus on four simple physical problems formulated in a L-shaped closed domain. All the operations are enclose in the  tutorial example \mlfile{EXAMPLE\_Lshape.m} which is published on the website of the project. A simplified (but still operational) version if this script is provided in figure \ref{fig:SCRIPT_Lshape.m}.

The reader is encouraged to study the code in parallel to the lecture of this chapter, and to try by his own all the steps. 

%This directory contains three freefem programs and a demo matlab script doing solving the following problem :

\begin{figure}
\includegraphics[width = .45 \linewidth]{../../STABLE_CASES/EXAMPLE_Lshape/FIGURES/Lshape_T0.png}
\includegraphics[width = .45 \linewidth]{../../STABLE_CASES/EXAMPLE_Lshape/FIGURES/Lshape_T0_Cut.png}
\caption{Solution for the steady heat equation in a L-shape domain : $(a)$ Temperature field $T(x,y)$. 
$(b)$ Temperature $T(x,0.25)$ along a horizontal line. }
\label{Lshape_Problem1}
\end{figure}

\begin{figure}
\setlistmatlab
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/SCRIPT_Lshape_summary.m}
\caption{
Matlab program \mlfile{SCRIPT\_Lshape.m} (simplified version)}
\label{fig:SCRIPT_Lshape.m}
\end{figure}


\section{Physical problems and results}



After initialization (lines 5-6 of the script) and generation of a mesh (lines 9-10), the four following problems are successively solved:

\subsubsection*{Physical problem 1} 
Steady heat conduction on a L-shaped domain with constant volumic source term $P$ and homogeneous Dirichlet boundary conditions. 

$$ \nabla^2 T = P  \mbox {  for } {\bf x} \in \Omega ; \quad T = 0   \mbox {  for } {\bf x} \in \Gamma$$

This problem is solved on lines 13-18 of the script and the results are displayed in figure \ref{Lshape_Problem1}.

\begin{figure}
\includegraphics[width = .32 \linewidth]{../../STABLE_CASES/EXAMPLE_Lshape/FIGURES/Lshape_Tc.png}
\includegraphics[width = .32 \linewidth]{../../STABLE_CASES/EXAMPLE_Lshape/FIGURES/Z_omega.png}
\includegraphics[width = .32 \linewidth]{../../STABLE_CASES/EXAMPLE_Lshape/FIGURES/StokesFlow.png}
\caption{$(a)$ Solution for the unsteady heat equation in a L-shape domain for $\omega =100$. $(b)$ Thermal impedance for  $\omega \in [0.1-10000]$. $(c)$ Stokes flow.}
\label{Lshape_problem2}
\end{figure}

\subsubsection*{Physical problem 2} Unsteady heat conduction equations on a L-shaped domain with zero source term $P$ and time-periodic Dirichlet boundary conditions.

$$ 
 \partial T/\partial t= \nabla^2 T  \mbox {  for } {\bf x} \in \Omega ; \quad T = T_w \cos (\omega t )   \mbox {  for } {\bf x} \in \Gamma
$$

The problem can be solved in the frequency domain by setting 
$T(x,y,t) = Re \left( \hat{T}(x,y) e^{-i \omega t} \right)$ , leading to: 
$$ 
 - i \omega \hat{T}= \nabla^2 \hat{T}  \mbox {  for } {\bf x} \in \Omega ; \quad \hat{T} = T_w    \mbox {  for } {\bf x} \in \Gamma
$$

This problem is solved on lines 21-27 of the script and the results for $\omega = 100$ are displayed in figure \ref{Lshape_problem2}$a$.





\subsubsection*{Physical problem 3} Thermal impedance of the L-shaped domain as function of $\omega$.

This problem is the same as the previous one, except that instead of plotting $T$ for a given value of $\omega$ we want to characterize the behavior in the unsteady regime by defining the { \em Thermal impedance} corresponding to

$$ Z_\theta = \frac{T_w}{\hat{\phi}} $$ 

where $\hat \phi$ is the complex heat flux given by $\hat \phi = \int_\partial \Omega \nabla \hat{T} \cdot {\bf n} $. The program computes $Z_\theta$ in the range $\omega  \in [10^{-1} - 10^4]$ and plots the results in logarithmic coordinates.
The problem is solved on lines 29-32 of the scripts and results are shown in figure \ref{Lshape_problem2}$b$.



\subsubsection*{Physical problem 4} Stokes flow with lid-driven cavity.

Here we compute the velocity field ${\bf u} = [u_x,u_y]$ and the pressure field $p$ of a viscous flow in the Stokes regime, with imposed velocity at the bottom boundary $\Gamma_b$:
 $$
 - \nabla p + \Delta {\bf u} =0 ;\quad \nabla \cdot {\bf u} = 0
 $$
 
  ${\bf u} = x(1-x) {\bf e}_x$ on bottom, and ${\bf u} = {\bf 0}$ on other boundaries.
  

The problem is solved on lines 24-38 of the scripts and results are shown in figure \ref{Lshape_problem2}$c$.




\section{Usage of the drivers}

In this section, we explain how the drivers are used to interface the various \freefem programs entering this example, \fffile{Lshape\_Mesh.edp}, \fffile{Lshape\_Steady.edp}, etc... and to plot the results.

The \freefem programs are sufficiently short to be fully given in these pages. See figures \ref{Lshape_Mesh.edp}, \ref{Lshape_Steady.edp} and subsequent. The reader already familiar with FreeFm++ should not have any problem in understanding the programs. If not, we recommand that you study the \freefem Manual (chapter "Learning by examples").


\subsection{ Mesh generation : driver \SF{Mesh}}
\label{sec:MeshBasic}

Let us explain in more detail the very first step of nearly all \stabfem computations : Generation of a mesh. This step is handled using the fundamental driver \SF{Mesh}. 
As can be seen on line  10 , the first argument of this function is the name of a valid \freefem script generating the mesh ; here  \fffile{Lshape\_Mesh.edp}. Next, a single optional parameter \mlcode{'meshdensity'} with value 40
is specified. This parameter is passed as optional parameter to the FreeFem program and detected through \ffcode{getARGV} : in the present case the sequence is equivalent to launching \shell{FreeFem++ Lshape\_Mesh.edp -density 40} when using FreeFem in shell terminal mode.

%The \mlcode{'Params'}, specifies a parameter to be passed as input paramer to the FreeFem program.
%In the present case, the sequence is equivalent to launching \shell{echo 40 | FreeFem++ Lshape\_Mesh.edp}\footnote{Here and in the}  when using FreeFem in shell terminal mode.

%The second parameter \mlcode{'problemtype'}, specifies the class of problems for which the mesh is designed. The value used here \mlcode{'EXAMPLE'} is not important, but as will be seen in the next chapters, the value of this parameter is used by higher-order drivers to select the right solvers to use.

\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Lshape_Mesh.edp}
\caption{ \freefem program \fffile{Lshape\_Mesh.edp}. }
\label{Lshape_Mesh.edp}
\end{figure*}


%The Freefem code \fffile{Lshape\_Mesh.edp} is displayed in figure \ref{Lshape_Mesh.edp} (up). This program generates a output file \fffile{mesh.msh} in the usual \freefem format, as well as two additional files in .ff2m format, generated by two macros (defined in a file \fffile{SF\_Tools.idp} included at the beginning). The significance of these exchange files will be explained in a subsequent chapter.


\Remarks{ 
\item The return value of the driver \SF{Mesh} is a {\em handle} to the mesh (i.e. a structure indicating the names of the underlying files, etc...)
\item This FreeFem program, and the next ones of the present chapters, require a few macros (here \ffcode{SFWriteMesh},  \ffcode{SFWriteConectivity}). In the pedagogical example of this chapter, these macros (and a few others) are defined in a file \fffile{SF\_Tools\_simplified.idp} located in the working directory. In the more advanced examples of the next chapters, such macros are either in a common file \fffile{SOURCES\_FREEFEM/INCLUDE/SF\_Tools.idp} (for generic macros) or in a local file \fffile{SF\_Custom.idp} (for customizable macros).
\item A more complete explanation of the driver \SF{Mesh} (syntax, optional parameters, associated macros) will be done in chapter \ref{chap:mesh}.
}



%Running this program through the driver \SF{Mesh} produces the result displayed in figure  \ref{Lshape_Mesh.edp} (bottom). As can be seen, the result is a Matlab/Octave structure with a number of field, including array of points, descriptors, etc... These fields will serve for plotting data obtained on this mesh. See more in section ??.



\subsection{ Running a FreeFem computation using a mesh : driver \SF{Launch}}
\label{sec:SFLaunch}


 The second step is {\bf launching a FreeFem program and importing results}. 
Both these steps are done using the generic driver function \SF{Launch} which is invoked four times in the main script. This driver is used in the same way as the previous one, i.e. the first argument is a valid \freefem program and the sequel is formed by pairs of descriptor/value optional arguments. 

The general syntax of \SF{Launch} is as follows:
\setlistmatlab
\lstinputlisting{SF_Launch_Syntax.txt}


The return value of the driver \SF{Launch} is a {\em handle} (or an array of handles) to the dataset generated by your program; namely, a structure with fields containing the names of the output files generated by FreeFem, the plotable data, the metadata used to organise the database, etc...

The possible optional arguments are as follows:


\begin{itemize}

\item \mlcode{'Mesh'} is used to specify the mesh on which the calculation will be performed. 
The value is a handle to a previously generated Mesh, and the corresponding .msh file will be automatically positioned for correct operation. Optionally, the value can also be directly the name of the .msh file.

\item \mlcode{'Initial'} can be used in a similar way for FreeFem++ programs requiring a starting point (for instance DNS time-steppers which require an initial condition, or Newton solvers which require a 'guess'). 
The value is a handle to a previously generated DataSet.  
Alternatively you may also specify directly the name of a FreeFem dataset file(.txt).

NB : In the same way, and according to the needs of your program, you may specify a \mlcode{'BaseFlow'} object (for instance programs solving linearized problems in fluid dynamics) or a \mlcode{'Forcing'} object.


\item  \mlcode{'Options'} is used to pass optional arguments to the FreeFem program. Three ways to pass the parameters are possible:

\begin{description}
\item{$(i)$} 'Options' can be a cell array of descriptor/value pairs.

For example, \SF{Launch('Lshape\_Stokes.edp','Options',\{'nu',1,'Umax',2\})}.

\item{$(ii)$} 'Options' can be a structure.

For example, typing \mlcode{ StokesOptions.nu = 1; StokesOptions.Umax = 2;} followed by

\SF{Launch('Lshape\_Stokes.edp','Options',StokesOptions)} will produce the same result.

\item{$(iii)$} Alternatively, the value of 'Options' can be directly a string. 

For instance   \SF{Launch('Lshape\_Stokes.edp','Options','-nu 1 -Umax 2')} is equivalent to the two previous syntaxes.

\end{description}

All three methods will result in launching \shell{FreeFem++ Lshape\_Stokes.edp -nu 1 -Umax 2} in shell terminal mode. 


The optional arguments passed in this way have to be detected using  \ffcode{getARGV} inside of your FreeFem programs. See the various examples in this chapter and the FreeFem manual for explanations. Note that both numerical and alphanumeric values are detected. For instance a syntax such as 
\SF{Launch('MyProgram.edp','Options',\{'nu',1,'bctype','dirichlet'\}} if your programs expects using \ffcode{getARGV} a string-valued parameter 'bctype'.



%For instance \SF{Launch('Mysolver.edp','Arguments','-Re 100 -M .1')} sing the \ffcode{getARGV} FreeFem command (see Freefem manual for explanations). 


 
\item  \mlcode{'Params'} (not used in the present examples) is an alternative method
 to specify the input parameters needed by the FreeFem solver using 'piping' through the standard input.
The value may be an array of real values. For instance \SF{Launch('Mysolver.edp','Params',[1 2 3])} will result in launching \shell{echo 1 2 3 | FreeFem++ Mysolver.edp} in shell terminal mode.

This method can be found in some of the outdated examples of the project but is not recommended any more ; it is now recommended to use 'Options' instead as explained above.


\item  \mlcode{'Datafile'} specifies the name of the data files generated by the FreeFem program to be imported. %The latter are expected as a couple of .txt / .ff2m files. 
If this parameter is not specified (as in the first call to \fffile{Lshape\_Steady.edp} in the present example), the files are expected with default  names \shell{Data.txt} / \shell{Data.ff2m}.

 
Note : If \mlcode{'Datafile'} contains a sharp character, multiple datafiles are expected to be imported, and the results will be returned as an array or DataSets. For instance \mlcode{results = }\\ \SF{Launch('Mysolver.edp','DataFile','Data\_\#.txt')} will look for files called \shell{Data\_000001.txt},  
 \shell{Data\_000002.txt},   \shell{Data\_000003.txt},  etc..
{\em (feature still to be implemented).}

\item \mlcode{'Store'} option can be used to specify the name of a "database" folder where a backup of the generated data will be stored. 
%For instance, at the end of the online version of the present exemple, option  \mlcode{'Store','Thermal'} is used so that \shell{Data.txt} / \shell{Data.ff2m} will be copied as \shell{./WORK/Thermal/FFDATA\_000001.txt} / \shell{./WORK/Thermal/FFDATA\_000001.ff2m}.
%Using  \mlcode{'Store'} allows to reload all your data, for instance for postprocessing of your results. 
See section \ref{sec:DB} and the numerous examples available online for more explanations on database management.

\end{itemize}

\Remarks{ 
\item In this simple explanation, the \freefem executable is assumed to be \shell{FreeFem++} and to be available in the current path. In reality, the executable can be specified (the default being \shell{FreeFem++-mpi} if available with the installed version of \freefem ) and the corresponding path is automatically detected. These initial settings are managed by \mlfile{SF\_Start}  which is expected to be invoked at the beginning of every \stabfem script.

\item The FreeFem codes (.edp files) specified when calling the drivers are expected to be present either in the current directory (\shell{./}), or in the common directory \shell{~/StabFem/SOURCES/\_FREEFEM}. The drivers generate a file \shell{freefem++.pref} specifying these default paths (and a few more things required for correct operation).

\item If providing an initial condition through \mlcode{'Initial'},  it is not necessary to specify a  \mlcode{'Mesh'}; since information on the mesh is already contained in the handle to the Initial dataset.


\item The data files read or written by FreeFem programs (\shell{mesh.msh}, \shell{Data.txt}, etc...) are expected to be in a directory called \ffcode{ffdatadir} in FreeFem programs. By default this is \shell{./WORK/} but a different value can be specified when initializing with \SF{Start}. The name of the folder will be written in a file \shell{workdir.pref} written by the drivers.  

\item Note that the two successive actions of \SF{Launch}, namely launching FreeFem++ and importing results, are performed by functions \mlfile{SF\_core\_freefem} and  \mlfile{SFcore\_ImportData}.  You may look inside these functions to understand their operation, but it is not advised to use them directly in a script.


}




%%%%%%%%%%%%%%%%
\subsection{ Plotting results : driver \SF{Plot}}
\label{sec:PlotBasic}

The next important step is plotting the results. A variety of plots can be produced in StabFem, and all of them are managed by the unique driver \mlcode{SF\_Plot}. Examination of the script shows that this driver has to be used with the following set of arguments:

\begin{enumerate}

\item First argument is a handle to a valid dataset or mesh.

\item Second argument is the name of a field of the dataset (possibly followed by the suffixes '.re' or '.im' to select the real or imaginary parts of a complex field) or the keyword 'mesh' (to plot mesh only). Note that we can also specify a cell-array of two names (e.g. \mlcode{\{'ux','uy'\} } in example 4) in which case a vector field is plotted.

\item The next arguments are optional ones passed as pairs or descriptor/value. The options used here comprise:

\begin{itemize}
\item \mlcode{'contour'} to plot isocontours in addition to color levels (with value 'on') or instead of color levels (with value 'only').

\item \mlcode{'xlim'} and  \mlcode{'ylim'}  to specify ranges,

\item \mlcode{'title'} to specify a title,

\item \mlcode{'colormap'} to specify the color map,

\item (...) 

\end{itemize}

A large number of other options are available and a full list will not be given here. 
The user is encouraged to explore the numerous examples of the project to learn about all possibilities.
A documentation can also be generated using \mlcode{'help SF\_Plot' } .


\end{enumerate}

\Remark{ The driver \SF{Plot} is built upon the function \mlcode{ffpdeplot} designed by Markus Meisinger. See the website of this project for more documentation.}


\subsection{ Extracting results from mesh-associated data : driver \SF{ExtractData}}

For post-processing your results, you will certainly need to access to the value of the mesh-associated fields at a given point, or a given series of points. 
The driver \SF{ExtractData} is designed for this purpose. This function is called at line 17 of our main script. We can see that the general usage is as follows:

\mlcode{ values = SF\_ExtractData(bf,field,x,y) }

Where :
\begin{itemize}
\item \mlcode{bf} is a valid dataset.
\item \mlcode{field} is the name of a mesh-associated field of this dataset (here \mlcode{'T'}) or the keyword \mlcode{'all'}.
\item \mlcode{x} and \mlcode{y} are the coordinated on which the field is to be interpolated.  \mlcode{x} and \mlcode{y} are normally expected as arrays with same dimensions ; the return value \mlcode{values} will have the same dimension.

Note that the routine will also work if \mlcode{x} is an array and \mlcode{y} is a single value; in this case the extraction will be done on a horizontal line.
Same for a vertical line if \mlcode{y} is an array and \mlcode{x} is a single value.

\end{itemize}

Note that the driver \SF{ExtractData} is built upon the function \mlcode{ffinterpolate} designed by Markus Meisinger. Part of this function contains a .mex file which can be compiled, leading to substantial speed-up of the routine. See the documentation on the website of ffmatlib about this.

In your work you will certainly need some post-processing results which are not easily extracted from the mesh-associated data (e.g. the heat flux across the whole boundary for the thermal problems, the force exerted on the moving wall for the Stokes flow problem, etc...). This second kind of pos-processing has to be incorporated directly in the FreeFem codes and the importation will be done from the  .txt/.ff2m. exchange files. 
This will be the object of the next section.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{How does is work ?}


\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Lshape_Steady.edp}
\setlistmatlab
\lstinputlisting[linerange={25-37}]{Lshape_ExecutionExample.txt}
\caption{
\freefem program \mlfile{Lshape\_Steady.edp} (up) and result of the execution of this program through the driver\SF{Launch} (bottom).  }
\label{Lshape_Steady.edp}
\end{figure*}




The FreeFem++ programs for the four elementary problems as well as the result of their execution through \SF{Launch} are presented in figures \ref{Lshape_Steady.edp}-\ref{Lshape_Stokes.edp}.

As can be seen all the files produce a couple of \shell{.txt/.ff2m} files which are imported by the drivers.
The role and structure of these files are as follows:
\begin{itemize}
\item The \shell{.txt} contains the main results of the computation exported under the default (ascii) output mode of FreeFem++, namely the mesh-associated fields (here \ffcode{T,u,v,p}) and/or scalar or vectorial numerical data (here \ffcode{omega,Zr,Zi}. These files can thus be subsequently used either as input files for next FreeFem++ computations, or for importation by StabFem.

\item The  \shell{.ff2m} may contain {\em Explanations for the drivers on how to import/interpret the \shell{.txt} files}, plus  {\em auxiliary data} not contained in the .txt file but useful for post-processing, and  {\em metadata} useful for indexing the data. 
 \end{itemize}
 
 
 
 \begin{table}
\begin{tabular}{|l|l|}
\hline
\ffcode{datatype} \qquad   & Type of data (e.g. BaseFlow, Eigenmode, etc...) \\
\ffcode{datastoragemode} &  Finite-element structure (e.g. ReP2P2P1) or keyword  \ffcode{'columns'} \\ 
\ffcode{datadescriptors}    &  Name of the components of the FE data (e.g. \ffcode{ux,uy,p}) or of the columns (see text). \\
\ffcode{meshfilename}*  &  Name of the file of associated mesh \\
\ffcode{baseflowfilename}* \qquad  &  Name of the file of associated base flow \\
\ffcode{solver}** & Name of the FreeFem+++ solver which generated this file \\	
\hline
\end{tabular}
\caption{List and significance of keywords which may appear on line 3 of a .ff2m file. Keywords with a a star are normally not written by the FreeFem programs but appended by the drivers when using data base management. 
}
\label{tab:keyword}
\end{table}
 
\subsection{Explanation of the \shell{.ff2m} exchange format} 

The \shell{.ff2m} format is an exchange format specifically designed for StabFem. As can be inferred from the examples, the general structure of such files is as follows:



\setlistfreefem
\lstinputlisting{Lshape_FormatExplanation.ff2m}.

It is thus constituted of a four-lines header, optionally followed by a set of numerical data. The signification of the header is as follows :

\begin{itemize}
\item Line 1 is skipped.
\item Line 2 contains a description string which is imported as field 'datadescription'.
\item Line 3 contains a list of string-valued keywords coming by pairs of descriptors/values 
{\em explaining how to interpret the .txt file}. A list of currently recognized keyword is given in table \ref{tab:keyword}
 but you can your own keywords to adapt to your needs.

%\ffcode{datatype} specifies the kind of data (this parameter may affect the operation of some of the drivers). 
%The signification of the two next keywords \ffcode{datastoragemode} and \ffcode{datadescriptors}  is explained in next paragraph.



\item Line 4 gives the list of auxiliary data or metadata variables, coming by pairs of variable type/variable name. 
\item The sequel contains the numerical data forming the auxiliary and metadata, provided as a unique array of real values.
\end{itemize}

\subsubsection{Importation main data from the .txt file} 

The importation of the .txt file depends on the keywords  \ffcode{datastoragemode} and \ffcode{datadescriptors} provided on line 3. Their significance vary depending if the data is {\em mesh-associated} or {\em non-mesh associate}.

\begin{itemize}

\item For {\bf Non-mesh-associated data}, \ffcode{datastoragemode} is obligatorily \ffcode{'columns'} meaning that data in .txt is organized in columns. Then the keywords \ffcode{datadescriptors} is a list of descriptors separated by commas specifying the names under which the data will be imported.

For instance \ffcode{datastoragemode columns datadescriptors Re,Fx} means the data is organized as two columns which will be imported as arrays \mlcode{Re} and \mlcode{Fx}. 

Note in this mode the data in the .txt file must contain only real data, so complex data have to be separated as real and imaginary parts. if the list of descriptors contains a pair of names with \ffcode{\_r} and \ffcode{\_i}  then the driver will reassociate the data as a complex one. 

For instance \ffcode{datastoragemode columns datadescriptors omega,Z\_r,Z\_i} means that the .txt file contains 3 columns and that the two latter will be reassembled as a complex array with name \mlcode{Z}.



\item For {\bf Mesh-associated data},  \ffcode{datastoragemode} contains a prefix (\ffcode{Re} or \ffcode{Cx})  explaining if the data are real or complex, followed by a description of the finite elements used (\ffcode{P1},\ffcode{P2},...), and possibly a suffix \ffcode{'.(n)'}. This suffix explains that the \shell{.txt} file contains a number $n$ of scalar values (real or complex) to be read after the mesh-associated data. Then the keyword \ffcode{datadescriptors} is a list of descriptors separated by commas specifying the name under which the data (mesh-associated ones and extra scalars) 



For instance, \ffcode{datastoragemode CxP2 datadescriptors Tc}  means that the .txt file contains a complex mesh-associated field defined using P2 elements which will be imported under the name \mlcode{Tc}.

 As a second example  \ffcode{datastoragemode ReP2P2P1.1 datadescriptors ux,uy,p,Re} means that the .txt file contains a (real) vectorized field whose components (stored as P2,P2,P1) will be imported under names \mlcode{ux}, \mlcode{uy} and \mlcode{p} plus an additional scalar which will be imported under name \mlcode{Re}.  

Note that unlike for non-mesh-associated data, complex data as expected to we written in the .txt file under the form \ffcode{(re,im)} following the default output mode of \freefem. If present, additional scalar datas are treated in the same way (i.e. if the main data is complex, so are the scalars).

\end{itemize}

\subsubsection{Importation of 'auxiliary data' and 'metadata' from the .ff2m file} 

auxiliary data and metadata are defined by pairs of type/names on line 4 of the header. There are four kinds of data :

\begin{enumerate}

\item {\em Scalar auxiliary data} comprises the following types :

\ffcode{ real, complex, int }

They will be imported as simple scalars identified with the specified name.


\item {\em Scalar metadata} are specified as follows:
\ffcode{ real*, complex*, int* }

They are handled as the simple scalar data, except that they are used by the high-level drivers to index the data and allow  
powerful exploration of the database while postprocessing.

{\em NB : for correct operation the metadata must be specified first and must appear before auxiliary data. The reason is that some operations done by the drivers (e.g. generation of the summary of the data base by \SF{Status}) will read only the beginning of the .ff2m files to import only the metadata.}



\item {\em Vectorial auxiliary data} are specified as follows:

\ffcode{ real.(N), complex.(N), int.(N) } 

They are imported as arrays  under the specified name.

\item{ Mesh-associated auxiliary data} are extra field derived from the main data which are potentially useful for post-processing but are not needed for subsequent calls to FreeFem (hence they do not need to be in the .txt file).

The following types are currently available:
\begin{itemize}
\item \ffcode{P1},  \ffcode{P1b} and  \ffcode{P2}  refer to standard finite-element formats (for real field).
\item \ffcode{P1c},  \ffcode{P1bc} and  \ffcode{P2c} are the analogous for complex fields.
\item \ffcode{P1surf} for real data defined along a frontier of the mesh (for free-surface cases),
\item \ffcode{P1surfc} same for complex data.
\end{itemize}

The reader is invited to study the examples in figures \ref{Lshape_Unsteady.edp}-\ref{Lshape_Stokes.edp} which will help to understand the link between the content of the \shell{.ff2m} files and the resulting matlab/octave  structures.

\end{enumerate}

\Remark{
The importation of data from \shell{.txt/.ff2m} files is actually done by the \stabfem driver \mlfile{SFcore\_ImportData.m}. This function is called internally within \mlfile{SF\_Launch}, but is  not expected to be directly called within a high-level \stabfem script.
}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Database management : \SF{Status} and \SF{Load} }
\label{sec:DB}

\begin{figure*}[t]
\setlistmatlab
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/SCRIPT_Lshape_DB.m}
\setlistmatlab
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Demo_SF_Status.log}
\caption{
\freefem program \mlfile{SCRIPT\_Lshape\_DB.m} demonstrating database management (up), and result of command \SF{Status} at line 16 of this program (down).}
\label{Lshape_DB}
\end{figure*}



StabFem offers a powerful way to store and reaccess data generated by FreeFem++ through the DataBase manager. Usage of database is demonstrated in numerous tutorial examples available online on the website of the project. Here we demonstrate it by a short example, again considering thermal problems in a L-shaped geometry.

The upper part of figure \ref{Lshape_DB} shows a short Matlab/Octave program, again extracted from the full example \mlfile{EXAMPLE\_Lshape.m} available on the website. Let us detail this example:
\begin{itemize}
\item On line 4 we launch the Data-Base manager by setting the global variable \mlcode{'ffdatadir'} to value \mlcode{'./WORK'}. This will create a folder \shell{./WORK} in the working directories, where all files generated by FreeFem will be placed.

\item On line 5 the command \mlcode{SF\_core\_arborescence('cleanall')} is used to clear all previous content from the arborescence (this is basically equivalent to the shell command \shell{rm -rf ./WORK/*/}).

\item  In line 11-13 we do a loop over the frequency to compute a series of unsteady heat problems. This is equivalent to what was done in "Physical problem 3" but the loop is done in the Octave/Matlab layer, instead of inside the FreeFem program. Note the usage of option \mlcode{'Store','Thermal'} which tells \SF{Launch} to put a backup copy of the FreeFem output files in folder \shell{./WORK/Thermal/'}.

\item At line 16 we invoke the data-base explorator \SF{Status}. The result of this command is displayed in the bottom part of figure \ref{Lshape_DB}. We can see that there are effectively 10 \shell{.txt} files in folder \shell{./WORK/Thermal/'}, and the important informations about these files, including the metadata \mlcode{omega} and \mlcode{HeatFlux}, as read from the \shell{.ff2m} files.

In addition we can see that all meshes generated in the course of the calculation (here a single one) have been stored in folder \shell{./WORK/MESHES/'}.

\item Line 17 demonstrates that the return value of \SF{Status} (here called \mlcode{sfs}) is a Matlab/Octave structure containing all important data from your previous computations. The fields of these object correspond to indexes of the existing folders in your database (here \shell{MESHES} and \shell{Thermal}).

See lines 43-58 from the bottom part of fig. \ref{Lshape_DB} to observe the content of these objects. In particular,
The object \mlcode{IndexThermal = sfs.Thermal} is an array 10 of structures with fields corresponding to informations on how to recover the data (file name and asssociated mesh file name) and to metadata.


\item Line 19 demonstrates how to reload a specific dataset (here the one corresponding to $ \omega
= 0.5$). 

\item Lines 24-25 demonstrate how to plot results extracted from the database in post-process mode. Here we plot impedance as function of frequency as in Problem 3, but using results extracted from the database. This way of generating plots can be very useful to post-process results and draw final figures at the outcome of a parametric study, for instance.

\end{itemize}




\Remarks{

\item The design of the database manager is fully customizable.
You can manage as many data folders in your database as you want, and design as many metadata as you want to perform your postprocessings according to your needs. 

\item For good operation all data files stored in a given folder should have same format and same set of metadata.

\item Folder names \shell{BASEFLOWS},  \shell{EIGENMODES}, \shell{THRESHOLDS} and \shell{STABSTATS} are reserved for stability analysis (see chapter 4) and managed by the devoted drivers \SF{BaseFlow} and \SF{Stability}. 

\item Similarly, folder names \shell{DNSFLOWS},  \shell{DNSSTATS} are reserved for Direct Numerical Simulation results managed by the driver \SF{DNS}.

\item Folder \shell{MISC} is reserved to store several files internally generated by the drivers at various stages  (dataset projected on a new mesh during mesh adaptation, sum of several datasets), etc... 

\item In some "old" codes present in the project, the folder \shell{MESHES} was used to store a copy of the first computed baseflow associated to each mesh in addition to the mesh itself. This method is not recommended any more.


}







\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Lshape_Unsteady.edp}
\setlistmatlab
\lstinputlisting[linerange={42-58}]{Lshape_ExecutionExample.txt}
\caption{
\freefem program \mlfile{Lshape\_Unsteady.edp} (up) and result of the execution of this program through the driver\SF{Launch} (bottom).}
\label{Lshape_Unsteady.edp}
\end{figure*}




\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Lshape_Impedance.edp}
\setlistmatlab
\lstinputlisting[linerange={62-75}]{Lshape_ExecutionExample.txt}
\caption{
\freefem program \mlfile{Lshape\_Impedance.edp} (up) and result of the execution of this program through the driver\SF{Launch} (bottom).}
\label{Lshape_Impedance.edp}
\end{figure*}

\begin{figure*}[t]
\setlistfreefem
\lstinputlisting{../../STABLE_CASES/EXAMPLE_Lshape/Lshape_Stokes.edp}
\setlistmatlab
\lstinputlisting[linerange={77-93}]{Lshape_ExecutionExample.txt}
\caption{
\freefem program \mlfile{Lshape\_Stokes.edp} (up) and result of the execution of this program through the driver\SF{Launch} (bottom).s}
\label{Lshape_Stokes.edp}
\end{figure*}


\clearpage



\iffalse
\subsection{Explanations about the mesh }

Examination of the simple example here shows that the mesh generation produces four files (here all generated by the macro SF\_writemesh defined
in file Macros\_StabFem.edp)

\begin{itemize}
\item file \mlfile{mesh.txt} contains the mesh in the native FreeFem format. Namely, information about the vertices, the boundaries and the triangles. This mesh is imported in stabfem using importFFmesm.m , and it is also needed for subsequent \freefem programs.


\item file \mlfile{mesh.ff2m} contains informations about the mesh structure, to be imported by StabFem (it is read by importFFmesm.m)

\item file  \mlfile{SF\_Init.ff2m} contains informations about the mesh geometry, to be imported by StabFem as well  (it is also read by importFFmesm.m)

\item file \mlfile{SF\_geom.edp} contains definitions of case-dependent geometrical parameters which may be used by the \freefem solvers.
This file is designed to be {\em included } in the header of the \freefem solvers.
\end{itemize}

Note that files \mlfile{mesh.ff2m} and  \mlfile{SF\_Init.ff2m} may seem redundant... the difference is that  \mlfile{SF\_Init.ff2m} is created only once 
while  \mlfile{mesh.ff2m} is recreated each time the mesh is modified (adapted, splitted, etc...) .
\fi










