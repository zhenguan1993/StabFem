> mkdir WORK
> echo "./WORK/'' > workdir.pref

> FreeFem++ -v 0 Mesh_Cylinder.edp
$$ Generation of an initial mesh for a 2D cylinder
$$ Enter the dimensions xmin, xmax, ymax >> -40 40 80
(...)

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Newton_2D.edp
$$ ENTERING Newton_2D.edp
$$ Entrer Reynolds Number : >> 10
(...)

> cp WORK/BaseFlow.txt WORK/BaseFlow_guess.txt

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Newton_2D.edp
$$ ENTERING Newton_2D.edp
$$ Entrer Reynolds Number >> 60
(...)

> cp WORK/BaseFlow.txt WORK/FlowFieldToAdapt1.txt

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/AdaptMesh.edp
$$ ENTERING ADAPTMESH.edp
$$ Enter nfields (number of fields to be used for adaptation) >> 1
$$ Enter storage mode of .txt file number 1 ? (string like ReP2P2P1, etc...) >> ReP2P2P1
$$ Enter number of additional real scalars associated to flowfield in file number 0 >> ?1
(...)

> cp WORK/FlowFieldToAdapt1.txt WORK/BaseFlow_guess.txt
> cp WORK/mesh_adapt.msh WORK/mesh.msh
> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Newton_2D.edp
$$ ENTERING Newton_2D.edp
$$ Entrer Reynolds Number >> 60
(...)

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Stab2D.edp
$$ ENTERING Stab2D.edp?
$$ Enter Reynolds               		>>  60
$$ Enter SHIFT (re,im)          		>> 0.04 0.74
$$ Symmetry properties ?? (A, S or N).  >> A
$$ Direct (D), Adjoint (A), D&A+sensitivity (S) or Endo. (E) ?>> S
$$ Enter nev ? (will use simple shift-invert if nev = 1) >> 1
(...)

> cp WORK/BaseFlow.txt WORK/FlowFieldToAdapt1.txt
> cp WORK/Sensitivity.txt WORK/FlowFieldToAdapt2.txt

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/AdaptMesh.edp
$$ Entering ADAPTMESH.edp 
$$ Enter nfields (number of fields to be used for adaptation) >> 2
$$ Enter storage mode of .txt file number 1 ? (string like ReP2P2P1, etc...) >> ReP2P2P1
$$ Enter number of additional real scalars associated to flowfield in file number 0 >> ?1
$$ Enter storage mode of .txt file number 2 ? (string like ReP2P2P1, etc...) >> ReP2
$$ Enter number of additional real scalars associated to flowfield in file number 1 >> ?0
(...)

> cp WORK/FlowFieldToAdapt1.txt WORK/BaseFlow_guess.txt
> cp WORK/mesh_adapt.msh WORK/mesh.msh

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Newton_2D.edp
$$ ENTERING Newton_2D.edp
$$ Entrer Reynolds Number ?>> 60
(...)

> FreeFem++ -v 0 ../../SOURCES_FREEFEM/Stab2D.edp
$$ ENTERING Stab2D.edp?
$$ Enter Reynolds               		>>  60
$$ Enter SHIFT (re,im)          		>> 0.04 0.74
$$ Symmetry properties ?? (A, S or N).  >> A
$$ Direct (D), Adjoint (A) ? >> D
$$ Enter nev ? (will use simple shift-invert if nev = 1) >> 1
(...)

