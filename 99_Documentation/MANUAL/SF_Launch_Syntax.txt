handle = SF_Launch( 'MySolver.edp', ...
		'Options',{'descriptor1',value1,'descriptor2',value2, (...) }, ...
		'Params', [ value1, value2, ...] , ...
		'Mesh', (themesh), ...
		'Initial',(theinitial), ['BaseFlow',(thebaseflow)], ['forcing',(theforcing), ], ...
		'DataFile','MyFileName.ff2m', ...
		'Store','MyFolderName' ...
		)