%% Acoustic field in a pipe with harmonic forcing at the bottom
%
%  Ce script montre comment utiliser StabFem pour calculer l'imp�dance et
%  les coefficients de r�flexion et transmission d'un pot d'�chappement.
%
% Le programme Mesh_3.edp g�n�re un maillage correspondant � un pot
% d'�chappement.
%
% Le rayon du tube principal est R1=1.
%
% Le volume de la cavit� est pi*(R3^2-R2^2)*Lcav avec Lcav=9,R2=1.5,R3=4
%
% la section du goulot est Sg = 2*pi*R1*h avec R1 = 1 et h=1 (l'ouverture est annulaire) 
% la longueur du goulot est l = (R2-R1) = 0.5 (a quoi il faut ajouter une correction de
% longueur a d�terminer)


%% Chapter 0 : initialisation
addpath([fileparts(fileparts(pwd)) '/SOURCES_MATLAB/']);
SF_Start('verbosity',4);


%% Chapter 1 : building an adapted mesh
ffmeshInit = SF_Mesh('Mesh_3.edp','Params',10,'problemtype','acousticaxi');
Forced = SF_LinearForced(ffmeshInit,'omega',2);
ffmesh = SF_Adapt(ffmeshInit,Forced,'Hmax',2); % Adaptation du maillage


%% 
%plot the mesh :

 figure;  SF_Plot(ffmeshInit,'mesh','symmetry','ym','boundary','on');
 hold on; SF_Plot(ffmesh,'mesh','title','Mesh : Initial (left) and Adapted (right)','boundary','on');
 


%% Chapter 2 : Compute and plot the pressure fied with harmonic forcing at the bottom of the tube
% 
omega = 1;
Forced = SF_LinearForced(ffmesh,'omega',omega)


figure();
SF_Plot(Forced,'p.im','boundary','on','colormap','redblue','cbtitle','Re(p'')');
hold on;
SF_Plot(Forced,'p','boundary','on','colormap','redblue','symmetry','YM','cbtitle','Im(p'')/Re(p'')');


%%
% Create a movie (animated gif) from this field

h = figure;
filename = 'AcousticTube.gif';
SF_Plot(Forced,'p','boundary','on','colormap','redblue','colorrange',[-1 1],...
        'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k','Amp',1);
set(gca,'nextplot','replacechildren');
    for k = 1:20
       Amp = exp(-2*pi*1i*k/20);
       SF_Plot(Forced,'p','boundary','on','contour','on','clevels',[-2 :.5 :2],...
           'colormap','redblue','colorrange',[-1 1],...
           'symmetry','YS','cbtitle','p''','colorbar','eastoutside','bdlabels',[1 2 ],'bdcolors','k','Amp',Amp); 
      frame = getframe(h); 
      im = frame2im(frame); 
      [imind,cm] = rgb2ind(im,256); 
      if k == 1 
          imwrite(imind,cm,filename,'gif', 'Loopcount',inf); 
      else 
          imwrite(imind,cm,filename,'gif','WriteMode','append'); 
      end 
    end
 

%%
% Here is the movie
%
% <<AcousticTube.gif>>
%



%%
% Extract p and |u| along the symmetry axis
%            
Xaxis = [-21 :.1 :0];
Uyaxis = SF_ExtractData(Forced,'uz',0,Xaxis);
Paxis = SF_ExtractData(Forced,'p',0,Xaxis);

%%
% Plot  p and |u| along the symmetry axis
figure();
plot(Xaxis,real(Uyaxis),Xaxis,imag(Uyaxis)); hold on;plot(Xaxis,real(Paxis),Xaxis,imag(Paxis));
xlabel('x');
legend('Re(u''_z)','Im(u''_z)','Re(p'')','Im(p'')');
pause(0.1);

%% Chapter 3 : loop over k to compute the impedance $Z(k)$ (comparing SOMMERFELD, PML and CM)

omegarange = [0.01:.01:1];

IMP = SF_LinearForced(ffmesh,omegarange)


%% 
% Plot $Z(k)$ 
figure;
plot(IMP.omega,real(IMP.Z),'r',IMP.omega,imag(IMP.Z),'r--');
title(['Impedance $Z_r$ and $Z_i$'],'Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);

%% 
% Plot $Z(k)$ 
figure;
plot(IMP.omega,real(IMP.Z),'r',IMP.omega,imag(IMP.Z),'r--');
title(['Impedance $Z_r$ and $Z_i$'],'Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$Z_r,Z_i$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);

figure;
semilogy(IMP.omega,abs(IMP.Z),'r');
title(['Impedance $|Z|$ '],'Interpreter','latex','FontSize', 30)
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$|Z|$','Interpreter','latex','FontSize', 30);
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
pause(0.1);


%%
% plot Reflection coefficient 


figure;
plot(IMP.omega,IMP.R,'b--',IMP.omega,1-IMP.R,'r--');
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$R,T$','Interpreter','latex','FontSize', 30);
title(['R (blue) and T (red) coefficients'],'Interpreter','latex','FontSize', 30)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);

figure;
semilogy(IMP.omega,IMP.R,'b--',IMP.omega,1-IMP.R,'r--');
xlabel('$\omega R/c$','Interpreter','latex','FontSize', 30);
ylabel('$R,T$','Interpreter','latex','FontSize', 30);
title(['R (blue) and T (red) coefficients'],'Interpreter','latex','FontSize', 30)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);

