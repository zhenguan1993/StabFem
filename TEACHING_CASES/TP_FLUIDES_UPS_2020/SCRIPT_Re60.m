%%  DNS FLOW - TUTORIAL 3 FOR MSc 1st year in Paul Sabatier University
%
% In this tutorial script we show how to postprocess DNS results for the wake of a
% cylinder for Re = 60, allowing to observe the development and saturation
% of the well-known Von Karman vortex street.
% 
% The data is in the directory ./WORK/ and can be re-created by running the
% script SF_GENERATE_DATA.m in the same directory

%%
%
% First : initialization of StabFem tools
%

close all;
addpath('../../SOURCES_MATLAB');
if ~isempty(getenv('CI'))
% this line is only for good operation of the server when generating the website of the project... don't pay attention !
    system('tar -xzvf /work/SF_works/TP_FLUIDES_UPS_2020/$(ls -1 /work/SF_works/TP_FLUIDES_UPS_2020 | tail -n 1)') ;
else
    system('tar -xvfz WORK_CYLINDER.tgz');
end
SF_Start('verbosity', 3,'ffdatadir', './WORK_CYLINDER/');

%% Chapter 1 : check what is available in the database
sfs = SF_Status

% We can see that there are 11 data files in the "DNSFIELDS" directory
% corresponding to 11 instants in an oscillation cycle.


%% Chapter 2 : import one snapshot and plot a few things.


field = SF_Load('DNSFIELDS','last');

%%

SF_Plot(field,'ux','xlim',[-2 10],'ylim',[-3 3 ],'colorrange','cropminmax',...
        'title',['t = ',num2str(field.t)],'boundary','on','bdlabels',2);
hold on; SF_Plot(field,'psi','contour','only','clevels',[-2 -1.5 -1 -.5  -.2 -.1 -0.05 -.02 0 0.02 0.05 0.1 .2 .5 1 1.5 2],'xlim',[-2 10],'ylim',[-3 3]);


%% Chapter 3 : generating a movie
% 





% Here is how to generate a movie
h = figure;
filename = 'html/DNS_Cylinder_Re60.gif';
for i=1:length(sfs.DNSFIELDS)
    DNSfields(i) = SF_Load('DNSFIELDS',i);
    SF_Plot(DNSfields(i),'ux','xlim',[-2 10],'ylim',[-3 3 ],'colorrange','cropmaxmin',...
        'title',['t = ',num2str(DNSfields(i).t)],'boundary','on','bdlabels',2);
    hold on; SF_Plot(DNSfields(i),'psi','contour','only','clevels',[-2 -1.5 -1 -.5  -.2 -.1 -0.05 -.02 0 0.02 0.05 0.1 .2 .5 1 1.5 2],'xlim',[-2 10],'ylim',[-3 3]);
    hold off;
    pause(0.1);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if (i==1) 
       imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
       set(gca,'nextplot','replacechildren');
    else 
       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
    end 
end

%%
% Here is the movie
%
% <<DNS_Cylinder_Re60.gif>>
%

%% Chapter 4
% We now plot the lift force as function of time

DNSstats = sfs.DNSSTATS

figure(15);
subplot(2,1,1);
plot(DNSstats.t,DNSstats.Fy);title('Lift force as function of time');
xlabel('t');ylabel('Fy');
subplot(2,1,2);
plot(DNSstats.t,DNSstats.Fx);title('Drag force as function of time');
xlabel('t');ylabel('Fx');


%%
% Now we plot the pressure and vorticity along the surface
alpha = linspace(-pi,pi,201);
Xsurf = .501*cos(alpha);
Ysurf = .501*sin(alpha);
Psurf = SF_ExtractData(DNSfields(end),'p',Xsurf,Ysurf);
Omegasurf = SF_ExtractData(DNSfields(end),'vort',Xsurf,Ysurf);
figure(16);
subplot(2,1,1);title('Pressure along the surface P(r=a,\theta) at final time step')
plot(alpha,Psurf);
xlabel('\theta');ylabel('P(r=a,\theta)');
subplot(2,1,2);title('Vorticity along the surface \omega(r=a,\theta) at final time step')
plot(alpha,Omegasurf);
xlabel('\theta');ylabel('\omega_z(r=a,\theta)');


%% [[PUBLISH]]
