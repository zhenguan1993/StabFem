%%  DNS FLOW - TUTORIAL 3 FOR MSc 1st year in Paul Sabatier University
%
% In this tutorial script we show how to perform a DNS of the wake of a
% cylinder for Re = 60, allowing to observe the development and saturation
% of the well-known Von Karman vortex street.
% The DNS solver is based on Uzawa method using Cahouet preconditionner.
% The FreeFem++ code <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/TimeStepper_2D.edp TimeStepper_2D.edp> 
% is adapted from the one used in Jallas, Marquet & Fabre (PRE, 2017).

%% Chapter 0 : initialization
addpath('../../SOURCES_MATLAB');
SF_core_start();
SF_core_setopt('verbosity', 4);
SF_core_setopt('ffdatadir', './WORK/');
% NB change verbosity to 4 to follow the simulation while it's running 
%  or 2 to supress output from freefem++



%% Chapter 3 : Launch a DNS
%
% We do 400 time steps and produce snapshots each 10 time steps.
% We use the driver <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_MATLAB/SF_DNS.m SF_DNS.m> 
% which launches the Freefem++ solver 
% <https://gitlab.com/stabfem/StabFem/blob/master/SOURCES_FREEFEM/TimeStepper_2D.edp TimeStepper_2D.edp>

load('DNSAll.mat'); % Load previously computed transient and converged simulation

%% Chapter 4 : plotting the results and generating a movie
% 
 
% Here is how to generate a movie
h = figure;
filename = 'html/DNS_Cylinder_Re60.gif';
for i=1:length(DNSfields)
    SF_Plot(DNSfields(i),'ux','xlim',[-2 10],'ylim',[-3 3 ],'colorrange','cropmaxmin',...
        'title',['t = ',num2str((i-1)*dt)],'boundary','on','bdlabels',2);
    hold on; SF_Plot(DNSfields(i),'psi','contour','only','clevels',[-2 -1.5 -1 -.5  -.2 -.1 -0.05 -.02 0 0.02 0.05 0.1 .2 .5 1 1.5 2],'xlim',[-2 10],'ylim',[-3 3]);
    hold off;
    pause(0.1);
    frame = getframe(h); 
    im = frame2im(frame); 
    [imind,cm] = rgb2ind(im,256); 
    if (i==1) 
       imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.1); 
       set(gca,'nextplot','replacechildren');
    else 
       imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.1); 
    end 
end

%%
% Here is the movie
%
% <<DNS_Cylinder_Re60.gif>>
%

%% 
% We now plot the lift force as function of time

figure(15);
subplot(2,1,1);
plot(DNSstats.t,DNSstats.Fy);title('Lift force as function of time');
xlabel('t');ylabel('Fy');
subplot(2,1,2);
plot(DNSstats.t,DNSstats.Fx);title('Drag force as function of time');
xlabel('t');ylabel('Fx');


%%
% Now we plot the pressure and vorticity along the surface
alpha = linspace(-pi,pi,201);
Xsurf = .501*cos(alpha);
Ysurf = .501*sin(alpha);
Psurf = SF_ExtractData(DNSfields(end),'p',Xsurf,Ysurf);
Omegasurf = SF_ExtractData(DNSfields(end),'vort',Xsurf,Ysurf);
figure(16);
subplot(2,1,1);title('Pressure along the surface P(r=a,\theta) at final time step')
plot(alpha,Psurf);
xlabel('\theta');ylabel('P(r=a,\theta)');
subplot(2,1,2);title('Vorticity along the surface \omega(r=a,\theta) at final time step')
plot(alpha,Omegasurf);
xlabel('\theta');ylabel('\omega_z(r=a,\theta)');



