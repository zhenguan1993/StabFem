/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////
///// 
/////                              MESH GENERATOR for a cylinder
/////
/////             This file creates a mesh and an initial base flow for the "StabFem" program.
/////
/////             input parameters : Xmin Xmax Ymax
/////  			  
/////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
include "Macros_StabFem.idp";

real RADIUS=0.5;         /////// Cylinder Radius
real n=0.5;                 
real ns=100; 
real nb=20;
real xinlet, xoutlet, yside ;
real boxW, boxH;
boxW = 5; boxH = 5;
cout << " Generation of an initial mesh for a 2D cylinder" << endl;
cout << " Enter the dimensions xmin, xmax, ymax ? " << endl; 

cin >> xinlet >> xoutlet >> yside ;
cout << "Xmin = " << xinlet << " ; Xmax = " << xoutlet << " ; ymax = " << yside << endl;  

/////////////////////////////////////////////////////////////////////////////////
//int bclat=4,bcinlet=1,bcoutflow=3,bcwall=2,bcaxis=6;
                         /////// label for boundary conditions  
/////////////////////////////////////////////////////////////////////////////////
// cylinder center is in (0,0)

border cylinder(t=0,2*pi){ x=RADIUS*cos(t);y=RADIUS*sin(t);label=2;}
//border cylref(t=0,2*pi){ x=(RADIUS+RR)*cos(t);y=(RADIUS+RR)*sin(t);label=999;}
border inlet(t=1,-1){ x=xinlet;y=yside*t;label=1;}
//border axis1(t=0,1){ x=xinlet+(-RADIUS-xinlet)*t;y=0;label=6;}
//border axis2(t=0,1){ x=RADIUS+(xoutlet-RADIUS)*t;y=0;label=6;}
border outlet(t=-1,1){ x=xoutlet;y=yside*t;label=3;}
border latsup(t=0,1){ x=xoutlet-(xoutlet-xinlet)*t;y=yside;label=3;} // label 3 : same conditions as at outlet
border latbot(t=0,1){ x=xinlet+(xoutlet-xinlet)*t;y=-yside;label=3;} // label 3 : same conditions as at outlet
border boxRefR(t=-1,1){x=boxW;y=boxH*t;label=999;}
border boxRefU(t=1,-1){x=boxW*t;y=boxH;label=999;}
border boxRefL(t=1,-1){x=-boxW;y=boxH*t;label=999;}
border boxRefB(t=-1,1){x=boxW*t;y=-boxH;label=999;}

// Build a mesh 
//plot(inlet(yside*n)+axis1((-RADIUS-xinlet)*n)+axis2((xoutlet-RADIUS)*n)+outlet(2*yside*n)+latsup((xoutlet-xinlet)*n)+cylinder(-ns));

mesh th=buildmesh(inlet(yside*2*n) + latbot((xoutlet-xinlet)*n)
+outlet(2*yside*n) + latsup((xoutlet-xinlet)*n) + cylinder(-ns)
+ boxRefR(boxH*nb) + boxRefU(boxW*nb) + boxRefL(boxH*nb) + boxRefB(boxW*nb)
);


// SAVE THE MESH in mesh.msh file 
savemesh(th,ffdatadir+"mesh.msh");


// SECOND AUXILIARY FILE  for Stabfem : mesh.ff2m
IFMACRO(SFWriteMesh)
	SFWriteMesh(ffdatadir+"mesh.ff2m",th,"initial")
ENDIFMACRO

// AUXILIARY FILE NEEDED TO PLOT P2 DATA
IFMACRO(SFWriteConnectivity)
	SFWriteConnectivity(ffdatadir+"mesh_connectivity.ff2m",th);
ENDIFMACRO

// THIRD AUXILIARY FILE for Stabfem : SF_Geom.edp
{
			ofstream file2("SF_Geom.edp"); 
			file2 << "// Description geometry (file automatically created ; to be included in the FreeFem programs)" << endl;
            file2 << " real SFGeomLengthscale = 1 ; // length scale for nondimensionalization" << endl ;
            file2 << " real SFGeomVelocityscale = 1 ; // velocity scale for nondimensionalization " << endl ;
}
