%% CHAPTER 0 : set the global variables needed by the drivers
disp(' ');
disp('######     ENTERING LINEAR PART       ####### ');
disp(' ');
%clear all;
close all;
addpath('../../SOURCES_MATLAB/');
SF_core_start('verbosity',4,'workdir','./WORK/');
figureformat='tif'; AspectRatio = 0.56; % for figures
system('mkdir FIGURES');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
tic;
Ma = 0.2;
Re = 50;
HMax = 1;

%% CHAPTER 2 : Create a mesh

ffmesh = SF_Mesh('Mesh_Cylinder.edp','problemtype','2dcomp',...
                 'Params',[-40,80,40]);
bf=SF_BaseFlow(ffmesh,'Re',50,'Mach',0.2,'type','NEW');
ffmesh = SF_SetMapping(ffmesh, 'MappingType', 'box',...
                       'MappingParams', [-10,10.0,-10,10,-0.3,5,-0.3,5]); 
[evD,emD] = SF_Stability(bf,'shift',0.0098 + 0.7176i,'nev',2,'type','D');
[evA,emA] = SF_Stability(bf,'shift',0.0098 + 0.7176i,'nev',2,'type','A');
sen = SF_Sensitivity(bf, emD(1), emA(1), 'Type', 'SMa');

bf2=SF_BaseFlow(ffmesh,'Re',50,'Mach',0.21,'type','NEW');
[evD2,emD2] = SF_Stability(bf2,'shift',0.0108 + 0.7270i,'nev',2,'type','D');
slopeFD = (evD(1)-evD2(1))./0.01;

%% Sensitivity to rotation
bfR=SF_BaseFlow(ffmesh,'Re',50,'Mach',0.2,'Omegax',0.5,'type','NEW');
[evDR,emDR] = SF_Stability(bfR,'shift',0.0098 + 0.7176i,'nev',2,'type','D');
[evAR,emAR] = SF_Stability(bfR,'shift',0.0098 + 0.7176i,'nev',2,'type','A');
senForc = SF_Sensitivity(bfR, emDR(1), emAR(1), 'Type', 'SForc');


%%
% Post-processing along the wall
theta = linspace(0,2*pi,1000);
x = 0.5*cos(theta);
y = 0.5*sin(theta);
lamDUwall = SF_ExtractData(senForc,'lambdaUwall',x,y);
lamDVwall = SF_ExtractData(senForc,'lambdaVwall',x,y);
lamDTwall = SF_ExtractData(senForc,'lambdaTwall',x,y);
% Plotting
figure(1);
subplot(3,1,1);
plot(theta,lamDUwall);
xlabel('$\theta$','Interpreter','latex');
ylabel('$\nabla \lambda_{u_w}$','Interpreter','latex');
subplot(3,1,2);
plot(theta,lamDVwall);
xlabel('$\theta$','Interpreter','latex');
ylabel('$\nabla \lambda_{v_w}$','Interpreter','latex');
subplot(3,1,3);
plot(theta,lamDTwall);
xlabel('$\theta$','Interpreter','latex');

ylabel('$\nabla \lambda_{T_w}$','Interpreter','latex');
%% Reference values 
% Re = 50 (Canuto et. al.)
% \lambda = \sigma + i \omega
% MaRef = [0.0:0.1:0.5];
% sigma = [0.01505, 0.01428, 0.01184, 0.008232, 0.008974, 0.003894];
% omega = 2*pi.*[0.1194,0.1190,0.1180,0.1166,0.1145,0.1127];
% slopeOmega = (omega(2:end) - omega(1:end-1))./(MaRef(2) - MaRef(1));
% slopeSigma = (sigma(2:end) - sigma(1:end-1))./(MaRef(2) - MaRef(1));
