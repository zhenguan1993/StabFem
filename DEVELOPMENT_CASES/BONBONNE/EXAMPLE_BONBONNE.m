%% THIS ONE IS OBSOLETE... TAKE L2 OR L6 WHICH USE SmartMesh
addpath('../../SOURCES_MATLAB');
SF_Start;
close all;
SF_core_setopt('verbosity',4);
SF_core_arborescence('cleanall');

%Rtank = 10/.3 ; Ltank = 80/.3; Rpipe = 1; Lpipe = 10/.3;LpipeIn=Lpipe; % for generic  
Rtank = 5; Ltank = 20; Rpipe = 1; Lpipe = 5;LpipeIn=5;
ffmesh = SF_Mesh('mesh_bonbonne.edp','params',[Rtank Ltank Rpipe Lpipe LpipeIn]);;

figure; SF_Plot(ffmesh);
figure; SF_Plot(ffmesh,'xlim',[Lpipe-3*Rpipe,Lpipe+3*Rpipe],'ylim',[0,3*Rpipe]);

 bf=SF_BaseFlow(ffmesh,'Re',10,'solver','Newton_Axi.edp'); 
 figure ; SF_Plot(bf,'ux');
hold on; SF_Plot(bf,{'ux','ur'})
pause(0.1);

%%
    disp('computing base flow and adapting mesh');
    Re_start = [10 , 30, 60, 100 200 300]; % values of Re for progressive increasing up to end
    for Rei = Re_start
        bf=SF_BaseFlow(bf,'Re',Rei,'solver','Newton_Axi.edp'); 
        bf=SF_Adapt(bf,'Hmax',0.5);
    end
 
 figure ; SF_Plot(bf,'ux');
hold on; SF_Plot(bf,{'ux','ur'})
pause(0.1);


figure; SF_Plot(bf);
figure; SF_Plot(bf,'xlim',[Lpipe-3*Rpipe,Lpipe+3*Rpipe],'ylim',[0,3*Rpipe]);



%% Look at eigenmodes
[ev1,em1] = SF_Stability(bf,'m',1,'nev',10,'shift',.1+0.1i,'plotspectrum','yes','solver','StabAxi.edp')
%[ev2,em2] = SF_Stability(bf,'m',1,'nev',10,'shift',.1+0.3i,'plotspectrum','yes','solver','StabAxi.edp')
%[ev3,em3] = SF_Stability(bf,'m',1,'nev',10,'shift',.1+1i,'plotspectrum','yes','solver','StabAxi.edp')
figure; SF_Plot(em1(1),'ux')
saveas(1,'eigenmode','png')

% Adjoint
[ev,em] = SF_Stability(bf,'m',1,'nev',10,'shift',.1+0.1i,'plotspectrum','yes','solver','StabAxi.edp','type','A')
figure; SF_Plot(em(1),'ux')
%%[[PUBLISH]]