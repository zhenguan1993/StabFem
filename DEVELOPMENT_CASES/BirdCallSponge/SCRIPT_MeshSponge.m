%%  Instability and impedance of a jet a hole tone configuration
%
% 
% THIS SCRIPT GENERATES PLOTS FOR THE FORCED STRUCTURES AND THE
% EIGENMODES FOR THE FLOW THROUGH A TWO HOLE IN A THICK PLATE W
% 
% REFEFENCE : Sierra, Fabre, Citro & Giannetti , ICTAM, 2020
%
%

%% Chapter 0 : Initialization 
close all;close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%%
nu0 = 1.466e-5; D0 = 2*1.24e-3; T0=288.5; gamma=1.4; R=288;
Re0=500; U0=Re0*nu0/(D0); c0 = sqrt(gamma*R*T0); 
Ma = U0/c0;
Ma=0.02;
ReList = [10,20,30,50,75,100,150,200,250,300,350,400];
% Create initial mesh
ffmesh = SF_Mesh('Mesh_BirdCall_Sponge.edp','problemtype','axicompsponge');
bf=SF_BaseFlow(ffmesh,'Re',1,'Mach',Ma,'type','NEW');
bf=SF_Adapt(bf,'Hmax',10);
for Re = ReList
    bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW');
    bf=SF_Adapt(bf,'Hmax',6.5);
end
%% Mesh adaptation to sensitivity
ffmesh = SF_SetMapping(ffmesh, 'MappingType', 'box', 'MappingParams', [-5,10,0,5,-0.0,2.5,-0.0,2.5]) 
[ev,em] = SF_Stability(bf,'shift',0.2+3.56i,'nev',5,'type','D','m',0);
[evA,emA] = SF_Stability(bf,'shift',ev(1),'nev',5,'type','A','m',0);
sensitivity = SF_Sensitivity(bf, em(1), emA(1), 'Type','S');
MaskCav = SF_Mask(bf.mesh,[-1 3.0 0 4 .1]);
MaskWake = SF_Mask(bf.mesh,[0.0 20.0 -1 1 .05]);
bf=SF_Adapt(bf,sensitivity,MaskCav,MaskWake,'Hmax',8);
[ev,em] = SF_Stability(bf,'shift',ev(1),'nev',5,'type','D','m',0);
%% WNL
Rec=337.05; Ma=0.02;
bf=SF_BaseFlow(bf,'Re',Rec,'Mach',Ma,'type','NEW');
[ev,em] = SF_Stability(bf,'shift',0.2+3.56i,'nev',5,'type','D','m',0);
[evA,emA] = SF_Stability(bf,'shift',conj(ev(1)),'nev',5,'type','A','m',0);
ReT=Rec+0.25;
[wnl,meanflow,mode,mode2] = SF_WNL(bf,em(1),'Retest',ReT,'Adjoint',emA(1),'Normalization','none');
sensitivity = SF_Sensitivity(bf, em(1), emA(1), 'Type','S');
MaskCav = SF_Mask(bf.mesh,[-1 3.0 0 4 .1]);
MaskWake = SF_Mask(bf.mesh,[0.0 20.0 -1 1 .1]);
bf=SF_Adapt(bf,sensitivity,MaskCav,MaskWake,'Hmax',6);
[ev,em] = SF_Stability(bf,'shift',0.2+3.56i,'nev',5,'type','D','m',0);
[evA,emA] = SF_Stability(bf,'shift',conj(ev(1)),'nev',5,'type','A','m',0);
ReT=Rec+0.05;
[wnl,meanflow,mode,mode2] = SF_WNL(bf,em(1),'Retest',ReT,'Adjoint',emA(1),'Normalization','none');
% modeHBN = [mode,mode2];
% % [meanflow,modeHBN] = SF_HB1(bf,mode,'Re',ReT,'Ma',Ma,'symmetry','N','symmetryBF','N','Aguess',0.5); % mode at Re=Rec+0.01 or Rec+0.25 and bf
modeHB2=[mode,mode2];
modeHB2(1).omega=imag(mode.lambda);
[meanflow2,modeHBN2] = SF_HBN(meanflow,modeHB2,'Re',ReT,'Ma',Ma,'symmetry','N','symmetryBF','N','NModes',2,'PCtype','bjacobi','ncores',6);
% [meanflow2S,modeHBN2S] = SF_HBN(meanflow2S,modeHBN2S,'Re',Rec+0.03,'Ma',Ma,'symmetry','N','symmetryBF','N','NModes',2,'PCtype','bjacobi');
% 
% [meanflow3,modeHBN3] = SF_HBN(meanflow2,modeHBN2,'Re',ReT,'Ma',Ma,'symmetry','N','symmetryBF','N','NModes',3,'PCtype','bjacobi');
% [meanflow4,modeHBN4] = SF_HBN(meanflow3,modeHBN3,'Re',ReT,'Ma',Ma,'symmetry','N','symmetryBF','N','NModes',4,'PCtype','bjacobi');
% mflow=meanflow4; mHBN=modeHBN4;
% %% Loop in 
% mflow=meanflow2S; mHBN=modeHBN2S;
% mflowList = [mflow]; modeList = [mHBN];
% %%
% ReList = [350:5:400];
% omegaNList = [imag(modeList(1).lambda),imag(modeList(3).lambda),imag(modeList(5).lambda),imag(modeList(7).lambda)];
% AEnergy1 = [0.0589862,0.0625044,0.0659934,0.0692001];
% AEnergy2 = [0.0132572,0.0147569,0.0163071,0.0177859];
% ReInt=[337:1:340];
% %%
% for Re=ReList
%     omegaNL=interp1(ReInt,omegaNList,Re,'makima','extrap');
%     A1=interp1(ReInt,AEnergy1,Re,'makima','extrap');
%     A2=interp1(ReInt,AEnergy2,Re,'makima','extrap');
%     modeHBN = modeList(end-1:end); modeHBN(1).lambda=omegaNL*1i;
%     meanflow=mflowList(end); 
% 
% 
%     [mflow,mHBN] = SF_HBN(meanflow,modeHBN,'Re',Re,'Ma',Ma,...
%                             'symmetry','N','symmetryBF','N',...
%                             'NModes',2,'PCtype','bjacobi',...
%                              'Aguess',[A1,A2],'ncores',6);
%     mflowList = [mflowList,mflow];
%     modeList = [modeList,mHBN];
%     ReInt = [ReInt,Re];
%     omegaNList = [omegaNList,imag(mHBN(1).lambda)]
%     AEnergy1 = [AEnergy1,mHBN(1).AEnergy];
%     AEnergy2 = [AEnergy2,mHBN(2).AEnergy];
%    
% end
% %% Test Re=400
% omega400=interp1(ReInt,omegaNL,345,'linear','extrap');
% A1=interp1(ReInt,AEnergy1,345,'linear','extrap');
% A2=interp1(ReInt,AEnergy2,345,'linear','extrap');
% modeHBN = modeList(end-1:end); modeHBN(1).lambda=omega400*1i;
% meanflow=mflowList(end); 
% 
% [meanflow2S,modeHBN2S] = SF_HBN(meanflow,modeHBN,'Re',345,'Ma',Ma,...
%     'symmetry','N','symmetryBF','N','NModes',2,'PCtype','bjacobi',...
%     'Aguess',[A1,A2]);
% 
% % IMPORT 
% % newname = ['./WORK/MEANFLOWS/MeanFlow_Re340_Ma0.02.txt'];
% %         meanflow = SFcore_ImportData(meanflow.mesh,newname);
% % mode= [];
% %         for i=[1:2]
% %             newname =  [ffdatadir, 'MEANFLOWS/Harmonic',...
% %                        num2str(i),'_Re',num2str(Re),...
% %                        '_Omegax',num2str(Omegax),'.txt']
% %             modeImport = SFcore_ImportData(meanflow.mesh,newname);
% %             mode = [mode, modeImport];
% %         end
