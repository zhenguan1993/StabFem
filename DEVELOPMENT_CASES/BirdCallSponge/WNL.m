%%  Instability and impedance of a jet a hole tone configuration
%
% 
% THIS SCRIPT GENERATES PLOTS FOR THE FORCED STRUCTURES AND THE
% EIGENMODES FOR THE FLOW THROUGH A TWO HOLE IN A THICK PLATE W
% 
% REFEFENCE : Sierra, Fabre, Citro & Giannetti , ICTAM, 2020
%
%

%% Chapter 0 : Initialization 
close all;close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK/');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%%
Ma=0.02;
ReList = [10,20,30,50,75,100,150,200,250,300,350,400];
% Create initial mesh
ffmesh = SF_Mesh('Mesh_BirdCall_Sponge.edp','problemtype','axicompsponge');
bf=SF_BaseFlow(ffmesh,'Re',1,'Mach',Ma,'type','NEW');
bf=SF_Adapt(bf,'Hmax',10);
for Re = ReList
    bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'type','NEW');
    bf=SF_Adapt(bf,'Hmax',5);
end
%% Mesh adaptation to sensitivity
ffmesh = SF_SetMapping(ffmesh, 'MappingType', 'box', 'MappingParams', [-5,10,0,5,-0.0,2.5,-0.0,2.5]) 
[ev,em] = SF_Stability(bf,'shift',0.2+3.56i,'nev',5,'type','D','m',0);
[evA,emA] = SF_Stability(bf,'shift',ev(1),'nev',5,'type','A','m',0);
sensitivity = SF_Sensitivity(bf, em(1), emA(1), 'Type','S');
bf=SF_Adapt(bf,sensitivity,MaskCav,MaskWake,'Hmax',8);
%% WNL
Rec=332.5; Ma=0.02;
bf=SF_BaseFlow(bf,'Re',Rec,'Mach',Ma,'type','NEW');
[ev,em] = SF_Stability(bf,'shift',0.01+3.268i,'nev',5,'type','D','m',0);
[evA,emA] = SF_Stability(bf,'shift',conj(ev(1)),'nev',5,'type','A','m',0);
ReT=Rec+0.05;
[wnl,meanflow,mode,mode2] = SF_WNL(bf,em(1),'Retest',ReT,'Adjoint',emA(1),'Normalization','E');
modeHB2=[mode,mode2];
modeHB2(1).omega=imag(mode.lambda);
[meanflow2,modeHBN2] = SF_HBN(meanflow,modeHB2,'Re',ReT,'Ma',Ma,'symmetry','N','symmetryBF','N','NModes',2,'PCtype','bjacobi','ncores',6);

%% Refine 
% MaskCav = SF_Mask(bf.mesh,[-1 3.0 0 4 .1]);
% MaskWake = SF_Mask(bf.mesh,[0.0 20.0 -1 1 .05]);
% bf=SF_Adapt(bf,meanflow,mode,mode2,MaskCav,MaskWake,'Hmax',5);
% 
% ffmesh = SF_SetMapping(ffmesh, 'MappingType', 'box', 'MappingParams', [-5,10,0,5,-0.0,2.5,-0.0,2.5]) 
% [ev,em] = SF_Stability(bf,'shift',0.2+3.56i,'nev',5,'type','D','m',0);
% [evA,emA] = SF_Stability(bf,'shift',ev(1),'nev',5,'type','A','m',0);
% sensitivity = SF_Sensitivity(bf, em(1), emA(1), 'Type','S');
% MaskCav = SF_Mask(bf.mesh,[-1 3.0 0 4 .1]);
% MaskWake = SF_Mask(bf.mesh,[0.0 20.0 -1 1 .05]);
