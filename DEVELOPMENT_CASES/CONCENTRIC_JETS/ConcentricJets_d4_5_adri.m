%%  StabFem Demo script for concentric jets WITH TANH VELOCITY PROFILE
%
% This case corresponds to the configuration of Canton et al.
% This scripts aims to reproduce figure 5 of the reference article : 
% base flow for Re = 1000 and several values of U2/U1
clc
clear all
close all

format shorte

%% Chapter 0 : Initialization 
close all;clear all; clc
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',2,'ffdatadir','./WORK_d4_5_adri/'); % NB use verbosity=2 for autopublish mode ; 4 during work
SF_core_setopt('ErrorIfDiverge',false); % this is to correctly report problems in cases of divergence
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');


%% First generate a mesh

% Parameters
U1Max=1.5;
U2Max=1.5; % first value
R1=0.5; distance=4.5; D2=1; Lpipe = 5.2;
Xmax = 100*(R1+distance+D2)     % 100*D
YMax = 20 *(R1+distance+D2)           % 20*D
bctype = 1; % select 0 for constant and 1 for Poiseuille and 2 for Tanh
% Mesh generation



paramMESH = [R1 distance D2 Lpipe Xmax YMax];
ffmesh = SF_Mesh('Mesh_ConcentricJets_Pipes.edp','params',paramMESH,'problemtype','axixr','cleanworkdir','no');

% Generation of an initial solution
bf = SF_BaseFlow(ffmesh,'Re',1,'bctype',bctype ,'U2',U2Max,'U1',U1Max);
bf = SF_Adapt(bf,'anisomax',4,'nbjacoby',3);

%% Raise Re

ReTab = [5 10:10:50 53:0.05:53.2]

en = [];

for Re = ReTab
    bf = SF_BaseFlow(bf,'Re',Re); %NB it is not needed to specify other parameters U1,U2,bctype : values of previous bf are used as default
    if bf.iter==-1
        disp(['Divergence in Newton for Re =',num2str(Re),' ; U2/U1 = ',num2str(U2Max)]);
        break;
    end
    % Generation of a mask to refine a given region
    MaskReg = [-8.0 20.0 0.0 1.5*(R1+D2+distance) 0.1]; % [Xmin Xmax Ymin Ymax delta]
    Mask = SF_Mask(bf.mesh,MaskReg);
    % Adaptation with a Mask
    [bf] = SF_Adapt(bf,Mask,'Hmax',2,'anisomax',4,'nbjacoby',3);
    en = [en bf.Energy];
    
 
end
 

%% Contour Plots
figure;
SF_Plot(bf,'ux','title','Re = 53.2',...
                'xlim',[-0.1 10],'ylim',[0 8],'ColorMap','parula');
hold on;
SF_Plot(bf,'psi','contour','only','xlim',[-0.1 10],...
                'ylim',[0 8],'clevels',[-1.5:0.05:1.5],'CColor','white' );
        
%% Energy Plot

figure();
subplot(1,2,1); plot(ReTab(1:length(en)),en,'r','LineWidth',2)
 xlabel('Re','FontSize',15); ylabel('Energy','FontSize',15);grid on
 
subplot(1,2,2); plot(ReTab(1:length(en)),en,'r','LineWidth',2)
xlim([52 55]); xlabel('Re','FontSize',15); ylabel('Energy','FontSize',15); grid on
            
            
%% Velocity profiles 
XposList = [-Lpipe 0 1]; Y=[0:0.001:8]; 
NPos = length(XposList); 
uxProf = zeros(NPos,length(Y)); urProf = zeros(NPos,length(Y));
for index = 1:NPos
    uxProf(index,:)=SF_ExtractData(bf,'ux',XposList(index),Y);
    urProf(index,:)=SF_ExtractData(bf,'ur',XposList(index),Y);
end
figure; plot(uxProf,Y);xlabel('Ux(r,1)');ylabel('r');title('axial velocity');legend('x = -5.2','x = 0','x = 1')
figure; plot(urProf,Y);xlabel('Ur(r,1)');ylabel('r');title('radial velocity');legend('x = -5.2','x = 0','x = 1')             


%% STABILITY ANALYSIS

MappingParams = [-10,30,-5,5,-0.3,5,-0.3,5];

[ev,em] = SF_Stability(bf,'m',0,'shift',0.05,'nev',1,'MappingDef','box','MappingParams',MappingParams);
[evA,emA] = SF_Stability(bf,'type','A','m',0,'shift',ev,'nev',1);

                   
emS=SF_Sensitivity(bf, em, emA,'Type','S');

figure();
subplot(3,1,1);SF_Plot(em,'ux','xlim',[-0.5 10],'ylim',[0 8]);
title(['Direct Mode, \lambda = ',num2str(ev)]);
subplot(3,1,2);SF_Plot(emA,'ux','xlim',[-0.5 10],'ylim',[0 8]);
title(['Adjoint Mode, \lambda = ',num2str(ev)]);
subplot(3,1,3);SF_Plot(emS,'sensitivity','xlim',[-0.5 10],'ylim',[0 8],'colormap','ice');
title('Sensitivity');
%% SUMMARY OF ALL WE HAVE DONE SO FAR


SF_Status;


% [[PUBLISH]]





            