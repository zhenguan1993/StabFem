/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////
/////
///// 
/////                              MESH GENERATOR for concentric jets
/////
/////             This file creates a mesh for the "StabFem" program.
/////
/////             input parameters :  R1 , LS, D2, Lpipe, Xmax, Rmax
/////  			 
/////
/////
/////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////



//						(Xmax). (lab 3)
//                _______________________________________________________
//                |                                                     |
//                |                                                     |
//       (lab 22) |                                                     |
//                |                                                     |
//                |                                                     |
//          ------|                                                     |
//          |                                                           | (Rmax)
//  (dim D2)|                                                           | (lab 3) 
//  (lab 11)|                                                           |
//          ------|                                                     |
//        (dim LS)|                                                     |
//       (lab 21) |                                                     |
//          ------|                                                     |
//  (dim R1)|                                                           |
//  (lab 1 )|___________________________________________________________|
//								(lab 6)
//


include "Macros_StabFem.idp" 



real pipeL;//=10;
pipeL = 0;
real n=.5;                 
int ns=10; 

cout << " Generation of an initial mesh for CONCENTRIC JETS" << endl;
cout << " Enter the dimensions R1 (radius 1st jet) , LS (separation), D2 (diameter 2nd jet), Lpipe (length of pipes), Xmax , Rmax? " << endl; 
real R1 , LS, D2,Xmax,Rmax;
cin >> R1 >> LS >> D2 >> pipeL >> Xmax >> Rmax;




cout << "RADIUS OF THE FIRST HOLE R1 = " << R1 << endl; 
cout << "Length separating the two holes : LS = " << LS << endl;  
cout << "DIAMETER OF THE ANULAR HOLE D2 = " << D2 << endl;
cout << " Length of the pipe " << pipeL << endl;
cout << "SIZE OF DOMAIN : Xmax = " << Xmax << " ; Rmax = " << Rmax << endl;


//cout << " Enter the two velocities : " << endl; 
//real Umax1; real Umax2;
//cin >> Umax1 >> Umax2 ;
//cout << " Velocity : Umax1 = " << Umax1 << " ; Umax2 = " << Umax2 << endl;
//int bctype;
//cout << "Type of Boundary condition at inlet (0 = constant ; 1 = Poiseuille ; 2 = Tanh) : " << endl;
//cin >> bctype;


/* Inlet boundaries */
border hole1(t=R1,0){x=-pipeL;y=t;label=1;}
border hole2(t=D2,0){x=-pipeL;y=R1+LS+t;label=11;}
/* Vertical walls */
border plate1(t=LS,0){x=0;y=R1+t;label=21;}
border plate2(t=(Rmax-LS-R1-D2),0){x=0;y=R1+LS+D2+t;label=22;}
/* Horizontal walls of the pipe */
border plate11(t=0,-pipeL){ x=t;y=R1;label=21;}
border plate12(t=-pipeL,0){ x=t;y=R1+LS;label=21;}
border plate22(t=0,-pipeL){ x=t;y=R1+LS+D2;label=22;}
/* Axis */
border axis1(t=-pipeL,R1){ x=t;y=0;label=6;}
border axis2(t=R1,Xmax){ x=t;y=0;label=6;}
/* Outlet */
//border outlet(t=0,pi/2){ x=(Xmax-Rmax)+Rmax*cos(t);y=Rmax*sin(t);label=3;}
border outlet(t=0,Rmax){ x=Xmax;y=t;label=3;}
/* Upper boundary of the domain */
border latsup(t=Xmax,0){x=t;y=Rmax;label=51;} // 22 for wall or 3 for no-stress outlet or 51 for slip



plot(hole1(R1*ns)+plate1(LS*n+ns*.1)+hole2(D2*ns)+plate2((Rmax-LS-R1-D2)*n)
	+axis1(R1*ns)+axis2((Xmax-R1)*n)+plate11(pipeL*ns+1)+plate12(pipeL*ns+1)+plate22(pipeL*ns+1)
	+outlet(Rmax*n)+latsup(Xmax*n),wait=1,dim=1);



mesh th=buildmesh(hole1(R1*ns+1)+plate1(LS*n+ns*.1+1)+hole2(D2*ns+1)+plate2((Rmax-LS-R1-D2)*n+1)
	+axis1((pipeL+R1)*ns+1)+axis2((Xmax-R1)*n+1)+plate11(pipeL*ns+1)+plate12(pipeL*ns+1)+plate22(pipeL*ns+1)
	+outlet(Rmax*n+1)+latsup(Xmax*n+1));


// AUXILIARY FILE for Stabfem : SF_Geom.edp (Legacy but used for this case)
{
			ofstream file2("SF_Geom.edp"); 
			file2 << "// Description geometry (file automatically created ; to be included in the FreeFem programs)" << endl;
            file2 << " real SFGeomLengthscale = 1 ; // length scale for nondimensionalization" << endl ;
            file2 << " real SFGeomVelocityscale = 1 ; // velocity scale for nondimensionalization"  << endl ;
            file2 << " real SFGeomR1 = " << R1  << " ; " << endl ;
            file2 << " real SFGeomLS = " << LS  << " ; " << endl ;
            file2 << " real SFGeomD2 = " << D2  << " ; " << endl ;
//			file2 << " real SFGeomUmax1 = " << Umax1 << " ; " << endl ;
//          file2 << " real SFGeomUmax2 = " << Umax2 << " ; " << endl ;
            file2 << " real SFGeomBI = 5. ; " << endl; // thickness parameter for tanh profile
            file2 << " real SFGeomBO = 5. ; " << endl; // thickness parameter for tanh profile
//            file2 << " int SFGeomInletType = " << bctype << " ; // (0 for constant, 1 for Poiseuille, 2 for Tanh) " << endl ;
}



plot(th,wait=0);


 // SAVE THE MESH in mesh.msh file 
savemesh(th,ffdatadir+"mesh.msh");

// SECOND AUXILIARY FILE  for Stabfem : mesh.ff2m
IFMACRO(SFWriteMesh)
	SFWriteMesh(ffdatadir+"mesh.ff2m",th,"initial")
ENDIFMACRO

// AUXILIARY FILE NEEDED TO PLOT P2 DATA
IFMACRO(SFWriteConnectivity)
	SFWriteConnectivity(ffdatadir+"mesh_connectivity.ff2m",th);
ENDIFMACRO



