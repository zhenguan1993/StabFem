%% StabFem Demo script for concentric jets WITH CONSTANT PROFILE.
%
% Here we set Umax,2 = 1 for the outer jet  and Umax,1 = 3/4 so that the ratio of MEAN VELOCITIES
% is the same as in the case of Poiseulle profile with Umax =1 for each jet
%
% The arclength continuation is done in the plane (Re,beta) where beta is
% the "monitor" defined as beta = 100*int( ur* exp(-(x-2)^2 ).
% you can also use the energy as a monitor. For this just remove customized
% definitions of beta and dbetadu in SF_Custom.idp, the solver will switch
% to default ones.
% 
% NB : 
%
% - Using new generic interface
% - postprocessing is done completely from database exploration


%% Chapter 0 : Initialization 
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK_ArcLength_Constant/'); % NB use verbosity=2 for autopublish mode ; 4 during work
% SF_core_arborescence('cleanall'); % uncomment this line to clean database
SF_core_setopt('ErrorIfDiverge',false); % recommended option when using continuation mode
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%% Chapter 1 : Generate a mesh and initial base flow
% Parameters
U1Max= 3/4.; % to have same ratio of mean velocities as for Poiseuille case   
U2Max=1.;    % set to 1
R1=0.5; distance=4.5; D2=1; Lpipe = 0.01;
Xmax = 100;    
YMax = 20;     
bctype = 0; % select 0 for constant and 1 for Poiseuille and 2 for Tanh
% Mesh generation
paramMESH = [R1 distance D2 Lpipe Xmax YMax];
ffmesh = SF_Mesh('Mesh_ConcentricJets_Pipes.edp','params',paramMESH,'cleanworkdir','yes');
figure;subplot(2,1,1);SF_Plot(ffmesh,'mesh','title','Initial mesh');
subplot(2,1,2);SF_Plot(ffmesh,'mesh','title','Initial mesh','xlim',[-2 2],'ylim',[0 2]);

% Generation of an initial solution
bf = SF_BaseFlow(ffmesh,'Re',1,'bctype',bctype,'U1',U1Max,'U2',U2Max,'solver','Newton_Axi.edp');


% Adapt
%bf = SF_Adapt(bf,'anisomax',3,'nbjacoby',3);
% plots
%figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8]);
%Xpos = 1; Y=[0:0.01:1.5]; 
%uxline=SF_ExtractData(bf,'ux',Xpos,Y);
%figure; plot(uxline,Y);xlabel('Ux(r,1)');ylabel('r');title('axial velocity at location x=1');

%% loop with increasing Re
% Increasing Reynolds in a structured manner
ReTab = [10,30,50,55,60,65];
for Re = ReTab
    bf = SF_BaseFlow(bf,'Re',Re);
    [bf] = SF_Adapt(bf,'anisomax',3,'nbjacoby',10,'Hmax',2); 
%    [bf] = SF_Adapt(bf,'anisomax',3,'nbjacoby',10,'Hmax',5); 
end

%% plot last BF after this stage
figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8],'title',['Re = ',num2str(bf.Re),'; E = ',num2str(bf.Energy)] );
hold on;  SF_Plot(bf,'psi','contour','only','xlim',[-1 12],'ylim',[0 8],'Clevels',[ - 1:.1:-.1, 0:.01:.0625,.1,.2, .5:.5:10]);

figure;subplot(2,1,1);SF_Plot(bf.mesh,'mesh','title','Adapted mesh');
subplot(2,1,2);SF_Plot(bf.mesh,'mesh','title','Initial mesh','xlim',[-2 2],'ylim',[0 2]);


%% Test Arclength
%

 bf = SF_BFContinue(bf,'step',0.001,'solver','ArcLengthContinuationAxi.edp');
% note that the first step is still backwards and I don't understand why !!!


%% 
% Arclength continuation loop

Re = bf.Re;
ReMax = 80; ReMin = 25; % this is the termination of the loop. Remin is in case the curve does not loop back.
stepMax = 2;
step = stepMax; 
%EnerList = []; betaList = []; ReList = []; // NOW USELESS IF USING DATABASE !

while ( bf.Re < ReMax && bf.Re > ReMin && step > 1e-3)
    bf=SF_BFContinue(bf,'step',step,'solver','ArcLengthContinuationAxi.edp');
    if(bf.iter < 0)
        disp("Not converged. Arc-length step reduced by a factor 2");
        step = 0.5*step;
        bf = SF_BFContinue(bf,'step',step,'solver','ArcLengthContinuationAxi.edp');
         if(bf.iter < 0)
            disp("Not converged. Reducing again by a factor 10");
            step = 0.1*step;
            bf = SF_BFContinue(bf,'step',step,'solver','ArcLengthContinuationAxi.edp');
            if bf.iter<0
                disp('Arclength continuation failed')
                break
            end
         end
    end
    step = min(1.4*step,stepMax); % raise the step for next iteration
    Re = bf.Re;
    figure(45);plot(Re,bf.beta,'*'); pause(0.1); hold on;
end


%% Generating the summary of Data Base for postprocessing 

sfs = SF_Status;

%% redraw figure from database

figure; plot([sfs.BASEFLOWS.Re],[sfs.BASEFLOWS.beta]);xlabel('Re'); ylabel('\beta');xlim([ReMin,ReMax]);

figure; plot([sfs.BASEFLOWS.Re],[sfs.BASEFLOWS.Energy]);xlabel('Re'); ylabel('E');xlim([ReMin,ReMax]);


%% Plot last Base flow


figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8],'title',['Re = ',num2str(bf.Re),'; E = ',num2str(bf.Energy)] );
hold on;  SF_Plot(bf,'psi','contour','only','Clevels',[ - 1:.1:-.1, 0:.01:.0625,.1,.2, .5:.5:10],'xlim',[-1 12],'ylim',[0 8]);

% larger range
figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8],'title',['Re = ',num2str(bf.Re),'; E = ',num2str(bf.Energy)] );
hold on;  SF_Plot(bf,'psi','contour','only','Clevels',[ - 1:.1:-.1, 0:.01:.0625,.1,.2, .5:.5:10],'xlim',[-1 100],'ylim',[0 20]);


%% compare with base flow for Re=30 from main branch 

bf= SF_Load('BASEFLOWS',6);
figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8],'title',['Re = ',num2str(bf.Re),'; E = ',num2str(bf.Energy)] );
hold on;  SF_Plot(bf,'psi','contour','only','Clevels',[ - 1:.1:-.1, 0:.01:.0625,.1,.2, .5:.5:10],'xlim',[-1 12],'ylim',[0 8]);

% larger range
figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8],'title',['Re = ',num2str(bf.Re),'; E = ',num2str(bf.Energy)] );
hold on;  SF_Plot(bf,'psi','contour','only','Clevels',[ - 1:.1:-.1, 0:.01:.0625,.1,.2, .5:.5:10],'xlim',[-1 100],'ylim',[0 20]);


% [[PUBLISH]]