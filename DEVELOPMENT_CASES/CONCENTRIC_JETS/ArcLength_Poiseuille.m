%% StabFem Demo script for concentric jets WITH POISEUILLE PROFILE, Umax = 1 for each jet
%
% The arclength continuation is done in the plane (Re,beta) where beta is
% the "monitor" defined as beta = 100*int( ur* exp(-(x-2)^2 ).
% you can also use the energy as a monitor. For this just remove customized
% definitions of beta and dbetadu in SF_Custom.idp, the solver will switch
% to default ones.
% 
% NB : 
%
% - Using new generic interface
% - postprocessing is done completely from database exploration
% - not that here Re is based on maximum velocities. To use MEAN velocities
%   one should use U1 = 2 and U2 = 1.5. The Reynolds numbers should be
%   modified to compare with the case using mean profile.
%   

%% Chapter 0 : Initialization 
close all;
addpath([fileparts(fileparts(pwd)), '/SOURCES_MATLAB']);
SF_Start('verbosity',4,'ffdatadir','./WORK_Arclenth_Poiseuille/'); % NB use verbosity=2 for autopublish mode ; 4 during work
% SF_core_arborescence('cleanall'); % uncomment this line to clean database
SF_core_setopt('ErrorIfDiverge',false); % recommended option when using continuation mode
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');

%% Chapter 1 : Generate a mesh and initial base flow
% Parameters
U1Max=1.;   % umax = 1 -> umean = 1/2 for central jet  
U2Max=1.;   % umax = 1 -> umean = 2/3 for external jet (quasi 2D)
R1=0.5; distance=4.5; D2=1; Lpipe = 5.2; % NB we use a long pipe for Poiseuille
Xmax = 100;    
YMax = 20;     
bctype = 1; % select 0 for constant and 1 for Poiseuille and 2 for Tanh
% Mesh generation
paramMESH = [R1 distance D2 Lpipe Xmax YMax];
ffmesh = SF_Mesh('Mesh_ConcentricJets_Pipes.edp','params',paramMESH,'cleanworkdir','yes');

% Generation of an initial solution
bf = SF_BaseFlow(ffmesh,'Re',1,'bctype',bctype,'U1',U1Max,'U2',U2Max,'solver','Newton_Axi.edp');
% Adapt
bf = SF_Adapt(bf,'anisomax',3,'nbjacoby',3);
% plots
%figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8]);
%Xpos = 1; Y=[0:0.01:1.5]; 
%uxline=SF_ExtractData(bf,'ux',Xpos,Y);
%figure; plot(uxline,Y);xlabel('Ux(r,1)');ylabel('r');title('axial velocity at location x=1');

%%
% Increasing Reynolds in a structured manner
ReTab = [10,30,50,70,75,77];
for Re = ReTab
    bf = SF_BaseFlow(bf,'Re',Re);
    [bf] = SF_Adapt(bf,'anisomax',3,'nbjacoby',10,'Hmax',2); 
%    [bf] = SF_Adapt(bf,'anisomax',3,'nbjacoby',10,'Hmax',5); 
end

%% plot last BF after this stage
figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8],'title',['Re = ',num2str(bf.Re),'; E = ',num2str(bf.Energy)] );
hold on;  SF_Plot(bf,'psi','contour','only','xlim',[-1 12],'ylim',[0 8],'Clevels',[ - 1:.1:-.1, 0:.01:.0625,.1,.2, .5:.5:10]);
pause(0.1);

figure;subplot(2,1,1);SF_Plot(bf,'mesh','title','Adapted mesh');
subplot(2,1,2);SF_Plot(bf,'mesh','xlim',[-2 2],'ylim',[0 2]);


%% Test Arclength
%

 bf = SF_BFContinue(bf,'step',0.001,'solver','ArcLengthContinuationAxi.edp');
% note that the first step is still backwards and I don't understand why !!!


%% 
% Arclength continuation loop

Re = bf.Re;
ReMax = 90; ReMin = 60; % this is the termination of the loop. Remin is in case the curve does not loop back.
stepMax = 1;
step = stepMax; 
%EnerList = []; betaList = []; ReList = []; // NOW USELESS IF USING DATABASE !

while ( bf.Re < ReMax && bf.Re > ReMin && step > 1e-3)
    bf=SF_BFContinue(bf,'step',step,'solver','ArcLengthContinuationAxi.edp');
    if(bf.iter < 0)
        disp("Not converged. Arc-length step reduced by a factor 2");
        step = 0.5*step;
        bf = SF_BFContinue(bf,'step',step,'solver','ArcLengthContinuationAxi.edp');
         if(bf.iter < 0)
            disp("Not converged. Reducing again by a factor 10");
            step = 0.1*step;
            bf = SF_BFContinue(bf,'step',step,'solver','ArcLengthContinuationAxi.edp');
            if bf.iter<0
                disp('Arclength continuation failed')
                break
            end
         end
    end
    step = min(1.4*step,stepMax); % raise the step for next iteration
    Re = bf.Re;
    figure(45);plot(Re,bf.beta,'*'); pause(0.1); hold on;
end


%% Generating the summary of Data Base for postprocessing 

sfs = SF_Status;

%% redraw figure from database

figure; plot([sfs.BASEFLOWS.Re],[sfs.BASEFLOWS.beta]);xlabel('Re'); ylabel('\beta');xlim([70,90]);

figure; plot([sfs.BASEFLOWS.Re],[sfs.BASEFLOWS.Energy]);xlabel('Re'); ylabel('E');xlim([70,90]);


%% Plot 3 Base flows for Re = 82

% pick up first base flow : find the number in database corresonding to BF closest to [Re,E] = [82,105.5]
% 
[~,index1] = min(([sfs.BASEFLOWS.Re]-82).^2+([sfs.BASEFLOWS.Energy]-105.5).^2);
bf = SF_Load('BASEFLOWS',index1);

figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8],'title',['Re = ',num2str(bf.Re),'; E = ',num2str(bf.Energy)] );
hold on;  SF_Plot(bf,'psi','contour','only','xlim',[-1 12],'ylim',[0 8],'Clevels',[ - 1:.1:-.1, 0:.01:.0625,.1,.2, .5:.5:10]);

% pick up second base flow : find the number in database corresonding to BF closest to [Re,E] = [82,103.5]
% 
[~,index1] = min(([sfs.BASEFLOWS.Re]-82).^2+([sfs.BASEFLOWS.Energy]-103.5).^2);
bf = SF_Load('BASEFLOWS',index1);

figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8],'title',['Re = ',num2str(bf.Re),'; E = ',num2str(bf.Energy)] );
hold on;  SF_Plot(bf,'psi','contour','only','xlim',[-1 12],'ylim',[0 8],'Clevels',[ - 1:.1:-.1, 0:.01:.0625,.1,.2, .5:.5:10]);


% pick up third base flow : find the number in database corresonding to BF closest to [Re,E] = [82,99.5]
% 
[~,index1] = min(([sfs.BASEFLOWS.Re]-82).^2+([sfs.BASEFLOWS.Energy]-99.5).^2);
bf = SF_Load('BASEFLOWS',index1);

figure; SF_Plot(bf,'ux','xlim',[-1 8],'ylim',[0 8],'title',['Re = ',num2str(bf.Re),'; E = ',num2str(bf.Energy)] );
hold on;  SF_Plot(bf,'psi','contour','only','xlim',[-1 12],'ylim',[0 8],'Clevels',[ - 1:.1:-.1, 0:.01:.0625,.1,.2, .5:.5:10]);



%% Appendix : attempt to compute m=0 eigenvalues along all branches
%
% We do that using a loop over baseflows from database
%
% for jbase = 14:length(sfs.BASEFLOWS)
%    bf = SF_Load('BASEFLOWS',jbase)
%    ev = SF_Stability(bf,'solver','StabAxi.edp','m',0,'nev',10,'shift',0.2i+0.2);
%    figure(34); hold on;plot(ev,'*');
% end
% 
% DAVID : I am not able to compute eigenvalues when the shift is close to 0
%
% [[PUBLISH]]