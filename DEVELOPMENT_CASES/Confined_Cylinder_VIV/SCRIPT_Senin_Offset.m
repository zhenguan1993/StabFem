%% Demo of the flow around a cylinder in a channel with offset


%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_Start('verbosity',3);
mkdir('FIGURES');
SF_core_arborescence('cleanall');

%% Base flow for Re = 25, Yoffest = 0.05

    chi = 2/3.; Xmin = -5; Xmax = 7; 
    Yoffset = 0.05;
    Re = 25;
    ffmesh = SF_Mesh('Mesh_CylinderConfined_Offset.edp','Params',[chi,Yoffset,Xmin,Xmax],'problemtype','2D');
    bf = SF_BaseFlow(ffmesh,'Re',1);

    bf = SF_BaseFlow(bf,'Re',Re);
    bf = SF_Adapt(bf,'Hmax',.1);
     
%% 
% a few plots
figure; SF_Plot(bf,'mesh');
figure; SF_Plot(bf,'ux','Contour','on','ColorMap','jet','xlim',[-2 4],'ylim',[-2 2]);

Yline = [-.5 :.001 :.5];
Uinlet = SF_ExtractData(bf,'ux',-4,Yline);
Uinlet0 = SF_ExtractData(bf,'ux',0,Yline);
plot(Uinlet,Yline,Uinlet0,Yline); %to check the parabolic profile


%% Now Re = 200

Re = 200;


%%
 Yoffset = 0.01;
    ffmesh = SF_Mesh('Mesh_CylinderConfined_Offset.edp','Params',[chi,Yoffset,Xmin,Xmax],'problemtype','2D','symmetry','N');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    bf = SF_BaseFlow(bf,'Re',25);
    bf = SF_Adapt(bf,'Hmax',.1);
    bf = SF_BaseFlow(bf,'Re',100);
    bf = SF_BaseFlow(bf,'Re',150);
    bf = SF_BaseFlow(bf,'Re',175);
    bf = SF_BaseFlow(bf,'Re',Re);
    
Yoffset 
bf.Fy/Yoffset

%%
 Yoffset = 0.005;
    ffmesh = SF_Mesh('Mesh_CylinderConfined_Offset.edp','Params',[chi,Yoffset,Xmin,Xmax],'problemtype','2D','symmetry','N');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    bf = SF_BaseFlow(bf,'Re',25);
    bf = SF_Adapt(bf,'Hmax',.1);
    bf = SF_BaseFlow(bf,'Re',100);
    bf = SF_BaseFlow(bf,'Re',150);
    bf = SF_BaseFlow(bf,'Re',175);
    bf = SF_BaseFlow(bf,'Re',Re);
    
Yoffset 
bf.Fy/Yoffset

%%
 Yoffset = 0.001;
    ffmesh = SF_Mesh('Mesh_CylinderConfined_Offset.edp','Params',[chi,Yoffset,Xmin,Xmax],'problemtype','2D','symmetry','N');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    bf = SF_BaseFlow(bf,'Re',25);
    bf = SF_Adapt(bf,'Hmax',.1);
    bf = SF_BaseFlow(bf,'Re',100);
    bf = SF_BaseFlow(bf,'Re',150);
    bf = SF_BaseFlow(bf,'Re',175);
    bf = SF_BaseFlow(bf,'Re',Re);
    
Yoffset 
bf.Fy/Yoffset



%% NOW REPEATING EVERYTHING WITH ADAPTED MESH
    Yoffset = 0.05;
    ffmesh = SF_Mesh('Mesh_CylinderConfined_Offset.edp','Params',[chi,Yoffset,Xmin,Xmax],'problemtype','2D','symmetry','N');
    bf = SF_BaseFlow(ffmesh,'Re',1);
   bf = SF_BaseFlow(bf,'Re',25);
    bf = SF_Adapt(bf,'Hmax',.1);
    bf = SF_BaseFlow(bf,'Re',100);
    bf = SF_BaseFlow(bf,'Re',150);
    bf = SF_BaseFlow(bf,'Re',175);    
    bf = SF_BaseFlow(bf,'Re',Re);
    Mask = SF_Mask(bf.mesh,[-1 2 -1 1 .02]);
    bf = SF_Adapt(bf,Mask,'Hmax',.1);
     
%% 
% a few plots
figure; SF_Plot(bf,'mesh');
figure; SF_Plot(bf,'ux','Contour','on','ColorMap','jet','xlim',[-2 4],'ylim',[-2 2]);



%% Flow with small values of offset (to compare with quasi-static case)
 Yoffset = 0.02;
    ffmesh = SF_Mesh('Mesh_CylinderConfined_Offset.edp','Params',[chi,Yoffset,Xmin,Xmax],'problemtype','2D','symmetry','N');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    Mask = SF_Mask(bf.mesh,[-1 2 -1 1 .02]);
    bf = SF_Adapt(bf,Mask,'Hmax',.1);
    bf = SF_BaseFlow(bf,'Re',25);
    bf = SF_Adapt(bf,'Hmax',.1);
    bf = SF_BaseFlow(bf,'Re',100);
    bf = SF_BaseFlow(bf,'Re',150);
    bf = SF_BaseFlow(bf,'Re',175);
    bf = SF_BaseFlow(bf,'Re',Re);
    Mask = SF_Mask(bf.mesh,[-1 2 -1 1 .02]);
    bf = SF_Adapt(bf,Mask,'Hmax',.1);
    
Yoffset 
bf.Fy/Yoffset

%%
 Yoffset = 0.01;
    ffmesh = SF_Mesh('Mesh_CylinderConfined_Offset.edp','Params',[chi,Yoffset,Xmin,Xmax],'problemtype','2D','symmetry','N');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    bf = SF_BaseFlow(bf,'Re',25);
    bf = SF_Adapt(bf,'Hmax',.1);
    bf = SF_BaseFlow(bf,'Re',100);
    bf = SF_BaseFlow(bf,'Re',150);
    bf = SF_BaseFlow(bf,'Re',175);    
    bf = SF_BaseFlow(bf,'Re',Re);
    Mask = SF_Mask(bf.mesh,[-1 2 -1 1 .02]);
    bf = SF_Adapt(bf,Mask,'Hmax',.1);
    
Yoffset 
bf.Fy/Yoffset

%%
 Yoffset = 0.005;
    ffmesh = SF_Mesh('Mesh_CylinderConfined_Offset.edp','Params',[chi,Yoffset,Xmin,Xmax],'problemtype','2D','symmetry','N');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    bf = SF_BaseFlow(bf,'Re',25);
    bf = SF_Adapt(bf,'Hmax',.1);
    bf = SF_BaseFlow(bf,'Re',100);
    bf = SF_BaseFlow(bf,'Re',150);
    bf = SF_BaseFlow(bf,'Re',175);    
    bf = SF_BaseFlow(bf,'Re',Re);
    Mask = SF_Mask(bf.mesh,[-1 2 -1 1 .02]);
    bf = SF_Adapt(bf,Mask,'Hmax',.1);
    
Yoffset 
bf.Fy/Yoffset

%%
 Yoffset = 0.001;
    ffmesh = SF_Mesh('Mesh_CylinderConfined_Offset.edp','Params',[chi,Yoffset,Xmin,Xmax],'problemtype','2D','symmetry','N');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    bf = SF_BaseFlow(bf,'Re',25);
    bf = SF_Adapt(bf,'Hmax',.1);
    bf = SF_BaseFlow(bf,'Re',100);
    bf = SF_BaseFlow(bf,'Re',150);
    bf = SF_BaseFlow(bf,'Re',175);    
    bf = SF_BaseFlow(bf,'Re',Re);
    Mask = SF_Mask(bf.mesh,[-1 2 -1 1 .02]);
    bf = SF_Adapt(bf,Mask,'Hmax',.1);
    
Yoffset 
bf.Fy/Yoffset


%% Summary

SF_Status


% [[PUBLISH]] 