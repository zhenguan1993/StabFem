%% Initialise
addpath('../../SOURCES_MATLAB/');
SF_core_start('verbosity',4,'workdir','./WORK/H10Rc12/'); % verbosity 4 to see what is sufficient
mkdir('FIGURES');

HC = 10; RcC=12; RoutC=200; LpipeC=10; ReTarget=144; meshRef=5;

bf = meshGeneration('Rc',RcC,'H',HC,'Rout',RoutC,'Lpipe',LpipeC,...
                            'ReEnd',ReTarget,'meshRef',meshRef,'ReFirstStep',...
                            4,'ReSectStep',4);

bfCont = SF_BaseFlow(bf,'Re',144);
[ev,em] = SF_Stability(bfCont,'nev',3,'shift',0.02+0.i,'k',0,'type','D','sym','N');       


% Start with a stable baseflow under steady modes
%%
% First bifurcation at Re = 140.5 
%bfCont = SF_BaseFlow(bfCont,'Re',146.3482);
iter = -1; % Used to control BFContinue bad iterations
%%
for i = [1:500]
    step = max(500*min(abs((ev))),0.05); % We take the closest to zero as ref
    %sign = -sign(ev(1)*ev(2)*ev(3)*ev(4)*ev(5)*ev(6));
    %step = step;
    disp(['The current step size, step = ',num2str(step)]);
% correct issue with baseflow_guess.txt
    while(iter == -1 || iter == 10)
        bfCont = SF_BFContinue(bfCont,'step',step);
        iter = bfCont.iter;
        if(iter == -1 || iter == 10 )
            step = step/10;
        end
    end
    [ev,em] = SF_Stability(bfCont,'nev',6,'shift',0.5+0.i,'k',0,'type','D','sym','N');
    BFArray(i) = bfCont;
    EV1Array(i) = ev(1);
    EV2Array(i) = ev(2);
    EV3Array(i) = ev(3);
    EV4Array(i) = ev(4);
    EV5Array(i) = ev(5);
    EV6Array(i) = ev(6);
    EM1Array(i) = em(1);
    EM2Array(i) = em(2);
    EM3Array(i) = em(3);
    EM4Array(i) = em(4);
    EM5Array(i) = em(5);
    EM6Array(i) = em(6);
    % Restart iter
    iter = -1;
end
%% 
for i=[1:500]
   bfRe(i) = BFArray(i).Re ;
   EK(i) = BFArray(i).EkinAxis;
   Fx(i) = BFArray(i).Fx;
   uxAxis(i) = BFArray(i).uxAxis;
   uyAxis(i) = BFArray(i).uyAxis;
end

save('BifurcationScenearioSteadyH10.mat','bfRe','EK','Fx','uxAxis','uyAxis',...
    'EV1Array','EV2Array','EV3Array','EV4Array','EV5Array','EV6Array');

figure;
plot(bfRe,EV1Array);
hold on;
plot(bfRe,EV2Array);
plot(bfRe,EV3Array);
plot(bfRe,EV4Array);
hold off;
legend('EV1','EV2','EV3','EV4')

figure;
plot(bfRe,EK);
figure;
plot(bfRe,Fx);
