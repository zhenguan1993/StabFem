/// 
/// file Macros_StabFem.edp
///
/// This file contains the case-dependent Macros for boundary conditions and postprocessing output.
///
/// This file can be customized according to requirements but it must contain the following macros :
///
/// boundary conditions : 
/// macro BoundaryconditionsBaseFlow(du,dp,dT,drho) 
/// macro BoundaryconditionsStability(du,us,dp,dT,drho,symmetry)
///
///
/// For post-processsing purposes :
/// macro SFWriteBaseFlow(filename,ux,uy,p,type,iter)
/// macro SFWriteMode(namefile,ux,uy,p,ev,shift,type,iter)
/// 
/// The following case is for the axisymmetric body and include the additional macros
/// required to compute lift and drag forces : Trainee(ux,uy,p,symmetry) and Portance(ux,uy,p,symmetry)
///
/// The file must also contain 




macro SLEPCDEV() 1// EOM For SLEPC EigSolve // PROBABLY USELESS




IFMACRO(!STORAGEMODES)
macro STORAGEMODES() "P2,P2P2P1P1P1,P2P2P2P1P1P1"// EOM
ENDIFMACRO 


// Azimuthal number

real m = 0;
// Sponge
real alpha=1e-4;
real xinfv=40; // X_max
real xinfm=-6.0; // X_min
real yinf=50.0;  // R_min
//



/////////////////////////GEOMETRY//////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
real Rhole	= 0.5;       		// radius of the hole (dimensionless)
real H	= 5.0;  		// altezza
real L	= 6;      	// lunghezza asse
real Lsponge=100.0;
real Hsponge=0.1;
real HspongeS=5;    //sponge stab
real Lpipe = Hsponge+HspongeS;
real Ltot=L+Lsponge;
real Ninit=4;
real Ninitsponge=1.0;
real expo2 = 1.5;
real expo1 = 0.8;
real HspongeInlet = 200;
real Lspongeinlet = 200; 

int bcinlet=1;
int bcinletS=19;   // dove imporre conizioni al contorno per la stabilita'
int bcinletinner=18;   // dove imporre conizioni al contorno per la stabilita'
int bcnoslip=2;
int bcnoslipinlet=21;
int bcoutlet=3;
int bcaxis=6;
int bcdonth=99;
int bcinneroutlet=16;
int bcinner=25;

IFMACRO(!MachNumber)
macro MachNumber() 0.385 // EOM
ENDIFMACRO
/////////////////////////U_inlet///////////////////////////////////////////
// Boundary condition for velocity TODOO!!!
real Rhoinlet=1.0;
real Tinlet = 1.0;
real Twall = 1.4*Tinlet;
real Uavholeinc=1.0;
real Rhoavholeinc=1.0;
real Mflux=pi*Rhole^2*Uavholeinc*Rhoavholeinc;  //mass flow in the pipe assuming u=rho=1 at the inlet


real xorig=-Lpipe; //per evitare sing in atan2
real yorig=0.0;
func R0=sqrt((x-xorig)^2+(y-yorig)^2);
func Teta0=atan2(y,x);
func Harris=(3*Mflux/(2*pi*Rhoinlet*R0(x,y)^2))*cos(Teta0(x,y))^2;
func Uinlet=-(N.x*Harris(x,y));
func Vinlet=-(N.y*Harris(x,y));
macro SFReCustom nu=1.0/Re; //EOM

///////////////////////////////////////////////////////////////////////////
/* Adiabatic walls at the pipe with uniform velocity profile */
/* Impinging plane has a hotter wall */

macro BoundaryconditionsBaseFlow(du,Ub,us)
	         on(bcinlet,du#x=Uinlet(x,y)-Ub#x,du#y=Vinlet(x,y)-Ub#y,du#rho=Rhoinlet-Ub#rho)
	         + on(bcoutlet,du#x=0.0-Ub#x)
	         + on(bcnoslip,du#x=-Ub#x,du#y=-Ub#y,du#T=Twall-Ub#T)
             + on(bcnoslipinlet,du#x=-Ub#x,du#y=-Ub#y)
      		 + on(bcaxis,du#y=-Ub#y) 
//EOM 

/*
macro BoundaryconditionsBaseFlow(du,dp,dT,drho,Uinlet)
	         on(1,11,du#x=-(N.x*Uin)-Ub#x,du#y=0.0-Ub#y,drho=1.-Rhob)
	         + on(3,du#x=0.0-Ub#x,du#y=0.0-Ub#y,drho=1.-Rhob)
	         + on(2,21,du#x=-Ub#x,du#y=-Ub#y)
      		 + on(6,du#y=-Ub#y) 
//EOM 
*/
macro BoundaryconditionsStability(du,us)
	         on(bcinlet,dux=0.0,du#rho=0.0) 
             + on(bcoutlet,du#x=0.0)
             + on(bcnoslip,du#x=0.0,du#y=0.0,du#T=0.0)
             + on(bcnoslipinlet,du#x=0.0,du#y=0.0)
             + on(bcaxis,du#y=0.0)
             //EOM
// HERE ARE SOME MACROS WHICH ARE USEFUL FOR POST-PROCESSING WITH A AXISYMMETRIC FLOW 

macro Trainee(u,symmetry)   
	(
	int1d(th,2,21,22,23)(2*pi*y*(u#p-1)/(gamma*Ma^2)*N.x)
	-nu*int1d(th,2,21,22,23)( 2*pi*y*(((4./3.)*dx(u#x) - (2./3.)*dy(u#y))*N.x+ (dx(u#y)+dy(u#x))*N.y))
	)
//EOM

macro Portance(u,symmetry)   
	(
	int1d(th,2,21,22,23)(2*pi*y*((u#p-1)/(gamma*Ma^2))*N.y)
	-nu*int1d(th,2,21,22,23)(2*pi*y*(((4./3.)*dy(u#y) - (2./3.)*dx(u#x))*N.y+(dx(u#y)+dy(u#x))*N.x))
	)
//EOM




macro SFWriteMesh(filename,TH,typemesh)
{
     ofstream fileFF(filename);
     fileFF << "### Data generated by Freefem++ ; " << endl;
     fileFF << "Mesh (generated by generic macro)" << endl;
     fileFF << "datatype Mesh meshtype 2D generationmode " << typemesh << endl;
	 fileFF << "int np int Ndof real deltamin real deltamax" ;
	 string PostProcessMeshDescription = " real deltaA real deltaB real deltaC real deltaD" ; /*description of customizable part*/
	 fileFF << PostProcessMeshDescription << endl << endl ;
     fespace femp1N(TH,P1);
     fespace femp2xfemp2xfemp1N(TH,[P2,P2,P1]);	  
     femp2xfemp2xfemp1N [uNx,uNy,uNp];
	 fileFF << TH.nv <<  endl << uNx[].n << endl ;
 	 cout << "#### Mesh information :" << endl;  
     cout << "## Number of vertices    " << TH.nv <<  endl;
	 cout << "## Number of DOF : " << uNx[].n << endl << endl;  
 	 femp1N delta = hTriangle;
	cout << "## delta min : " << delta[].min << endl;
	cout << "## delta max : " << delta[].max << endl;
	fileFF << delta[].min << endl;
	fileFF << delta[].max << endl;   
    /*Next is customizable part ; here determination of the grid size at several points */
    real[int] xprobe = [0,	2.5,	4,	10];
    real[int] yprobe = [.5, 	.5,	0,	0]; 
	cout << "## delta(0,.5) : ( boundary layer) : " << delta(xprobe(0),yprobe(0)) << endl ;
	cout << "## delta(2.5,.5) : ( sensitivity) : " << delta(xprobe(1),yprobe(1)) << endl ;
	cout << "## delta(4,0) : ( wake ) : " << delta(xprobe(2),yprobe(2)) << endl << endl ;
    cout << "## delta(10,0) : ( far wake ) : " << delta(xprobe(3),yprobe(3)) << endl << endl ;
	for(int ii = 0; ii<4;ii++){ fileFF << delta(xprobe(ii),yprobe(ii)) << endl ;}	
 
}; 	 
//EOM

macro CalcPsi(psi,vort,u,rho,test)
      solve LapLace(psi,test) = int2d(th)(y*(dx(psi)*dx(test)+dy(psi)*dy(test)  ))
					         - int2d(th)( y*(rho*vort*test - (u#x*dy(rho)+u#y*dx(rho))*test) )
                             - int1d(th,6,1,3) (y*(rho*u#x*N.x - rho*u#y*N.y)*test )
					         + on(6,2,21,psi=0);
//EOM

IFMACRO(!SFWriteBaseFlow)
macro SFWriteBaseFlow(filename,u,typeFlow,iter)
	{
		fespace p1forff2m(th,P1);  
	    p1forff2m vort,divergence,Mach,mFlow,psi,rho,rhoux,rhouy,test;
        rho = u#rho;
		vort=dy(u#x)-dx(u#y); /* vorticity */
	    divergence=dx(u#x)+dy(u#y);
        Mach = Ma*sqrt(u#x^2+u#y^2)/sqrt(u#T);
        rhoux = u#rho*u#x;
        rhouy = u#rho*u#y;
        CalcPsi(psi,vort,u,rho,test)
		ofstream file(filename);
	   	{
		file << "### Data generated by Freefem++ ; " << endl;
	    file << typeFlow << " for a 2D-compressible problem " << endl;
	    file << "datatype " << typeFlow << " datastoragemode ReP2P2P1P1P1.2 datadescriptors ux,uy,p,T,rho,Re,Ma" << endl;
		file << "real* Re real* Ma P1 vort P1 div P1 MachField P1 rhoux P1 rhouy P1 psi real iter " << endl << endl ; 
		file << Re << endl;	
	    file << Ma << endl;
				for (int j=0;j<vort[].n ; j++) file << vort[][j] << endl;
	            for (int j=0;j<divergence[].n ; j++) file << divergence[][j] << endl;
                for (int j=0;j<Mach[].n ; j++) file << Mach[][j] << endl;
                for (int j=0;j<rhoux[].n ; j++) file << rhoux[][j] << endl;
                for (int j=0;j<rhouy[].n ; j++) file << rhouy[][j] << endl;
                for (int j=0;j<psi[].n ; j++) file << psi[][j] << endl;
				file << iter << endl;
		}
	}; 
// EOM
ENDIFMACRO

macro SFWriteMode(namefile,u,ev,shift,typeFlow,iter)
{
	ofstream file(namefile);
	fespace p1forff2m(th,P1); 
	p1forff2m xx,yy;
    p1forff2m<complex> vort,divergence,Mach,psi,rhoux,rhouy,test;
    vort=dy(u#x)-dx(u#y); /* vorticity */
    divergence=dx(u#x)+dy(u#y);
    Mach = Ma*sqrt(u#x^2+u#y^2)/sqrt(u#T);
    rhoux = u#rho*u#x;
    rhouy = u#rho*u#y;
	xx=x;
	yy=y;
	file << "### Data generated by Freefem++ ; " << endl;
	file << "Eigenmode for a Axi-compressible problem " << endl;
	file << "datatype " << typeFlow <<" datastoragemode CxP2P2P1P1P1.4 datadescriptors ux,uy,p,T,rho,Re,Ma,lambdar,lambdai" << endl;
	string descriptionFF="real* Re real* Ma complex* lambda complex shift int iter P1c vort P1c divergence P1c MachField P1c rhoux P1c rhouy "; 
	file << descriptionFF << endl << endl ; 
	file << Re  << endl << Ma << endl << real(ev) << " " << imag(ev) << " " << real(shift) << " " << imag(shift) << " " << iter << endl << endl;
	for (int j=0;j<vort[].n ; j++) file << real(vort[][j]) << " " << imag(vort[][j]) << endl;
    for (int j=0;j<divergence[].n ; j++) file << real(divergence[][j]) << " " << imag(divergence[][j]) << endl;
    for (int j=0;j<Mach[].n ; j++) file << real(Mach[][j]) << " " << imag(Mach[][j]) << endl;
    for (int j=0;j<rhoux[].n ; j++) file << real(rhoux[][j]) << " " << imag(rhoux[][j]) << endl;
    for (int j=0;j<rhouy[].n ; j++) file << real(rhouy[][j]) << " " << imag(rhouy[][j]) << endl;
	};
//EOM	

