
%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_core_start('verbosity',4,'workdir','./WORK/');
subplot = @(m,n,p) subtightplot (m, n, p, [0.05 0.05], [0.05 0.03], [0.05 0.01]);
mkdir('FIGURES')
%% Chapter 1 : builds or recovers base flow

sfstat = SF_Status();
if strcmp(sfstat.status,'new')
    % creation of an initial mesh
    H = 2; Lpipe = 10; Rout = 80; AspectRatio = H; nRheo = 0.5; Cu = 1.0;
    ffmesh = SF_Mesh('mesh_ImpactingJet.edp','Params',[H Lpipe Rout],'problemtype','2drheology');
    bf = SF_BaseFlow(ffmesh,'Re',1,'AspectRatio',AspectRatio,'nRheo',nRheo,'Cu',Cu);
    bf = SF_BaseFlow(bf,'Re',10,'AspectRatio',AspectRatio,'nRheo',nRheo,'Cu',Cu);

    bf = SF_Adapt(bf,'Hmax',5);
    
    % computation of base flow with increasing Re
    for Re = [20:10:100]
        bf = SF_BaseFlow(bf,'Re',Re);
    end
    bf = SF_Adapt(bf);
    for Re = [110:10:150]
        bf = SF_BaseFlow(bf,'Re',Re);
    end
    bf = SF_Adapt(bf,'Hmax',2);
    for Re = [160:10:300]
        bf = SF_BaseFlow(bf,'Re',Re);
        bf = SF_Adapt(bf,'Hmax',2);
    end
    
    % Adaptation to the structure of an eigenmode 
    [ev,em] = SF_Stability(bf,'nev',1,'shift',-0.0013+0.i,'type','D','sym','A');
    bf = SF_Adapt(bf,em,'Hmax',5);
else
    bf = SF_Load('lastadapted'); 
end


% a few plots
SF_Plot(bf,'mesh');
SF_Plot(bf,'ux','Contour','on','ColorMap','jet');

bf.mesh.xlim=[-10,H];bf.mesh.ylim=[0,10]; % change range to fit structure of eigenmodes
% plot the spectrum and allow to click on modes !
  [ev,em] = SF_Stability(bf,'nev',1,'shift',-0.0013+0.i,'type','D','sym','A');


% 
% % Loop over k for stability computations
% k_TAB = [0:.05:4];
% k_TAB(1)=1e-8;% because k=0 won't work in this directory (something to fix in the macros)
% evS_TAB = []; evA_TAB = [];
% for k = k_TAB
%     evA = SF_Stability(bf,'k',k,'sym','A','nev',10,'shift',0.1+0.1i,'sort','LR');
%     evS = SF_Stability(bf,'k',k,'sym','S','nev',10,'shift',0.1+0.1i,'sort','LR');
%     evS_TAB = [evS_TAB,evS];
%     evA_TAB = [evA_TAB,evA];
% end    
% 
% figure(10);
% for j = 1:5
%     plot(-k_TAB,real(evS_TAB(j,:)),'o-');hold on;
%     plot(k_TAB,real(evA_TAB(j,:)),'o-'); hold on;
% end
% xlabel('|k|' ); ylabel('\lambda_r')
% ylim([-0.06,0.04]);xlim([-4,4]);
% 
% figure(11);
% for j = 1:5
%     plot(-k_TAB,imag(evS_TAB(j,:)),'o');hold on;
%     plot(k_TAB,imag(evA_TAB(j,:)),'o'); hold on;
% end
% xlabel('|k|' ); ylabel('\lambda_i')
% ylim([-.2,.2]);xlim([-4,4]);
% 

