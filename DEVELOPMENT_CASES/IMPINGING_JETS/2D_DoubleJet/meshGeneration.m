function bf = meshGeneration(varargin)
    p = inputParser;
    addParameter(p, 'H', 2, @isnumeric); % H (Aspect Ratio)
    addParameter(p, 'Rc', 2, @isnumeric); % Rc (gap between jets)
    addParameter(p, 'Rout', 80, @isnumeric); % Rout (horizontal extent)
    addParameter(p, 'Lpipe', 10, @isnumeric); % Lpipe (length of the pipe)
    addParameter(p, 'ReEnd', 300, @isnumeric); % REnd (Last Reynolds)
    addParameter(p, 'meshRef', 5, @isnumeric); % meshRef (meshRefinement = Hmax)
    addParameter(p, 'ReFirstStep', 30, @isnumeric); % meshRef (meshRefinement = Hmax)
    addParameter(p, 'ReSectStep', 10, @isnumeric); % meshRef (meshRefinement = Hmax)
    addParameter(p, 'plotOption', false, @isbool); % plotOption (to plot or not)

    parse(p, varargin{:});
    % Now the right parameters are in p.Results
    H = p.Results.H;
    Rc = p.Results.Rc;
    Rout = p.Results.Rout;
    Lpipe = p.Results.Lpipe;
    Re1Step = p.Results.ReFirstStep;
    Re2Step = p.Results.ReSectStep;
    ReEnd = p.Results.ReEnd;
    meshRef = p.Results.meshRef;
    plotOption = p.Results.plotOption;


    ffmesh = SF_Mesh('mesh_DoubleJet.edp','Params',[H Rc Lpipe Rout],'problemtype','2D');
    bf = SF_BaseFlow(ffmesh,'Re',1);
    
    Mask = SF_Mask(bf.mesh,[-10 H 0 10 .2]);
    bf = SF_Adapt(bf,Mask,'Hmax',meshRef);
    
    % computation of base flow with increasing Re
    for Re = [10:Re1Step:60] 
        bf = SF_BaseFlow(bf,'Re',Re);
        bf = SF_Adapt(bf,'Hmax',meshRef);
    end

    for Re = [60:Re2Step:ReEnd] 
        bf = SF_BaseFlow(bf,'Re',Re);
        bf = SF_Adapt(bf,'Hmax',meshRef);
    end

    [ev,em] = SF_Stability(bf,'nev',3,'shift',0.005+0.i,'k',0,'type','D');
    
    Mask = SF_Mask(bf.mesh,[-10 H 0 10 .2]);
    bf = SF_Adapt(bf,em(1),Mask,'Hmax',meshRef);

    %% Mirroring the mesh for full domain
    %  bf = SF_Mirror(bf);

    %% a few plots
    if(plotOption == true)
        figure ; SF_Plot(bf,'mesh');
        figure ; SF_Plot(bf,'ux','Contour','on','ColorMap','jet','xlim',[-4 H],'ylim',[0 Rc+3]);
        figure;SF_Plot(bf,'ux','xlim',[-5 5],'ylim',[-5 5]);
    end
end
