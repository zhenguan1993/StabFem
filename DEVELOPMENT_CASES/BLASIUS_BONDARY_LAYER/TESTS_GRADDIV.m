close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',4,'ffdatadir','WORK_BLASIUS');
SF_Status

bf = SF_Load('VISCOUSFLOWS','last');

%% without GD
FlowV = SF_Launch('Newton_2D.edp','Options',{'Re', 1e6},'Init',bf,'Store','MISC');  
             
%% with GD              
FlowV = SF_Launch('Newton_2D_GD.edp','Options',{'Re', 1e6},'Init',bf,'Store','MISC');                