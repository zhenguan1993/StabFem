%% Computation of the viscous flow over a flat plate.
%
% This example shos how to use FreeFem/StabFem to compute the flow over
% a flat plate, up to Re = 10^5. We expect to obtain the Blasius boundary
% layer solution.
% The form factor is (theoretically) THETA = delta1/delta2 = 2.5901

close all;
addpath('../../SOURCES_MATLAB');
SF_Start('verbosity',2,'ffdatadir','WORK_BLASIUS');

%% Generation of a mesh

mesh = SF_Mesh('Mesh_Blasius.edp');

figure; 
SF_Plot(mesh,'title','Initial Mesh');

%% VISCOUS FLOWS
%
% First compute a solution for a low value of Re :
FlowV = SF_Launch('Newton_2D.edp','Options',{'Re', 10},'mesh',mesh,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS');
%%
% Then loop over Re to increase progressively Re (using mesh adaptation at each step) 
Re_Tab = [100 300 1000 3000 1e4 3e4 1e5];
for Re = Re_Tab
  FlowV = SF_Adapt(FlowV,'hmin',0.0001,'recompute',false);
  FlowV = SF_Launch('Newton_2D.edp','Options',{'Re', Re},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS');        
 
end
% A final mesh adaptation
  FlowV = SF_Adapt(FlowV,'hmin',0.0001,'recompute',false,'InterpError',0.003);
  FlowV = SF_Launch('Newton_2D.edp','Options',{'Re', Re},'Init',FlowV,...
                  'DataFile','BaseFlow.txt','Store','VISCOUSFLOWS');  

%%
% Plot

figure(4) ; SF_Plot(FlowV,'vort','xlim',[-.5 1],'ylim',[0 .4],'colorrange',[-10 10],'colormap','redblue');
hold on; SF_Plot(FlowV,{'ux','uy'},'xlim',[-.5 1],'ylim',[0 .4]);
hold on; SF_Plot(FlowV,'p','contour','only','xlim',[-.5 1],'ylim',[0 .4],'CStyle','dashedneg');

figure(5); 
SF_Plot(FlowV,'mesh','title','Adapted Mesh','ylim',[0 0.4]);

%%
% Extract along one line (x=0.5)
yline = linspace(0,.1,5000);
Uline = SF_ExtractData(FlowV,'ux',.5,yline);
figure(5); plot(Uline,yline);xlabel('U');ylabel('y');ylim([0, 0.025]);

%%
% Estimating boundary layer thickness
delta1 = trapz(yline,(1-Uline))
delta2 = trapz(yline,Uline.*(1-Uline))
THETA = delta1/delta2

%%
% Extract along one line (x=0.75)
yline = linspace(0,.1,5000);
Uline = SF_ExtractData(FlowV,'ux',.75,yline);
figure(5); plot(Uline,yline);xlabel('U');ylabel('y');ylim([0, 0.025]);

%% 
% Estimating boundary layer thickness
delta1 = trapz(yline,(1-Uline))
delta2 = trapz(yline,Uline.*(1-Uline))
THETA = delta1/delta2



%% Summary 

SF_Status

%%
% [[PUBLISH]]
