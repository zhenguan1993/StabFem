
%% Chapter 0 : Initialization 
addpath('../../SOURCES_MATLAB/');
SF_core_start('verbosity',4,'workdir','./Work_chi01/');
mkdir('FIGURES')
%% Chapter 1 : builds or recovers base flow

%sfstat = SF_Status();
%if strcmp(sfstat.status,'new')
    % creation of an initial mesh
    chi = 0.1; Xmin = -20; Xmax = 60; % ,nb first case was done with Rout = 120 !
    ffmesh = SF_Mesh('Mesh_SquareConfined.edp','Params',[chi,Xmin,Xmax],'problemtype','2D')
    bf = SF_BaseFlow(ffmesh,'Re',1);
 %   Mask = SF_Mask(bf.mesh,[-1 4 0 1 .2]);
    bf = SF_Adapt(bf,'Hmax',5);
    
    % computation of base flow with increasing Re
    for Re = [10 30 100]
        bf = SF_BaseFlow(bf,'Re',Re);
        bf = SF_Adapt(bf);
    end

    % Adaptation to the structure of an eigenmode (adjoint) 
    % AND A MASK which forces the grid step to be at max 0.2 in a given rectangle
    [ev,em] = SF_Stability(bf,'nev',1,'shift',0.21311 + 0.92146i,'type','A');
     Mask = SF_Mask(bf.mesh,[-2 4 0 2 .2]);
    bf = SF_Adapt(bf,em,Mask,'Hmax',5);
%else
%    bf = SF_Load('lastadapted'); 
%end


% a few plots
figure; SF_Plot(bf,'mesh');
figure; SF_Plot(bf,'ux','Contour','on','ColorMap','jet','xlim',[-2 4],'ylim',[0 2]);

%bf.mesh.xlim=[-10,H];bf.mesh.ylim=[0,10]; % change range to fit structure of eigenmodes
% plot the spectrum and allow to click on modes !
[ev,em] = SF_Stability(bf,'nev',10,'shift',0.2+1i,'k',2,'PlotSpectrum','yes')



% Loop over Re for stability computations

Re_TAB = [60:-5:20];
evA_TAB = [];
[ev,em] = SF_Stability(bf,'nev',1,'shift',0.128077+1.02524i); % starting point for the continuation mode
for Re = Re_TAB
    bf = SF_BaseFlow(bf,'Re',Re)
    ev = SF_Stability(bf,'nev',1,'shift','cont');
    evA_TAB = [evA_TAB,ev];
end    

figure(10);
    subplot(1,2,1);
    plot(Re_TAB,real(evA_TAB),'o-');hold on;
    xlabel('Re' ); ylabel('\lambda_r')
    subplot(1,2,2);
    plot(Re_TAB,imag(evA_TAB)/(2*pi),'o-');hold on;
    xlabel('Re' ); ylabel('St')

    
% Remark : Critical Reynolds based on MEAN VELOCITY is about 35.
% This corresponds to a Re based on MAX VELOCITY about 52.5
    
    


