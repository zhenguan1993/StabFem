%% Demonstration of the new generic interface to Newton and Eigenvalue solvers
%
% Now 


SF_Start;
SF_core_setopt('verbosity', 3);
SF_core_setopt('ffdatadir', './WORK/');
SF_core_arborescence('cleanall');
format long;

%% ##### CHAPTER 1 : COMPUTING THE MESH WITH ADAPTMESH PROCEDURE

disp('##### autorun test 1 : mesh and BASE FLOW');

SF_core_arborescence('cleanall');
dimensions = [-40 80 40];
ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',dimensions);
bf=SF_BaseFlow(ffmesh,'solver','Newton_2D.edp','Re',1,'Symmetry','S');

bf=SF_BaseFlow(bf,'Re',10);
bf=SF_BaseFlow(bf,'Re',60); 
% note that 'solver' and 'Symmetry' need not to be precised if they don't change after first call 

bf=SF_BaseFlow(bf,'Re',60); 
% to check if automatic load works

bf=SF_Adapt(bf,'Hmax',5); 
% Adaptmesh works !


[ev,em] = SF_Stability(bf,'solver','Stab_2D.edp','nev',10,'shift',0.74i)
% works as well !



%% Summary
SF_Status


% [[PUBLISH]]

