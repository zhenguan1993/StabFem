%% Equilibrium shape and oscillation modes of a liquid droplet
% 

close all;
addpath([fileparts(fileparts(pwd)),'/SOURCES_MATLAB']);
SF_Start('verbosity',4,'storagemode',1);
SF_core_setopt('eigensolver','SLEPC'); % this set of programs uses arpack ; to be rewritten someday with Slepc
figureformat = 'png';
mkdir('FIGURES');

%% CHAPTER 1 : creation of initial mesh for cylindrical bridge, L=4

L = 4;
density=20;
 %creation of an initial mesh (with volume corresponding to coalescence of two spherical caps) 
freesurf = SF_Init('Mesh_Droplet.edp','problemtype','3DFreeSurfaceStatic','cleanworkdir','yes');

%% CHAPTER 2 : Eigenvalue computation for m=0 and m=1 for L=4
[evm0,emm0] =  SF_Stability(freesurf,'nev',10,'m',0,'typestart','axis','typeend','axis','sort','SIA')
[evm1,emm1] =  SF_Stability(freesurf,'nev',10,'m',1,'typestart','axis','typeend','axis','sort','SIA')

%%
figure(4);
%suptitle('m=1 sloshing modes : Meniscus (45?), H/R = 2, Bo = 500, free condition');hold on;
subplot(2,4,1);
SF_Plot(emm1(2),'phi.im','title',{'Mode (m,n)= (1,0)',['freq = ',num2str(imag(evm1(2)))]});
hold on;SF_Plot_ETA(emm1(2),'Amp',0.05);
subplot(2,4,2);
SF_Plot(emm1(4),'phi.im','title',{'Mode (m,n)= (1,1)',['freq = ',num2str(imag(evm1(4)))]});
hold on;SF_Plot_ETA(emm1(4),'Amp',0.05);
subplot(2,4,3);
SF_Plot(emm1(6),'phi.im','title',{'Mode (m,n)= (1,2)',['freq = ',num2str(imag(evm1(6)))]});
hold on;SF_Plot_ETA(emm1(6),'Amp',0.05);
subplot(2,4,4);
SF_Plot(emm1(8),'phi.im','title',{'Mode (m,n)= (1,3)',['freq = ',num2str(imag(evm1(8)))]});
hold on;SF_Plot_ETA(emm1(8),'Amp',0.05);
subplot(2,4,5);
SF_Plot(emm0(2),'phi.im','title',{'Mode (m,n)= (0,0)',['freq = ',num2str(imag(evm0(2)))]});
hold on;SF_Plot_ETA(emm0(2),'Amp',0.05);
subplot(2,4,6);
SF_Plot(emm0(4),'phi.im','title',{'Mode (m,n)= (0,1)',['freq = ',num2str(imag(evm0(4)))]});
hold on;SF_Plot_ETA(emm0(4),'Amp',0.05);
subplot(2,4,7);
SF_Plot(emm0(6),'phi.im','title',{'Mode (m,n)= (0,2)',['freq = ',num2str(imag(evm0(6)))]});
hold on;SF_Plot_ETA(emm0(6),'Amp',0.05);
subplot(2,4,8);
SF_Plot(emm0(8),'phi.im','title',{'Mode (m,n)= (0,3)',['freq = ',num2str(imag(evm0(8)))]});
hold on;SF_Plot_ETA(emm0(8),'Amp',0.05);


pos = get(gcf,'Position'); pos(3)=pos(4)*2.;set(gcf,'Position',pos); % resize aspect ratio

%%Theory

l = 1:5
sqrt(l.*(l+1))

%% CHAPTER 3 : Eigenvalue computation for m=0 and m=1 (nu = 1e-3)

[evm1,emm1] =  SF_Stability(freesurf,'nev',10,'m',1,'typestart','axis','typeend','axis','sort','SIA','nu',1e-3,'shift',1+5i)

[evm0,emm0] =  SF_Stability(freesurf,'nev',10,'m',0,'typestart','axis','typeend','axis','sort','SIA','nu',1e-3,'shift',1+5i)

