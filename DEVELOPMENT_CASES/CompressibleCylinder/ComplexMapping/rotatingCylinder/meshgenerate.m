%% Code to generate standard meshes based on the baseflow
%clear all;
% close all;
run('../../../../SOURCES_MATLAB/SF_Start.m');
figureformat='png'; AspectRatio = 0.56; % for figures
tinit = tic;
verbosity=20;
meshDIM.xinfm=-40.; meshDIM.xinfv=80.; meshDIM.yinf=40.0;
% If previous mesh to load
meshfileName='./WORK/MESHES/mesh_adapt73_Re150_Ma0.3_Omegax2.3.msh';

[ffmesh,bf]= SmartMesh(meshDIM,'Mach',0.3,'OmegaxList',[0:0.1:14],...
                       'ReTarget',[100,150,200],'MeshRefine',5,'ncores',8);

                   
ffmesh = SFcore_ImportMesh('WORK/mesh.msh',...
                            'problemtype','2dcomp');
bf = SFcore_ImportData(ffmesh,'WORK/BASEFLOWS/BaseFlow_Re1Ma0.3Omegax0.ff2m');
