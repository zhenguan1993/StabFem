function [ffmeshList,bfList]= SmartMesh(meshDIM,varargin)
%
%  Author: Javier Sierra
%  Created: 2019-02-13
%
% ffmeshList,bfList]= SmartMesh(meshDIM,varargin)
%
%

%% Description
%
%  Create a mesh for the rotating cylinder case
%

%% Input Parameters
%
%  meshDIM:   Dimension of the mesh. Array [XMin,Xmax,Ymax]
%  meshRef:   Mesh refinement, used for SF_Adapt('Hmin')
%  Ma:       Mach number to adapt.
%  ReTarget: Reynolds target. It is used to adapt to the baseflow
%  OmegaxList: It runs a loop over the given list of Omegax.
%  
%

%% Output Parameters
%
%  ffmeshList: List of meshes
%  bfList:      List of baseflows
%

if(nargin==0)
    % parameters for meshDIM creation 
    % Outer Domain 
    meshDIM.xinfm=-40.; meshDIM.xinfv=80.; meshDIM.yinf=40.0;
end


disp(' STARTING ADAPTMESH PROCEDURE : ');    
disp(' ');
disp(' ');  

% varargin (optional input parameters)
p = inputParser;
addParameter(p, 'Mach', 0.01, @isnumeric); % variable
addParameter(p, 'ReTarget', [100], @isnumeric); % variable
addParameter(p, 'MeshRefine', 1.5, @isnumeric); % variable
addParameter(p, 'OmegaxList', [0], @isnumeric); % variable
addParameter(p, 'meshName', 'WORK/mesh.msh', @ischar); % variable
addParameter(p, 'ncores', 1, @isnumeric); % variable

parse(p, varargin{:});

meshRef = p.Results.MeshRefine;
Ma = p.Results.Mach;
ReTarget = p.Results.ReTarget;
OmegaxList = p.Results.OmegaxList;
meshName = p.Results.meshName;
ncores = p.Results.ncores;

% Create initial mesh
if(isfile(meshName) == 1)
    ffmesh = SFcore_ImportMesh(meshName,'problemtype','2dcomp');
else
    ffmesh = SF_Mesh('Mesh_Cylinder.edp','Params',[meshDIM.xinfm,meshDIM.xinfv,meshDIM.yinf],'problemtype','2dcomp');
end      
% Loop over Omegax 
bfList = [];
ffmeshList =  [];
    % Start computing     
    bf=SF_BaseFlow(ffmesh,'Re',1,'Mach',Ma,'Omegax',OmegaxList(1),'ncores',ncores);
    bf=SF_Adapt(bf,'Hmax',meshRef);
    bf=SF_BaseFlow(bf,'Re',10,'Mach',Ma,'Omegax',OmegaxList(1),'ncores',ncores);
    bf=SF_BaseFlow(bf,'Re',60,'Mach',Ma,'Omegax',OmegaxList(1),'ncores',ncores);
    bf=SF_Adapt(bf,'Hmax',meshRef);
    for Re=ReTarget(end-1)
            bf=SF_BaseFlow(bf,'Re',Re,'Mach',Ma,'Omegax',OmegaxList(1),'ncores',ncores);
            bf = SF_Adapt(bf,'Hmax',meshRef);
        end
    % 'BF' will use Target Re to adapt based on BaseFlow
for Omegax=OmegaxList
    bf=SF_BaseFlow(bf,'Re',ReTarget(end),'Mach',Ma,'Omegax',Omegax,'ncores',ncores);
    bf = SF_Adapt(bf,'Hmax',meshRef);
    SF_Plot(bf.mesh);
end
    % Save ffmesh in a variable
    ffmesh=bf.mesh;
    bfList=[bf,bfList];
    ffmeshList=[ffmesh,ffmeshList];
end
