clear all;
% close all;
run('../../../../SOURCES_MATLAB/SF_Start.m');
figureformat='png'; AspectRatio = 0.56; % for figures
tinit = tic;
verbosity=20;


Ma = 0.1;
% If you have previous files load them here
ffmesh = SFcore_ImportMesh('WORK/MESHES/mesh_adapt18_Re100_Ma0.05_Omegax1.4.msh',...
                            'problemtype','2dcomp');
bf = SFcore_ImportData(ffmesh,'WORK/BASEFLOWS/BaseFlow_Re100Ma0.1Omegax4.9.ff2m');
% Otherwise
% [ffmesh,bf]= SmartMesh('D',[-40,80,40],'Ma',0.3,'OmegaxList',[0:0.1:7],...
%                        'ReTarget',[100,150,200],'MeshRefine',5);

%% Searching second mode
% Validation of the Stability_Comp_3D.edp
ReTest = 100;
alphaTest=5.0;
MaInc = 0.01
mesh.xinfv = 80;
mesh.xinfm = 40;
mesh.yinf = 40;
Params =[mesh.xinfv*0.5 mesh.xinfm*0.5 10000000 mesh.xinfv*0.6 -0.3 mesh.yinf*0.5 10000000 mesh.yinf*0.6 -0.3]; % x0, x1, LA, LC, gammac, y0, LAy, LCy, gammacy
ffmesh = SF_SetMapping(ffmesh,'MappingType','box','MappingParams',Params);
bfInc=SF_BaseFlow(bf,'Re',ReTest,'Mach',MaInc,'Omegax',alphaTest,'type','NEW');
evk1List = [];

% Initial seed 
evk1=0.144
for k=[1:0.2:2]
    [evk1,emk1] = SF_Stability(bfInc,'k',k,'shift',evk1,'nev',1,'type','D');% to initiate the cont mode
    evk1List = [evk1,evk1List];
end

figure;
plot([2:-0.2:1],real(evk1List));
figure;
plot([2:-0.2:1],imag(evk1List)/2/pi);