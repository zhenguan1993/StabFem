%% CHAPTER 0 : set the global variables needed by the drivers
disp(' ');
disp('######     ENTERING LINEAR PART       ####### ');
disp(' ');
clear all;
close all;
addpath('../../../SOURCES_MATLAB/');
SF_core_start('verbosity',4,'workdir','./WORK/');
figureformat='tif'; AspectRatio = 0.56; % for figures
system('mkdir FIGURES');
set(groot, 'defaultAxesTickLabelInterpreter','latex'); 
set(groot, 'defaultLegendInterpreter','latex');
tic;
Ma = 0.1;
Re = 1000;
np = 128;
Rout = 70;
ffmesh = SF_Mesh('Naca0012MeshFull.edp','problemtype','2dcomp','Params',[Rout,np]);
bf=SF_BaseFlow(ffmesh,'Re',10,'Mach',Ma,'type','NEW');
bf=SF_BaseFlow(bf,'Re',100,'Mach',Ma,'type','NEW');
bf=SF_Adapt(bf,'Hmax',4);
bf=SF_BaseFlow(bf,'Re',500,'Mach',Ma,'type','NEW');
bf=SF_Adapt(bf,'Hmax',4);
bf=SF_BaseFlow(bf,'Re',1000,'Mach',Ma,'type','NEW');
bf=SF_Adapt(bf,'Hmax',4);
ffmesh = SF_SetMapping(ffmesh, 'MappingType', 'box', 'MappingParams', [-10,10,-10,10,-0.3,5,-0.3,5]) 
[ev,em] = SF_Stability(bf,'shift',0.7121 + 2.5875i,'nev',3,'type','D');
bf=SF_Adapt(bf,em(1),'Hmax',4);
[evCM,emCM] = SF_Stability(bf,'shift',ev(1),'nev',3,'type','D');
[evA,emA] = SF_Stability(bf,'shift',ev(1),'nev',3,'type','A');