# This script is to be executed each time you want to publish 
# a stable release on the public gitlab repository

# first you have to delete branch master_beta from server

  cd ~
  rm -rf StabFem_MasterBeta
  mkdir StabFem_MasterBeta
  cd StabFem_MasterBeta
  git clone https://gitlab.com/stabfem/StabFem_Develop 
  cd StabFem_Develop/
  git checkout develop_david
  git pull
  rm -rf .git
  rm -rf PRIVATE_CASES
  rm -rf OBSOLETE_CASES
  rm -rf SOURCES_FREEFEM_PRIVATE/
  rm -rf PYSTABFEM_DEV/
  cd ..
  mv StabFem_Develop/ StabFem
  cd StabFem/
  #git branch -D master_beta
  #git push origin master_beta
  git init
  git add *
  git add .gitlab*
  git add .gitignore
  git add .Automatic*
  git add -f STABLE_CASES/EXAMPLE_Lshape/FIGURES/*.png
  git add -f STABLE_CASES/EXAMPLE_Lshape/*.log
  git add -f STABLE_CASES/CYLINDER/FIGURES/*.png
  git add -f STABLE_CASES/CYLINDER/FIGURES/*.bash
  git add -f 99_Documentation/MANUAL/*.png
  git add -f 99_Documentation/MANUAL/*.txt
  git add -f 99_Documentation/MANUAL/*.ff2m
  git add -f STABLE_CASES/CYLINDER/FIGURES/*.bash
  git checkout -b master_beta
  git commit -m 'Release 3.7 (restarted from scratch for cleanup with [[PUBLISH]] and [[MANUAL]])'
  git remote add origin https://gitlab.com/stabfem/StabFem
  git push --set-upstream origin master_beta


## Pour recuperer dans une autre machine pour controle
# git clone https://gitlab.com/stabfem/StabFem
# cd StabFem
# git checkout master_beta
